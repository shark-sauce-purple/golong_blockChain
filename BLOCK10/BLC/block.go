package blc

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"fmt"
	"log"
	"time"
)

type Block struct { // 结构体，定义区块
	//1.区块高度
	Height int64

	//2.上一个区块hash值(前指针)
	PrevBlockHash []byte

	//3.交易数据
	Txs []*Transaction

	//4.时间戳
	Timestamp int64

	//5.哈希
	Hash []byte

	//6.Nonce
	Nonce int64
}

//功能

//把transaction的数据转换成字节数组
func (block *Block) HashTransactions() []byte {
	var TxHashes [][]byte
	var TxHash [32]byte
	for _, tx := range block.Txs {
		TxHashes = append(TxHashes, tx.TxHash)
	}
	TxHash = sha256.Sum256(bytes.Join(TxHashes, []byte{}))
	return TxHash[:]
}

//将区块序列化字节数组
func (block *Block) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(block)
	if err != nil {
		log.Panic(err)
	}

	return result.Bytes()
}

//反序列化
func DeserializeBlock(d []byte) *Block {
	var block Block

	decoder := gob.NewDecoder(bytes.NewReader(d))
	err := decoder.Decode(&block)
	if err != nil {
		log.Panic(err)
	}

	return &block
}

//创建新的区块
func NewBlock(txs []*Transaction, height int64, PrevBlockHash []byte) *Block { //返回block

	//创建区块
	block := &Block{height, PrevBlockHash, txs, time.Now().Unix(), nil, 0}
	//[]byte(data)为把string强转成byte
	//time.Now().Unix()为1970到现在

	//调用工作量证明的方法
	pow := NewProofOfWork(block)
	//挖矿验证，返回有效hash，Nonce
	hash, nonce := pow.Run()
	fmt.Println()
	block.Hash = hash[:]
	block.Nonce = nonce
	return block

}

//创建创世区块
func CreateGenesisBlock(txs []*Transaction) *Block {
	return NewBlock(txs, 1, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
}
