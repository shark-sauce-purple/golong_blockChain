package blc

import (
	"log"

	"github.com/boltdb/bolt"
)

//迭代器
type BlockchainIterator struct {
	currentHash []byte
	DB          *bolt.DB
}



//下一个
func (blockchainIterator *BlockchainIterator) Next() *Block {
	var block *Block
	err := blockchainIterator.DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			currentBlock := b.Get(blockchainIterator.currentHash)
			//获取当前迭代器里面所对应区块
			block = DeserializeBlock(currentBlock)
			//更新迭代器
			blockchainIterator.currentHash = block.PrevBlockHash
		}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}
	return block
}
