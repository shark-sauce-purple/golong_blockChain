package blc

import (
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
	"time"

	"github.com/boltdb/bolt"
)

//数据库名
const dbName = "blockchain.db"

//表名
const blockTableName = "blocks"

type Blockchain struct {
	//最新的区块的哈希值
	Tip []byte
	//数据库
	DB *bolt.DB
}

//创建迭代器
func (bc *Blockchain) Iterator() *BlockchainIterator {
	bci := &BlockchainIterator{bc.Tip, bc.DB}
	return bci
}

//判断数据库是否存在
func dbExists() bool {
	if _, err := os.Stat(dbName); os.IsNotExist(err) {
		return false
	}
	return true
}

//遍历输出所有区块信息
func (blc *Blockchain) PrintChain() {
	BlockchainIterator := blc.Iterator()
	for {
		block := BlockchainIterator.Next()
		fmt.Printf("Height:%d\n", block.Height)
		fmt.Printf("PrevBlockHash:%x\n", block.PrevBlockHash)
		fmt.Printf("Timestamp:%s\n", time.Unix(block.Timestamp, 0).Format("2006-01-02 03:04:05 PM"))
		fmt.Printf("Hash:%x\n", block.Hash)
		fmt.Printf("Nonce:%d\n", block.Nonce)
		fmt.Printf("Txs\n")
		for _, tx := range block.Txs {
			fmt.Printf("\tTxHash: %x\n", tx.TxHash)
			for _, in := range tx.Vins {
				fmt.Printf("\tInTxHASH: %x\n", in.TxHash)
				fmt.Printf("\tVout:%d\n", in.Vout)
				fmt.Printf("\tINScriptsig:%s\n", in.ScriptSig)
			}
			for _, out := range tx.Vouts {
				fmt.Printf("\tValue:%d\n", out.Value)
				fmt.Printf("\tOutScriptPubKey:%s\n", out.ScriptPubKey)
			}
		}

		fmt.Println()
		var hashInt big.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if big.NewInt(0).Cmp(&hashInt) == 0 {
			break
		}
	}
}

//增加区块方法
func (blc *Blockchain) AddBlockToBlockchain(txs []*Transaction) {
	err := blc.DB.Update(func(tx *bolt.Tx) error {
		//获取表
		b := tx.Bucket([]byte(blockTableName))

		//创建新区快
		if b != nil {
			//获取最新区块
			blockBytes := b.Get(blc.Tip)
			//反序列化
			block := DeserializeBlock(blockBytes)
			//将区块序列号并且存储到数据库中
			newBlock := NewBlock(txs, block.Height+1, block.Hash)
			err := b.Put(newBlock.Hash, newBlock.Serialize())
			if err != nil {
				log.Panic(err)
			}
			//更新数据库中new对于的hash
			err = b.Put([]byte("new"), newBlock.Hash)
			if err != nil { //存储失败
				log.Fatal(err)
			}
			//更新Tip
			blc.Tip = newBlock.Hash

		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}
}

//创建带有创世区块区块链
func CreateGenesisBlockchain(address string) *Blockchain {
	if dbExists() {
		fmt.Println("创世区块已经存在")
		os.Exit(1)
	}
	fmt.Println("正在创建创世区块.........")

	//创建数据库
	db, err := bolt.Open(dbName, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	var hash []byte

	err = db.Update(func(tx *bolt.Tx) error {

		b, err := tx.CreateBucket([]byte(blockTableName))
		if err != nil { //存储失败
			log.Fatal(err)
		}
		if b != nil {
			txCoinbase := NewCoinbaseTransaction(address)
			//创建创世区块
			genesisBlock := CreateGenesisBlock([]*Transaction{txCoinbase})
			//将创世区块存储到表中
			err := b.Put(genesisBlock.Hash, genesisBlock.Serialize())
			if err != nil { //存储失败
				log.Fatal(err)
			}

			//存储最新区块的hash
			err = b.Put([]byte("new"), genesisBlock.Hash)
			if err != nil { //存储失败
				log.Fatal(err)
			}
			hash = genesisBlock.Hash
		}
		fmt.Println("创建成功")
		return nil
	})
	return &Blockchain{hash, db}
}

//获取blockchain对象
func GetBlockchainObject() *Blockchain {
	db, err := bolt.Open(dbName, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	var tip []byte
	err = db.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			//读取最新的hash
			tip = b.Get([]byte("new"))

		}
		return nil
	})
	return &Blockchain{tip, db}

}

//挖掘新的区块
func (Blockchain *Blockchain) MineNewBlock(from, to, amount []string) {
	//建立一笔交易
	value, _ := strconv.Atoi(amount[0])
	tx := NewSimpletransaction(from[0], to[0], value)

	//建立Transaction数组
	var txs []*Transaction
	txs = append(txs, tx)

	var block *Block
	Blockchain.DB.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			//读取最新的block
			hash := b.Get([]byte("new"))
			blockBytes := b.Get(hash)
			block = DeserializeBlock(blockBytes)

		}
		return nil
	})

	//建立新的区块
	block = NewBlock(txs, block.Height+1, block.Hash)

	//存储到数据库中
	Blockchain.DB.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			b.Put(block.Hash, block.Serialize())
			b.Put([]byte("new"), block.Hash)
			Blockchain.Tip = block.Hash
		}
		return nil
	})

}
