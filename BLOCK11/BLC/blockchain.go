package     lc

import     
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"
	"time"

	"github.com/boltdb/bolt"
)

//数据库名
const    d bNam          "    ockchain.db"

//表名
const      lockTableNam          "    ocks"

type      lockchai        ru    ct {    
	//最新的区块的哈希值
	Tip      ]byte
	//数据库
	DB      bolt.DB
}

//创建迭代器
func      b        lockchai     I    rato    ) *B    ckchainIt    erator {    
	bci              lockchainIterator{bc.Ti     b    DB}
	return      ci
}

//判断数据库是否存在
func      bExists(            ol {    
	if               r  :      os    tat(dbNa    ); os.    NotEx    ist(err) {    
		return      alse
	}
	return      rue
}

//遍历输出所有区块信息
func      bl        lockchai     P    ntCha    in() {    
	BlockchainIterator              c.Iterator()
	for      
		block              ockchainIterator.Next()
		fmt.Printf("Height:%d\n",      lock.Height)
		fmt.Printf("PrevBlockHash:%x\n",      lock.PrevBlockHash)
		fmt.Printf("Timestamp:%s\n",      ime.Unix(block.Timestamp        .Format("2006-01-0 2             : 0     05          "))
		fmt.Printf("Hash:%x\n",      lock.Hash)
		fmt.Printf("Nonce:%d\n",      lock.Nonce)
		fmt.Printf("Txs\n")
		for                 :      ra      e blo        .Txs {    
			fmt.Printf("\tTxHash:            %x\   ",    x.TxHash)
			for                 :      ra      e t    x.    ns {    
				fmt.Printf("\tInTxHASH:            %x\   ",    n.TxHash)
				fmt.Printf("\tVout:%d\n",      n.Vout)
				fmt.Printf("\tINScriptsig:%s\n",      n.ScriptSig)
			}
			for               t  :      ra      e tx    .    uts {    
				fmt.Printf("\tValue:%d\n",      ut.Value)
				fmt.Printf("\tOutScriptPubKey:%s\n",      ut.ScriptPubKey)
			}
		}

		fmt.Println()
		var      ashIn        g.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if      ig.NewInt(0).Cmp(&hashInt               0        
			break
		}
	}
}

//增加区块方法
func      bl        lockchai     A    BlockToBlockchain(    s []    ransa    ction) {    
	err              c.DB.Update(func(     *    lt.    ) e    r    r {    
		//获取表
		b              .Bucket([]byte(blockTableName))

		//创建新区快
		if                n         {    
			//获取最新区块
			blockBytes              Get(blc.Tip)
			//反序列化
			block              serializeBlock(blockBytes)
			//将区块序列号并且存储到数据库中
			newBlock              wBlock(tx     b    ck.Height    , bl    k.Hash)
			err              Put(newBlock.Has     n    Block.Serialize())
			if      r           n         {    
				log.Panic(err)
			}
			//更新数据库中new对于的hash
			err             Put([]byte("new"     n    Block.Hash)
			if      r           n               存    败
				log.Fatal(err)
			}
			//更新Tip
			blc.Tip             wBlock.Hash

		}

		return      il
	})
	if      r           n         {    
		log.Panic(err)
	}
}

//创建带有创世区块区块链
func      reateGenesisBlockchain(addres        rin     *    ockc    hain {    
	if      bExists(         
		fmt.Println("创世区块已经存在")
		os.Exit(1)
	}
	fmt.Println("正在创建创世区块.........")

	//创建数据库
	db,      r           b    t.Open(dbNa    , 06       , nil    
	if      r           n         {    
		log.Fatal(err)
	}
	var      as        byte

	err             .Update(func(     *    lt.    ) e    r    r {    

		b,      r           t    CreateBucket([]byte(blockTableName))
		if      r           n               存    败
			log.Fatal(err)
		}
		if                n         {    
			txCoinbase              wCoinbaseTransaction(address)
			//创建创世区块
			genesisBlock              eateGenesisBlock([]*Transaction{txCoinbase})
			//将创世区块存储到表中
			err              Put(genesisBlock.Has     g    esisBlock.Serialize())
			if      r           n               存    败
				log.Fatal(err)
			}

			//存储最新区块的hash
			err             Put([]byte("new"     g    esisBlock.Hash)
			if      r           n               存    败
				log.Fatal(err)
			}
			hash             nesisBlock.Hash
		}
		fmt.Println("创建成功")
		return      il
	})
	return      Blockchain{hash        }
}

//获取blockchain对象
func      etBlockchainObject(        lockcha    in {    
	db,      r           b    t.Open(dbNa    , 06       , nil    
	if      r           n         {    
		log.Fatal(err)
	}
	var      i        byte
	err             .View(func(     *    lt.    ) e    r    r {    

		b              .Bucket([]byte(blockTableName))
		if                n         {    
			//读取最新的hash
			tip             Get([]byte("new"))

		}
		return      il
	})
	return      Blockchain{tip        }

}

//挖掘新的区块
func      Blockchai        lockchai     M    eNewBlock(fr  m , t       amo      t [    ]st    ng) {    
	//建立一笔交易
	value,                s    conv.Atoi(amount[0])
	tx              wSimpletransaction(from[0     t        , va    e)

	//建立Transaction数组
	var      x        *Transaction
	txs             pend(tx     t    

	var      loc        lock
	Blockchain.DB.View(func(tx      bolt.Tx        r    or {    

		b              .Bucket([]byte(blockTableName))
		if                n         {    
			//读取最新的block
			hash              Get([]byte("new"))
			blockBytes              Get(hash)
			block             serializeBlock(blockBytes)

		}
		return      il
	})

	//建立新的区块
	block             wBlock(tx     b    ck.Height    , bl    k.Hash)

	//存储到数据库中
	Blockchain.DB.Update(func(tx      bolt.Tx        r    or {    

		b              .Bucket([]byte(blockTableName))
		if                n         {    
			b.Put(block.Hash,      lock.Serialize())
			b.Put([]byte("new"),      lock.Hash)
			Blockchain.Tip             ock.Hash
		}
		return      il
	})

}

//查询余额
func      Blockchai        lockchai     G    Balance(addr    s st       g)     int     {    
	utxos              ockchain.UnUTXOs(address)
	var      moun        t64
	for                o  :      ra      e     utx     {    
		amount              t64(utxo.Outputs.Value)
	}
	return      mount
}
