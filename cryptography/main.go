package main

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"log"

	"golang.org/x/crypto/ripemd160"
)

func main() {

	//sha256
	hasher := sha256.New()
	hasher.Write([]byte("要转换的内容"))
	bytes := hasher.Sum(nil)
	fmt.Println(bytes)
	//ripemd160
	hasher = ripemd160.New()
	hasher.Write([]byte("要转换的内容"))
	hashBytes := hasher.Sum(nil)
	hashString := fmt.Sprintf("%x", hashBytes)
	fmt.Println(hashString)

	//base64
	input := []byte("hello world")
	fmt.Println("原始内容：" + string(input))

	// 演示base64编码
	encodeString := base64.StdEncoding.EncodeToString(input)
	fmt.Println("编码：" + encodeString)

	// 对上面的编码结果进行base64解码
	decodeBytes, err := base64.StdEncoding.DecodeString(encodeString)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("解码：" + string(decodeBytes))

	// 如果要用在url中，需要使用 URLEncoding
	uEnc := base64.URLEncoding.EncodeToString([]byte(input))
	fmt.Println("url编码：" + uEnc)

	uDec, err := base64.URLEncoding.DecodeString(uEnc)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("url解码：" + string(uDec))
}
