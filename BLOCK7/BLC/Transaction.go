package blc

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"log"
)

type Transaction struct {
	//1.交易hash
	TxHash []byte
	//2.输入
	Vins []*TXInput
	//3.输出
	Vouts []*TXOutput
}

//Transaction 创建
//1.创世区块的Transaction
func NewCoinbaseTransaction(address string) *Transaction { //address为

	//消费
	txinput := &TXInput{[]byte{}, -1, "创世区块"}
	txoutput := &TXOutput{10, "创世区块"}

	txCoinbase := &Transaction{[]byte{}, []*TXInput{txinput}, []*TXOutput{txoutput}}

	//此处hash为将自己序列化
	txCoinbase.HashTransactions()

	return txCoinbase
}

//此处hash为将自己序列化
func (tx *Transaction) HashTransactions() {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(tx)
	if err != nil {
		log.Panic(err)
	}
	hash := sha256.Sum256(result.Bytes())
	tx.TxHash = hash[:]

}
