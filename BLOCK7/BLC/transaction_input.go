package blc


// TXInput represents a transaction input
type TXInput struct {
	//交易的hash
	TxHash      []byte
	//存储TXout在Vout里的索引
	Vout      int
	//用户名
	ScriptSig string

	// Signature []byte
	// PubKey    []byte
}


// func (in *TXInput) UsesKey(pubKeyHash []byte) bool {
// 	lockingHash := HashPubKey(in.PubKey)

// 	return bytes.Compare(lockingHash, pubKeyHash) == 0
// }
