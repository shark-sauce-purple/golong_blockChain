package blc

import (
	"bytes"
	"encoding/hex"

	"log"

	"github.com/boltdb/bolt"
)

//方法：
//1.遍历整个数据库，将数据中所有的UTXO存入一张表中。会慢，但后续可以直接读表运行，会很快
//过程：
//1.遍历数据库
//2.遍历时，返回[]*TxOutputs
//结构
//type TXOutputs struct{
//	Txoutputs []*TXOutput
//	TxHash []byte
//}
// 遍历[]*TxOutputs，得到UTXO
// _,txoutputs:=range Txoutputs{
// }
//utxo表名
const utxoTableName = "utxoTableName"

//结构体
type UTXOSet struct {
	Blockchain *Blockchain
}

//重置数据库表
func (utxoset *UTXOSet) ResetUTXOset() {
	err := utxoset.Blockchain.DB.Update(func(tx *bolt.Tx) error {
		//判断表是否存在
		b := tx.Bucket([]byte(utxoTableName))
		if b != nil { //如果表存在
			//删除原来的表
			err := tx.DeleteBucket([]byte(utxoTableName))
			if err != nil {
				log.Panic(err)
			}
		}
		//创建新的表
		b, _ = tx.CreateBucket([]byte(utxoTableName))
		if b != nil {
			//[]*TxOutputs
			txoutputsMap := utxoset.Blockchain.FindUTXOMap()
			for keyHah, outs := range txoutputsMap {
				txHash, _ := hex.DecodeString(keyHah)
				b.Put(txHash, outs.Serialize())

			}
		}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}
}

func (utxoset *UTXOSet) findUTXOForAdress(address string) []*UTXO {
	var utxos []*UTXO

	err := utxoset.Blockchain.DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoTableName))
		c := b.Cursor() //游标

		for k, v := c.First(); k != nil; k, _ = c.Next() {
			//	fmt.Printf("key = %s,value=%s\n",k,v)

			TXOutputs := DeserializeOutputs(v)
			for _, utxo := range TXOutputs.UTXOs {
				if utxo.Outputs.OUTUnLock(address) {
					utxos = append(utxos, utxo)
				}
			}
		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}

	return utxos
}

//获取金额
func (utxoset *UTXOSet) GetBalance(address string) int64 {

	UTXos := utxoset.findUTXOForAdress(address)
	var amount int64
	for _, utxo := range UTXos {
		amount += int64(utxo.Outputs.Value)
	}
	return amount

}

//返回未花费包
func (utxoSet *UTXOSet) FindUnpackageSpentableUTXOS(from string, txs []*Transaction) []*UTXO {
	//计数
	//var money int = 0
	//未花费Transaction数组
	var unUTXOs []*UTXO
	//已花费
	sepentTXOutputs := make(map[string][]int)
	//处理Transaction

	for _, tx := range txs {

		if !tx.IsCoinbaseTransaction() {
			for _, in := range tx.Vins {
				//能否解锁
				pubKeyHash := Base58Decode([]byte(from))
				pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-4]
				if in.INUnLockripemd160(pubKeyHash) {
					//转换
					key := hex.EncodeToString(in.TxHash)
					//存入
					sepentTXOutputs[key] = append(sepentTXOutputs[key], in.Vout)
				}
			}
		}
	}
	for _, tx := range txs {
	work1:
		for index, out := range tx.Vouts {
			//判断
			if out.OUTUnLock(from) {
				if len(sepentTXOutputs) == 0 {
					utxo := &UTXO{tx.TxHash, index, out}
					unUTXOs = append(unUTXOs, utxo)
				}
				for hash, indexArray := range sepentTXOutputs {
					txHashStr := hex.EncodeToString(tx.TxHash)
					if txHashStr == hash {
						var isUnSpendUTXO bool
						for _, outIndex := range indexArray {
							if index == outIndex {
								isUnSpendUTXO = true
								continue work1
							}
							if !isUnSpendUTXO {
								utxo := &UTXO{tx.TxHash, index, out}
								unUTXOs = append(unUTXOs, utxo)
							}
						}
					} else {
						utxo := &UTXO{tx.TxHash, index, out}
						unUTXOs = append(unUTXOs, utxo)
					}

				}
			}
		}

	}

	return unUTXOs

}

func (utxoSet *UTXOSet) FindSpentableUTXOS(from string, amount int, txs []*Transaction) (int, map[string][]int) {
	unPa := utxoSet.FindUnpackageSpentableUTXOS(from, txs)
	var money int = 0
	spentableUtxo := make(map[string][]int)
	for _, UTXO := range unPa {
		money += UTXO.Outputs.Value
		txHash := hex.EncodeToString(UTXO.TxHash)
		spentableUtxo[txHash] = append(spentableUtxo[txHash], UTXO.index)
		if money >= amount { //如果大于了
			return money, spentableUtxo
		}
	}
	//钱还不够
	utxoSet.Blockchain.DB.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoTableName))
		if b != nil {
			c := b.Cursor() //游标
		UTXOB:
			for k, v := c.First(); k != nil; k, _ = c.Next() {
				//fmt.Printf("key = %s,value=%s\n",k,v)
				TXOutputs := DeserializeOutputs(v)
				for _, utxo := range TXOutputs.UTXOs {
					money += utxo.Outputs.Value
					txHash := hex.EncodeToString(utxo.TxHash)
					spentableUtxo[txHash] = append(spentableUtxo[txHash], utxo.index)
					if money >= amount { //如果大于了
						break UTXOB
					}
				}
			}

		}
		return nil
	})
	if money < amount {
		log.Panic("余额不足")
	}

	return money, spentableUtxo
}

//更新
func (utxoSet *UTXOSet) Update() {
	//最新的Block
	block := utxoSet.Blockchain.Iterator().Next()
	//utxoTable
	ins := []*TXInput{}
	outsMap := make(map[string]*TXOutputs)
	//找到要删除的书籍
	for _, tx := range block.Txs {
		for _, in := range tx.Vins {
			ins = append(ins, in)
		}
	}

	for _, tx := range block.Txs {
		utxos := []*UTXO{}
		for index, out := range tx.Vouts {
			isSpent := false
			for _, in := range ins {

				if in.Vout == index && bytes.Compare(tx.TxHash, in.TxHash) == 0 && bytes.Compare(out.Ripemd160, ripemd160hash(in.PubKey)) == 0 {
					isSpent = true
					continue
				}
			}
			if isSpent {
				utxo := &UTXO{tx.TxHash, index, out}
				utxos = append(utxos, utxo)
			}

		}
		if len(utxos) > 0 {
			txHash := hex.EncodeToString(tx.TxHash)
			outsMap[txHash] = &TXOutputs{utxos}
		}

	}

	err := utxoSet.Blockchain.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoTableName))
		if b != nil {
			//删除
			for _, in := range ins {
				TXoutputs := b.Get(in.TxHash) //判断是否存在
				if len(TXoutputs) == 0 {
					continue
				}
				txoutputs := DeserializeOutputs(TXoutputs)
				UTXOS := []*UTXO{}
				//判断是否需要
				isNeedDelete := false
				for _, utxo := range txoutputs.UTXOs {
					if in.Vout == utxo.index && bytes.Compare(utxo.Outputs.Ripemd160, ripemd160hash(in.PubKey)) == 0 {
						//b.Delete(in.TxHash)
						isNeedDelete = true
					} else {
						UTXOS = append(UTXOS, utxo)
					}
				}

				if isNeedDelete {
					b.Delete(in.TxHash)
					if len(UTXOS) > 0 {

						preTxoutputs := outsMap[hex.EncodeToString(in.TxHash)]
						preTxoutputs.UTXOs = append(preTxoutputs.UTXOs, UTXOS...)
						outsMap[hex.EncodeToString(in.TxHash)] = preTxoutputs
					}

				}
			}
			//新增
			for keyHash, outPuts := range outsMap {
				keyHashBytes, _ := hex.DecodeString(keyHash)
				b.Put(keyHashBytes, outPuts.Serialize())
			}

		}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}

}
