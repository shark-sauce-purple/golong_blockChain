package blc

import (
	"flag"
	"fmt"
	"log"
	"os"
)

type CLI struct {
	// Blockchain *Blockchain
}

//输出使用说明
func printUsage() {
	fmt.Println("使用说明")
	fmt.Println("\twalist --输出所有钱包地址")
	fmt.Println("\tcreatewallet --创建钱包")
	fmt.Println("\tcreateblockchain -address ADDRESS--地址 创建创世区块")
	fmt.Println("\tsend -from FROM -to To -amount AMOUNT  DATA--交易数据  发送交易")
	fmt.Println("\tprintchain --输出数据")
	fmt.Println("\tgetbalance -address ADDRESS--地址  输出区块账单余额信息")
	fmt.Println("\ttest 测试")

}

//判断args，防止error
func isValidArgs() {
	//如果终端数据<2
	if len(os.Args) < 2 {
		printUsage()
		os.Exit(0)
	}

}

//////////////////////////////////////////////////////
// //增加区块
// func (cli *CLI) addBlock(txs []*Transaction) {
// 	if dbExists() == false {
// 		fmt.Println("数据库不存在")
// 		os.Exit(1)
// 	}
// 	//获取数据库
// 	blockchain := GetBlockchainObject()
// 	defer blockchain.DB.Close()
// 	//调用函数
// 	blockchain.AddBlockToBlockchain(txs)

// }

////////////////////////////////////////
//运行
func (cli *CLI) Run() {
	isValidArgs()
	walistCmd:=flag.NewFlagSet("walist", flag.ExitOnError)
	testCmd:=flag.NewFlagSet("test", flag.ExitOnError)
	createWalletCmd := flag.NewFlagSet("createwallet", flag.ExitOnError)
	sendBlockCmd := flag.NewFlagSet("send", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)
	creatBlockChainCmd := flag.NewFlagSet("createblockchain", flag.ExitOnError)
	getbalanceCmd := flag.NewFlagSet("getbalance", flag.ExitOnError)

	flagFrom := sendBlockCmd.String("from", "", "转账源地址。。。")
	flagTo := sendBlockCmd.String("to", "", "转账目的地址。。。")                                          //增加数据
	flagamount := sendBlockCmd.String("amount", "", "转账金额。。。")                                    //增加数据
	flagCreateBlockchainWithAddress := creatBlockChainCmd.String("address", "创世区块数据", "创建创世区块地址") //第二个为默认值
	flaggetbalanceWithAdress := getbalanceCmd.String("address", "", "查询某一个账号的余额。。。")
	//解析数据
	switch os.Args[1] {
	case "send":
		//解析后续内容
		err := sendBlockCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "test":
		//解析后续内容
		err := testCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "walist":
		//解析后续内容
		err := walistCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "printchain":
		//解析后续内容
		err := printChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "createblockchain":
		err := creatBlockChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "getbalance":
		err := getbalanceCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "createwallet":
		err := createWalletCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	default:
		printUsage()
		os.Exit(0)

	}
	//增加
	if sendBlockCmd.Parsed() { //解析add数据
		if *flagFrom == "" || *flagTo == "" || *flagamount == "" { //如果没有data
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		// fmt.Println(JSONTOArry(*flagFrom))
		// fmt.Println(JSONTOArry(*flagTo))
		// fmt.Println(JSONTOArry(*flagamount))
		from := JSONTOArry(*flagFrom)
		to := JSONTOArry(*flagTo)
		for index, fromAdress := range from {
			if !isValidForAddress([]byte(fromAdress)) || !isValidForAddress([]byte(to[index])) {
				fmt.Println("地址无效")
				os.Exit(1)
			}
		}
		amount := JSONTOArry(*flagamount)
		cli.send(from, to, amount)
	}
	//输出
	if printChainCmd.Parsed() {
		//fmt.Println("输出所有区块数据")
		cli.printChain()
	}

	//创世区块创建
	if creatBlockChainCmd.Parsed() {
		if !isValidForAddress([]byte(*flagCreateBlockchainWithAddress)) { //如果没有data
			fmt.Println("地址无效...")
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		cli.creatBlockChain(*flagCreateBlockchainWithAddress)
	}

	//区块balance
	if getbalanceCmd.Parsed() {
		if !isValidForAddress([]byte(*flaggetbalanceWithAdress)) { //如果没有data
			fmt.Println("地址无效...")
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		cli.getBalance(*flaggetbalanceWithAdress)
	}

	//创建钱包
	if createWalletCmd.Parsed() {
		cli.CreateWallet()
	}

	//输出钱包列表
	if walistCmd.Parsed(){
		cli.addressLLists()
	}

	//输出钱包列表
	if testCmd.Parsed(){
		cli.testMethodd()
	}
}
