package blc

import (
	"bytes"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"

	"time"

	"github.com/boltdb/bolt"
)

//数据库名
const dbName = "blockchain.db"

//表名
const blockTableName = "blocks"

type Blockchain struct {
	//最新的区块的哈希值
	Tip []byte
	//数据库
	DB *bolt.DB
}

//创建迭代器
func (bc *Blockchain) Iterator() *BlockchainIterator {
	bci := &BlockchainIterator{bc.Tip, bc.DB}
	return bci
}

//判断数据库是否存在
func dbExists() bool {
	if _, err := os.Stat(dbName); os.IsNotExist(err) {
		return false
	}
	return true
}

//遍历输出所有区块信息
func (blc *Blockchain) PrintChain() {
	BlockchainIterator := blc.Iterator()
	for {
		block := BlockchainIterator.Next()
		fmt.Printf("Height:%d\n", block.Height)
		fmt.Printf("PrevBlockHash:%x\n", block.PrevBlockHash)
		fmt.Printf("Timestamp:%s\n", time.Unix(block.Timestamp, 0).Format("2006-01-02 03:04:05 PM"))
		fmt.Printf("Hash:%x\n", block.Hash)
		fmt.Printf("Nonce:%d\n", block.Nonce)
		fmt.Printf("Txs\n")
		for _, tx := range block.Txs {
			fmt.Printf("\tTxHash: %x\n", tx.TxHash)
			for _, in := range tx.Vins {
				fmt.Printf("\tInTxHASH: %x\n", in.TxHash)
				fmt.Printf("\tVout:%d\n", in.Vout)
				fmt.Printf("\tINPubKey:%x\n", in.PubKey)
			}
			for _, out := range tx.Vouts {
				fmt.Printf("\tValue:%d", out.Value)
				fmt.Printf("\tOutPubKeyHash:%x\n", out.Ripemd160)
			}
		}

		fmt.Println()
		var hashInt big.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if big.NewInt(0).Cmp(&hashInt) == 0 {
			break
		}
	}
}

//增加区块方法
func (blc *Blockchain) AddBlockToBlockchain(txs []*Transaction) {
	err := blc.DB.Update(func(tx *bolt.Tx) error {
		//获取表
		b := tx.Bucket([]byte(blockTableName))

		//创建新区快
		if b != nil {
			//获取最新区块
			blockBytes := b.Get(blc.Tip)
			//反序列化
			block := DeserializeBlock(blockBytes)
			//将区块序列号并且存储到数据库中
			newBlock := NewBlock(txs, block.Height+1, block.Hash)
			err := b.Put(newBlock.Hash, newBlock.Serialize())
			if err != nil {
				log.Panic(err)
			}
			//更新数据库中new对于的hash
			err = b.Put([]byte("new"), newBlock.Hash)
			if err != nil { //存储失败
				log.Fatal(err)
			}
			//更新Tip
			blc.Tip = newBlock.Hash

		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}
}

//创建带有创世区块区块链
func CreateGenesisBlockchain(address string) *Blockchain {
	if dbExists() {
		fmt.Println("创世区块已经存在")
		os.Exit(1)
	}
	fmt.Println("正在创建创世区块.........")

	//创建数据库
	db, err := bolt.Open(dbName, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	var hash []byte

	db.Update(func(tx *bolt.Tx) error {

		b, err := tx.CreateBucket([]byte(blockTableName))
		if err != nil { //存储失败
			log.Fatal(err)
		}
		if b != nil {
			txCoinbase := NewCoinbaseTransaction(address)
			//创建创世区块
			genesisBlock := CreateGenesisBlock([]*Transaction{txCoinbase})
			//将创世区块存储到表中
			err := b.Put(genesisBlock.Hash, genesisBlock.Serialize())
			if err != nil { //存储失败
				log.Fatal(err)
			}

			//存储最新区块的hash
			err = b.Put([]byte("new"), genesisBlock.Hash)
			if err != nil { //存储失败
				log.Fatal(err)
			}
			hash = genesisBlock.Hash
		}
		fmt.Println("创建成功")
		return nil
	})
	return &Blockchain{hash, db}
}

//获取blockchain对象
func GetBlockchainObject() *Blockchain {
	db, err := bolt.Open(dbName, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	var tip []byte
	db.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			//读取最新的hash
			tip = b.Get([]byte("new"))

		}
		return nil
	})
	return &Blockchain{tip, db}

}

//转帐时查询from可以的UTXO
func (Blockchain *Blockchain) FindSpentableUTXOS(from string, amount int, txs []*Transaction) (int, map[string][]int) {
	//1.获取所有的UTXO
	utxos := Blockchain.UnUTXOs(from, txs)

	//2.遍历utxos,得到钱
	var value int
	spendAbleUTXO := make(map[string][]int)
	for _, utxo := range utxos {
		value = value + utxo.Outputs.Value
		hash := hex.EncodeToString(utxo.TxHash)                       //获取utxo的哈希
		spendAbleUTXO[hash] = append(spendAbleUTXO[hash], utxo.index) //存入utxo
		//如果钱够了，直接跳出循环
		if value >= amount {
			break
		}
	}
	if value < amount {
		fmt.Printf("%s余额不足,无法转账,现有余额为%d", from, value)
		os.Exit(1)
	}
	return value, spendAbleUTXO
}

//挖掘新的区块
func (Blockchain *Blockchain) MineNewBlock(from, to, amount []string) {

	//建立Transaction数组

	utxoSet := &UTXOSet{Blockchain}
	var txs []*Transaction

	//建立交易
	for index, address := range from {
		value, _ := strconv.Atoi(amount[index])
		tx := NewSimpletransaction(address, to[index], value, utxoSet, txs)
		txs = append(txs, tx)
		//fmt.Println("交易tx:", tx)
	}

	//挖矿奖励交易,目前
	tx := NewCoinbaseTransaction(from[0])
	txs = append(txs, tx)

	//查询
	var block *Block
	Blockchain.DB.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			//读取最新的block
			hash := b.Get([]byte("new"))
			blockBytes := b.Get(hash)
			block = DeserializeBlock(blockBytes)

		}
		return nil
	})
	//在建立新区块之前进行验证
	_txs := []*Transaction{}
	for _, tx := range txs {
		if !Blockchain.VerifyTransaction(tx, _txs) {
			log.Panic("签名失败")
		}
		_txs = append(_txs, tx)
	}

	//建立新的区块
	block = NewBlock(txs, block.Height+1, block.Hash)

	//存储到数据库中
	Blockchain.DB.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			b.Put(block.Hash, block.Serialize())
			b.Put([]byte("new"), block.Hash)
			Blockchain.Tip = block.Hash
		}
		return nil
	})

}

//查询余额
func (Blockchain *Blockchain) GetBalance(address string) int64 {
	//查询时不用考虑transaction，因为查询是以前的数据
	utxos := Blockchain.UnUTXOs(address, []*Transaction{})
	var amount int64
	for _, utxo := range utxos {
		amount += int64(utxo.Outputs.Value)
	}
	return amount
}

//如果一个地址对应的TXOutput未花费，那么这个Transaction就应该添加到数组中返回
func (Blockchain *Blockchain) UnUTXOs(address string, txs []*Transaction) []*UTXO {

	//未花费Transaction数组
	var unUTXOs []*UTXO
	//已花费
	sepentTXOutputs := make(map[string][]int)
	//处理Transaction
	for _, tx := range txs {

		if !tx.IsCoinbaseTransaction() {
			for _, in := range tx.Vins {
				//能否解锁
				pubKeyHash := Base58Decode([]byte(address))
				pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-4]
				if in.INUnLockripemd160(pubKeyHash) {
					//转换
					key := hex.EncodeToString(in.TxHash)
					//存入
					sepentTXOutputs[key] = append(sepentTXOutputs[key], in.Vout)
				}
			}
		}
	}
	for _, tx := range txs {
	work1:
		for index, out := range tx.Vouts {
			//判断
			if out.OUTUnLock(address) {
				if len(sepentTXOutputs) == 0 {
					utxo := &UTXO{tx.TxHash, index, out}
					unUTXOs = append(unUTXOs, utxo)
				}
				for hash, indexArray := range sepentTXOutputs {
					txHashStr := hex.EncodeToString(tx.TxHash)
					if txHashStr == hash {
						var isUnSpendUTXO bool
						for _, outIndex := range indexArray {
							if index == outIndex {
								isUnSpendUTXO = true
								continue work1
							}
							if !isUnSpendUTXO {
								utxo := &UTXO{tx.TxHash, index, out}
								unUTXOs = append(unUTXOs, utxo)
							}
						}
					} else {
						utxo := &UTXO{tx.TxHash, index, out}
						unUTXOs = append(unUTXOs, utxo)
					}

				}
			}
		}

	}

	BlockIterator := Blockchain.Iterator() //迭代器
	for {

		//迭代
		block := BlockIterator.Next()

		for i := len(block.Txs) - 1; i >= 0; i-- {
			tx := block.Txs[i]
			//Vins
			if !tx.IsCoinbaseTransaction() {
				for _, in := range tx.Vins {
					//判断这笔钱是不是所要判断的人输入的
					pubKeyHash := Base58Decode([]byte(address))
					pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-4]
					if in.INUnLockripemd160(pubKeyHash) {
						//转换
						key := hex.EncodeToString(in.TxHash)
						//存入
						sepentTXOutputs[key] = append(sepentTXOutputs[key], in.Vout)
					}
				}
			}
			//Vouts
		work:
			for index, out := range tx.Vouts {
				//判断这笔钱是不是所要判断的人输出的
				if out.OUTUnLock(address) {
					fmt.Println(out)
					fmt.Println(sepentTXOutputs)
					//if sepentTXOutputs != nil {
					if len(sepentTXOutputs) != 0 { //判断长度是否为0
						var isSpendUTXO bool
						for txHash, indexArray := range sepentTXOutputs { //txHash为input的hash
							for _, i := range indexArray {
								//说明这比钱已经被花费
								if index == i && txHash == hex.EncodeToString(tx.TxHash) {
									isSpendUTXO = true
									continue work
								}
							}
						}
						if !isSpendUTXO {
							utxo := &UTXO{tx.TxHash, index, out}
							unUTXOs = append(unUTXOs, utxo)
						}

					} else {
						utxo := &UTXO{tx.TxHash, index, out}
						unUTXOs = append(unUTXOs, utxo)
					}
				}
				//}
			}
		}

		//判断是否到创世区块
		var hashInt big.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if hashInt.Cmp(big.NewInt(0)) == 0 {
			break
		}

	}
	return unUTXOs
}

//签名实现
func (Blockchain *Blockchain) SignTransaction(tx *Transaction, privKey ecdsa.PrivateKey, txs []*Transaction) {

	//如果为创世
	if tx.IsCoinbaseTransaction() {
		return
	}
	prevTXs := make(map[string]Transaction)

	for _, vin := range tx.Vins {
		//找到对应的hash
		prevTX, err := Blockchain.FindTransaction(vin.TxHash, txs)
		if err != nil {
			log.Panic(err)
		}
		prevTXs[hex.EncodeToString(prevTX.TxHash)] = prevTX
	}

	tx.Sign(privKey, prevTXs)

}

//找到交易
func (bc *Blockchain) FindTransaction(ID []byte, txs []*Transaction) (Transaction, error) {

	for _, tx := range txs {
		if bytes.Equal(tx.TxHash, ID) {
			return *tx, nil
		}

	}

	bci := bc.Iterator()

	for {
		block := bci.Next()

		for _, tx := range block.Txs {
			if bytes.Equal(tx.TxHash, ID) {
				return *tx, nil
			}
		}

		var hashInt big.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if big.NewInt(0).Cmp(&hashInt) == 0 {
			break
		}
	}

	return Transaction{}, nil
}

//验证交易
func (bc *Blockchain) VerifyTransaction(tx *Transaction, txs []*Transaction) bool {

	prevTXs := make(map[string]Transaction)

	for _, vin := range tx.Vins {
		prevTX, err := bc.FindTransaction(vin.TxHash, txs)
		if err != nil {
			log.Panic(err)
		}
		prevTXs[hex.EncodeToString(prevTX.TxHash)] = prevTX
	}

	return tx.Verify(prevTXs)
}

//寻找UTXO字典值
func (blc *Blockchain) FindUTXOMap() map[string]*TXOutputs {
	//迭代器
	blcIterator := blc.Iterator()
	//存储已花费的数组
	//找到UTXO，需要知道input中已花费掉的，才能知道未花费的，所以需要得到input
	spentableUTXOsMap := make(map[string][]*TXInput)
	//SpentMap:=[]*TXInput{}
	//要得到的值
	utxoMaps := make(map[string]*TXOutputs)

	for {
		//迭代
		Block := blcIterator.Next()

		//遍历Transactions
		for i := len(Block.Txs) - 1; i >= 0; i-- {
			txOutputs := &TXOutputs{[]*UTXO{}}

			tx := Block.Txs[i]

			//转化为字符串
			//coinbase
			if !tx.IsCoinbaseTransaction() {
				for _, txinput := range tx.Vins {
					txHash := hex.EncodeToString(txinput.TxHash)
					spentableUTXOsMap[txHash] = append(spentableUTXOsMap[txHash], txinput)
				}
			}
			txHash := hex.EncodeToString(tx.TxHash)
			//遍历输出
		workLoop:
			for index, out := range tx.Vouts {

				TXInputs := spentableUTXOsMap[txHash]
				if len(TXInputs) > 0 {
					isSpen := false
					for _, in := range TXInputs {
						outPublicKey := out.Ripemd160
						inPubkey := in.PubKey
						//判断是否花费
						if bytes.Equal(outPublicKey, ripemd160hash(inPubkey)) {
							if index == in.Vout {
								isSpen = true
								continue workLoop
							}
						}
					}
					if !isSpen {
						utxo := &UTXO{tx.TxHash, index, out}
						txOutputs.UTXOs = append(txOutputs.UTXOs, utxo)
					}
				} else {
					//加入数据
					utxo := &UTXO{tx.TxHash, index, out}
					txOutputs.UTXOs = append(txOutputs.UTXOs, utxo)
				}
			}
			//设置键值对
			utxoMaps[txHash] = txOutputs

		}
		//找到最初区块，退出
		var hashInt big.Int
		hashInt.SetBytes(Block.PrevBlockHash)
		if big.NewInt(0).Cmp(&hashInt) == 0 {
			break
		}
	}
	return utxoMaps
}
