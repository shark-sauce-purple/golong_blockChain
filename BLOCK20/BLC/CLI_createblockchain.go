package blc

//创建创世区块
func (cli *CLI) creatBlockChain(address string) {

	//创建交易
	blockchain := CreateGenesisBlockchain(address)
	defer blockchain.DB.Close()

	//utxo数据表
	UTXOSet:=&UTXOSet{blockchain}
	UTXOSet.ResetUTXOset()
}
