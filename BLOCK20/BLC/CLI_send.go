package blc

import (
	"fmt"
	"os"
)

//转帐
func (cli *CLI) send(from []string, to []string, amount []string) {
	if !dbExists() {
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	blockchain.MineNewBlock(from, to, amount)

	utxoSet:=&UTXOSet{blockchain}

	//转账成功后，进行更新
	utxoSet.Update()
}
