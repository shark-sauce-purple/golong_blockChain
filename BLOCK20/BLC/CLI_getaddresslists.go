package blc

import "fmt"

func (cli *CLI) addressLLists() []string {
	fmt.Println("打印所有的钱包地址：")
	wallets, _ := NewWallets()
	for address, _ := range wallets.Wallets {
		fmt.Println(address)
	}
	return nil
}
