package blc

import "bytes"

// TXInput represents a transaction input
type TXInput struct {
	//交易的hash
	TxHash []byte
	//存储TXout在Vout里的索引
	Vout int
	//数字签名
	Signature []byte
	//公钥
	PubKey []byte
}

//解锁
func (txInput *TXInput) INUnLockripemd160(r160 []byte) bool {

	PublicKey := ripemd160hash(txInput.PubKey)
	return bytes.Equal(PublicKey,r160)
}

// func (in *TXInput) UsesKey(pubKeyHash []byte) bool {
// 	lockingHash := HashPubKey(in.PubKey)

// 	return bytes.Compare(lockingHash, pubKeyHash) == 0
// }
