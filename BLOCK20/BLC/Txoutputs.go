package blc

import (
	"bytes"
	"encoding/gob"
	"log"
)

type TXOutputs struct {
	UTXOs []*UTXO
	//TxHash []byte
}

//将区块序列化字节数组
func (txsout *TXOutputs) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(txsout)
	if err != nil {
		log.Panic(err)
	}

	return result.Bytes()
}

//反序列化
func DeserializeOutputs(d []byte) *TXOutputs {
	var txou TXOutputs

	decoder := gob.NewDecoder(bytes.NewReader(d))
	err := decoder.Decode(&txou)
	if err != nil {
		log.Panic(err)
	}

	return &txou
}
