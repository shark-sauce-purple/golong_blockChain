package blc

import "fmt"

//查询余额
func (cli *CLI) getBalance(address string) {
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	utxoset := &UTXOSet{blockchain}
	amount := utxoset.GetBalance(address)
	fmt.Printf("%s一共有%d个Token\n", address, amount)
}
