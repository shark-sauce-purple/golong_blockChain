# 基于go开发的公链

本项目实验模仿比特币实现了简易的区块链，实现的内容有区块链的基本构成，pow算法，数据库持久化存储，UTXO，密钥，数字签名，钱包，分布式。

项目代码地址：https://gitee.com/shark-sauce-purple/golong_blockChain.git

## 一、区块链基本构成

### 1.区块的组成：

```go
	//1.区块高度
	Height int64

	//2.上一个区块hash值(前指针)
	PrevBlockHash []byte

	//3.交易数据
	Tx []*Transaction

	//4.时间戳
	Timestamp int64

	//5.哈希
	Hash []byte

	//6.Nonce
	Nonce int64
```

* 其中区块高度指的是当前块是区块链中的第几块

* 时间戳存储当前创建区块的时间

* 交易数据存储了交易的数据

* 哈希等于值由POW算法得出，字长为32字节

* Nonce为随机数，在pow中就是在不断的计算 nonce 值，即对区块 header 不断的运算哈希，直至找到能使区块哈希小于 target 的 nonce。

  ![image-20230430230921357](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230430230921357.png)

  

### 2.区块的结构

![2916760-20220707164619670-1403581417](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/2916760-20220707164619670-1403581417.jpg)

区块是一种容器型的数据结构，聚合了被记录在公开账簿（区块链）里的交易信息。区块由一个包含元数据的区块头和紧跟其后的构成区块主体的一长串交易组成。区块头为80个字节，而平均每个交易至少为250个字节，平均每个区块包含超过500个交易。因此，一个包含所有交易的完整区块比区块头大1000倍。

![区块的结构](C:\Users\HP\Desktop\golong_blockChain-master\pictures\区块的结构.png)

#### 2.1区块头

区块头包含三组区块元数据。首先，有一组引用父区块散列值的数据，这组元数据用于将该区块与区块链中前一区块相连接。第二组元数据，即难度、时间戳和随机数，与挖矿竞争相关。第三块元数据是默克尔树根（merkle tree root），一种数据结构，用于有效地汇总块中的所有交易。

![区块头的结构](C:\Users\HP\Desktop\golong_blockChain-master\pictures\区块头的结构.png)



### 3.创世区块

区块链中的第一个区块被称为创世区块。它是区块链中所有区块的共同祖先，这意味着如果你从任何区块开始并按时序回溯区块链的话，最终都将到达创世区块。

每个节点总是以至少一个区块的区块链开始，因为这个区块是静态编码的，它不能被改变。每个节点总是“知道”起始区块的散列值和结构，它创建的固定时间，甚至是其中的单个交易。因此，每个节点都有区块链的起点，这是一个安全的“根”，从中可以构建可信的区块链

创世区块的高度为1（或为0），前指针为0。

创世区块的散列值为：

```
000000000019d6689c085ae165831e934ff763ae46a2a6c172b3f1b60a8ce26f
```

创世区块包含一个隐藏的信息。在其Coinbase交易的输入中包含这样一句话“The Times 03/Jan/2009 Chancellor on brink of second bailout for banks.”这句话是《泰晤士报》当天的文章标题，引用这句话，既是对该区块产生时间的说明，也可视为半开玩笑地提醒人们一个独立的货币体系的重要性，同时告诉人们随着比特币的发展，一场前所未有的世界性货币危机将要发生。该消息是由比特币的创造者中本聪嵌入创世区块中的。

### 4.区块链

区块链数据结构是一个有序的，反向链接的交易块列表。区块链可以作为平面文件（Flat File）存储，也可以存储在简单的数据库中。

```go
	//最新的区块的哈希值
	Tip []byte
	//数据库
	DB  *bolt.DB
```

#### 4.1初始化区块链

初始化区块链即创建一个带有创世区块的区块链

```go
	if dbExists() {
		//fmt.Println("创世区块已经存在")
		db, err := bolt.Open(dbName, 0600, nil)
		if err != nil {
			log.Fatal(err)
		}
		var blockchain *Blockchain
		err = db.Update(func(tx *bolt.Tx) error {
			b := tx.Bucket([]byte(blockTableName))
			hash := b.Get([]byte("l"))
			blockchain = &Blockchain{hash, db}
			return nil
		})
		if err != nil {
			log.Panic(err)
		}
		return blockchain
```

输出区块链，可以看到第一个创世区块的散列值

![image-20230430214228152](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230430214228152.png)

#### 4.2增加新区块

```go
		//获取表
		b:=tx.Bucket([]byte(blockTableName))
		//创建新区快
		if b!=nil{
			//获取最新区块
			blockBytes := b.Get(blc.Tip)
			//反序列化
			block:=DeserializeBlock(blockBytes)
			//将区块序列号并且存储到数据库中
			newBlock := NewBlock(data, block.Height+1, block.Hash)
			err:=b.Put(newBlock.Hash,newBlock.Serialize())
			if err != nil {
				log.Panic(err)
			}
			//更新数据库中new对于的hash
			err=b.Put([]byte("new"),newBlock.Hash)
			if err != nil {//存储失败
				log.Fatal(err)
			}
			//更新Tip
			blc.Tip = newBlock.Hash
```

## 二、共识算法-POW

目前共识算法有很多，因为能力，时间有限，在此次实验中使用了最经典的共识算法-工作量证明POW

工作量证明(Proof Of Work，简称POW)，简单理解就是一份证明，用来确认你做过一定量的工作。监测工作的整个过程通常是极为低效的，而通过对工作的结果进行认证来证明完成了相应的工作量，则是一种非常高效的方式。

### 1.组成

```go
	//当前要验证的存储
	Block *Block
	//大数据存储
	Target *big.Int
```

### 2.算法原理

简单打个比方，想象人们不断扔一对骰子以得到小于特定点数的游戏。第一局，目标是12。只要不扔出两个6，你就会赢。然后下一局目标为11。玩家只能扔10或更小的点数才能赢，不过也很简单。假如几局之后目标降低为5。现在有一半概率以上扔出来的骰子加起来的点数会超过5，而因此无效。随着目标越来越小，要想赢的话，扔骰子的次数会呈现指数级的上升。最终当目标为2（最小的点数）时，只有平均扔36次或者2%的概率，才能赢。

从一个知道骰子游戏目标为2的观察者的角度来看，如果有人要成功中奖，那么他平均要尝试36次。换句话说，可以从目标难度来估算出成功所需要的工作量。当算法是基于诸如SHA256的确定性函数时，那么必须要有一定的工作量才能产生出低于目标的结果，能够产生正确结果的输入值就是工作量的证明。这就是工作量证明（Proof-of-Work）的由来。

### 3.难度调整

通过python脚本来输出不同随机数生产的散列值(Satoshi Nakamoto是比特币创始人中本聪)

```python
import hashlib
text = "I am Satoshi Nakamoto"
for nonce in range(20):
    input = text + str(nonce)
    hash = hashlib.sha256(input).hexdigest()
    print(input,"=>",hash)
```

输出结果：

```python
I am Satoshi Nakamoto0 => a80a81401765c8eddee25df36728d732...
I am Satoshi Nakamoto1 => f7bc9a6304a4647bb41241a677b5345f...
I am Satoshi Nakamoto2 => ea758a8134b115298a1583ffb80ae629...
I am Satoshi Nakamoto3 => bfa9779618ff072c903d773de30c99bd...
I am Satoshi Nakamoto4 => bce8564de9a83c18c31944a66bde992f...
I am Satoshi Nakamoto5 => eb362c3cf3479be0a97a20163589038e...
I am Satoshi Nakamoto6 => 4a2fd48e3be420d0d28e202360cfbaba...
I am Satoshi Nakamoto7 => 790b5a1349a5f2b909bf74d0d166b17a...
I am Satoshi Nakamoto8 => 702c45e5b15aa54b625d68dd947f1597...
I am Satoshi Nakamoto9 => 7007cf7dd40f5e933cd89fff5b791ff0...
I am Satoshi Nakamoto10 => c2f38c81992f4614206a21537bd634a...
I am Satoshi Nakamoto11 => 7045da6ed8a914690f087690e1e8d66...
I am Satoshi Nakamoto12 => 60f01db30c1a0d4cbce2b4b22e88b9b...
I am Satoshi Nakamoto13 => 0ebc56d59a34f5082aaef3d66b37a66...
I am Satoshi Nakamoto14 => 27ead1ca85da66981fd9da01a8c6816...
I am Satoshi Nakamoto15 => 394809fb809c5f83ce97ab554a2812c...
I am Satoshi Nakamoto16 => 8fa4992219df33f50834465d3047429...
I am Satoshi Nakamoto17 => dca9b8b4f8d8e1521fa4eaa46f4f0cd...
I am Satoshi Nakamoto18 => 9989a401b2a3a318b01e9ca9a22b0f3...
I am Satoshi Nakamoto19 => cda56022ecb5b67b2bc93a2d764e75f...
```

类似这样在语句末尾的变化的数字叫作随机数（nonce）。随机数是用来改变加密函数输出的

设定一个具有任意性的目标：找到一个语句，使其散列值的十六进制表示以0开头。

语句“I am Satoshi Nakamoto13”的散列值是0ebc56d59a34f5082aaef3d66b37a661696c2b618e62432727216ba9531041a5，刚好满足条件。我们得到它用了13次。从概率的角度来看，如果散列函数的输出是平均分布的，我们可以期望每16次得到一个以0开头的散列值（十六进制个位数字为0到F）。

从数字的角度来看，我们要找的是小于0x1000000000000000000000000000000000000000000000000000000000000000的散列值，我们称之为目标（**target**）阈值，我们的目的是找到一个小于这个目标阈值的散列值。如果我们减小这个目标阈值，那找到一个小于它的散列值会越来越难。

**因此要调整难度，即减小目标阈值即可**

在上述例子中，成功的随机数为13，且这个结果能被所有人独立确认。任何人将13加到语句“I am Satoshi Nakamoto”后面再计算散列值都能确认它比目标值要小。这个正确的结果同时也是工作量证明，因为它证明我们的确花时间找到了这个随机数。验证这个散列值只需要一次计算，而我们找到它却花了13次。如果目标值更小（难度更大），那我们需要多得多的散列计算才能找到合适的随机数，但其他人验证它时只需要一次散列计算。此外，知道目标值后，任何人都可以用统计学来估算其难度，因此就能知道找到这个随机数需要多少工作量。

产生小于目标的散列值才能算是工作量证明。**更高的目标意味着找到低于目标的散列值是不太困难的。较低的目标意味着在目标下方找到散列值更加困难。目标和难度成反比**。

#### 代码实现难度调整-移位

假设散列值总长为8：

0000 0000 =>可以看出有2^8次方种可能

target值为2，则有新值

0100 0000=>有2^6次方种可能

因此可以随着移位来实现调整难度，数值越大，难度越高

```go
	//初始化target
	target:=big.NewInt(1)

	//移位256-targetBit
	target=target.Lsh(target,256-targetBit)
```

### 4.pow运行

可以看到`nonce`从0开始计数，当符合条件后nonce停止+1

```go
		//1.将block的属性拼接成字节
		dataBytes := ProofOfWork.prepareData(int64(nonce))
		//2.生成hash
		hash = sha256.Sum256(dataBytes)
		//存储到hashInt
		hashInt.SetBytes(hash[:])
		//3.判断hash有效性，如果满足条件，跳出循环（小于target）
		// 	-1 if x <  y
		// 	0 if x == y
		//   +1 if x >  y
		if ProofOfWork.Target.Cmp(&hashInt) == 1 {
			break	
		}
		nonce = nonce + 1
```

### 验证哈希是否正确

哈希值，又称:散列函数(或散列算法，又称哈希函数，英语:Hash Function)是一种从任何一种数据中创建小的数字"指纹"的方法。散列函数把消息或数据压缩成摘要，使得数据量变小，将数据的格式固定下来。该函数将数据打乱混合，重新创建一个叫做散列值(hash values，hash codes，hash sums，或hashes)的指纹。散列值通常用一个短的随机字母和数字组成的字符串来代表。好的散列函数在输入域中很少出现散列冲突。在散列表和数据处理中，不抑制冲突来区别数据，会使得数据库记录更难找到。

可以得出hash值是唯一且确定的，因为hash是非常非常大的，想要出现相同的hash值（`哈希碰撞`）是几乎不可能的

hash碰撞：两个不同的值经过hash计算后，得到的hash值相同，后来的要放到原来的位置，但是数组的位置已经被占了，导致冲突。



```go
func (ProofOfWork *ProofOfWork) Validate() bool {
	var hashInt big.Int
	hashInt.SetBytes(ProofOfWork.Block.Hash)
	//判断hash有效性，如果满足条件，跳出循环（小于target）
		// 	-1 if x <  y
		// 	0 if x == y
		//   +1 if x >  y
	return  ProofOfWork.Target.Cmp(&hashInt) == 1
}
```

挖两个区块后验证第二个区块的hash值是否正确

![image-20230501011900603](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230501011900603.png)

## 三、数据库-boltdb

区块链使用**键值对**数据库

go中键值对数据库：boltdb

### 介绍boltdb

Bolt 是由 Howard Chu 的 LMDB(https://symas.com/lmdb/technical/) 项目启发的一个纯粹的 Go key/value 数据库。 该项目的目标是为不需要完整数据库服务器（如 Postgres 或MySQL）的项目提供一个简单，快速和可靠的数据库。

Bolt 一次只允许一个读写事务，但是一次允许多个只读事务。 每个事务处理都有一个始终如一的数据视图。

单个事务以及从它们创建的所有对象（例如 bucket，key）不是线程安全的。 要处理多个 goroutine 中的数据。

只读事务和读写事务不应该相互依赖，一般不应该在同一个例程中同时打开。 这可能会导致死锁，因为读写事务需要定期重新映射数据文件，但只有在只读事务处于打开状态时才能这样做。

### 安装boltdb

在程序终端下输入

```go
go get github.com/boltdb/bolt/...
```

![image-20230501114115912](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230501114115912.png)

安装完后，出现一个go.sum文件

![image-20230501114231124](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230501114231124.png)

### 数据库的使用

官方文档 www.github.com/boltdb/bolt/

![image-20230501115508533](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230501115508533.png)

创建数据库

```go
db, err := bolt.Open("my.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
```

创建表

```go
err = db.Update(func(tx *bolt.Tx) error {
		//创建BlockBucket，b为表
		b, err := tx.CreateBucket([]byte("BlockBucket"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		if b!=nil{
			err:=b.Put([]byte("l"),[]byte(",,,,,"))
			if err!=nil{
				fmt.Println("更新失败")
			}
		}
		return nil
	})
```

更新表

```go
db.Update(func(tx *bolt.Tx) error {
	b := tx.Bucket([]byte("BlockBucket"))//读取
    err := b.Put(key []byte, value []byte))//添加数据
	return err
})
```

查看表

```go
err = db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("BlockBucket"))
		if b!=nil{
			v := b.Get([]byte("1"))
			fmt.Printf("The answer is: %s\n", v)
			block:=blc.DeserializeBlock(v)
			fmt.Printf("The answer is: %v\n", block)
		}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}
```

### 大体步骤

1、序列化为[]byte

2、以当前区块hash为key，以序列化以后的字节数组为value

3、db.put(block.Hash,block.Seri0)-->存储完后为键值对

4、读取时，反序列化

### 序列化&反序列化

```go
//将区块序列化字节数组
func (block *Block) Serialize() []byte {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(block)
	if err != nil {
		log.Panic(err)
	}

	return result.Bytes()
}

//反序列化
func DeserializeBlock(d []byte) *Block {
	var block Block

	decoder := gob.NewDecoder(bytes.NewReader(d))
	err := decoder.Decode(&block)
	if err != nil {
		log.Panic(err)
	}

	return &block
}
```

### 遍历输出所有区块链

通过保存在blockchain中的`Tip`，可以看到最新的区块的hash值，根据最新的区块hash值可以进行反序列化逐渐向前迭代输出。

```go
	var block *Block
	var currentHash []byte = blc.Tip //当前的hash值
	for {
		err := blc.DB.View(func(tx *bolt.Tx) error {
			//读取表
			b := tx.Bucket([]byte(blockTableName))
			if b != nil {
				//获取当前区块的字节数组
				blockBytes := b.Get(currentHash)
				//反序列
				block = DeserializeBlock(blockBytes)
				//输出
		block:=BlockchainIterator.Next()
		fmt.Printf("Height:%d\n",block.Height)
		fmt.Printf("PrevBlockHash:%x\n",block.PrevBlockHash)
		fmt.Printf("Data:%s\n",block.Data)								
		fmt.Printf("Timestamp:%s\n",time.Unix(block.Timestamp,0).Format("2006-01-02 03:04:05 PM"))
		fmt.Printf("Hash:%x\n",block.Hash)
		fmt.Printf("Nonce:%d\n",block.Nonce)
			}
			return nil
		})
		if err != nil {
			log.Panic(err)
		}
		//判断是否为创世区块,如若不是，继续
		var hashInt big.Int
				hashInt.SetBytes(block.PrevBlockHash)
		if big.NewInt(0).Cmp(&hashInt)==0{
			break
		}
		currentHash = block.PrevBlockHash
		fmt.Println()
```

main.go:

```go
	//创建创世区块链
	genesisBlockchain := blc.CreateGenesisBlockchain()
	defer genesisBlockchain.DB.Close()

	//新区块
	genesisBlockchain.AddBlockToBlockchain("123")
	genesisBlockchain.AddBlockToBlockchain("1234")
	//输出
	genesisBlockchain.PrintChain()

```



输出值：

![image-20230501150855038](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230501150855038.png)

### 使用迭代器遍历区块链

创建迭代器

```go
//迭代器
type BlockchainIterator struct{
	currentHash []byte
	DB *bolt.DB
}
//创建迭代器
func (bc *Blockchain) Iterator() *BlockchainIterator {
	bci := &BlockchainIterator{bc.Tip, bc.DB}
	return bci
}
```

下一个

```go
func (blockchainIterator *BlockchainIterator) Next() *Block{
	var block *Block
	err:=blockchainIterator.DB.View(func(tx *bolt.Tx) error{
		b := tx.Bucket([]byte(blockTableName))
			if b != nil {
			currentBlock:=b.Get(blockchainIterator.currentHash)
			//获取当前迭代器里面所对应区块
			block=DeserializeBlock(currentBlock)
			//更新迭代器
			blockchainIterator.currentHash = block.PrevBlockHash
			}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}
	return block
}
```



### 区块链在数据库中的存储形势

区块链数据结构是一个有序的，反向链接的交易块列表。区块链可以作为平面文件（Flat File）存储，也可以存储在简单的数据库中。

区块链是一个分布式数据库。这意味着数据分散在节点(参与的计算机)上。每个节点都可以决定如何存储数据(如果存储数据的话)。

## 四、UTXO&交易1

交易输出是不可分割的数字币货币，记录在区块链上，并被整个网络识别为有效。数字币完整节点跟踪所有可用和可消费的输出，称为“未花费的交易输出”（`unspent transaction outputs`），即UTXO。所有UTXO的集合被称为UTXO集。

### UXTO模型

在之前编写时，为了方便一直以data type【】来代替交易数据，现在来编写交易数据transaction,并将之前的data换成transaction

![image-20230520164935302](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230520164935302.png)

#### 什么是UTXO？

* 电子记账的物理现金，模仿实物货币的记账货币
* 签名转账：前一笔交易和下一位所有人的签名
* 假如有50的utxo，要付30，则创造两个新的utxo一个30，一个20，30给交易方，20给自己。且记录都记在账本上
* 现有输入，再有输出，如A向B输入30，A原来有100，则有输出A->B 30,A->A 70

#### 转帐步骤中的input，output

现A有10块钱，随后A向B转5块，在随后B向C转3块钱。

则有过程：

```go
A->B ,5
input{交易ID,输出索引,A} 
output{5,B}
output{10-5,A}

B->c,3
input{交易ID,输出索引,B} 
output{3,C}
output{5-3,B}
```

#### transaction_input

```go
	//交易的hash
	TxHash      []byte
	//存储TXout在Vout里的索引
	Vout      int
	//用户名
	ScriptSig string

	// Signature []byte
	// PubKey    []byte
```

#### transaction_output

```go
	//值
	Value int
	//公钥
	//PubKeyHash []byte
	//用户名
	ScriptPubKey string
```

#### transaction的结构

```go
type Transaction struct {
	//1.交易hash
	TxHash []byte
	//2.输入
	Vins  []*TXInput
	//3.输出
	Vouts []*TXOutput
}
```

#### 把transaction的数据转换成字节数组

```go
	var TxHashes [][]byte
	var TxHash [32]byte
	for _, tx := range block.Txs {
		TxHashes = append(TxHashes, tx.TxHash)
	}
	TxHash = sha256.Sum256(bytes.Join(TxHashes, []byte{}))
	return TxHash[:]
```

#### 输出transaction的内容

此时并没有写公私钥

```go
fmt.Printf("Txs\n")
		for _, tx := range block.Txs {
			fmt.Printf("%x\n",tx.TxHash)
			for _,in:=range tx.Vins{
				fmt.Printf("HASH:\t%x\n",in.TxHash)
				fmt.Printf("Vout:\t%d\n",in.Vout)
				fmt.Printf("Scriptsig:\t%s\n",in.ScriptSig)
			}
			for _,out:=range tx.Vouts{
				fmt.Printf("Value:\t%d\n",out.Value)
				fmt.Printf("ScriptPubKey\t%s\n",out.ScriptPubKey)
			}
		}
```

#### 把JSON传转换成数组串

在传送交易信息时，是`json`格式，需要转换成数组。

```go
func JSONTOArry(js string)[]string{

	//json-》数组
	var sArr []string
	if err:=json.Unmarshal([]byte(js),&sArr);err!=nil{
		log.Panic(err)
	}
	return sArr
}
```

**！注意此处有一个陷阱，json字符串有关键词在输入到关键词时需要转义**

> 需要转义的	" : \ {} [] --
>
> -->转义为 \" \: \\ \{ \}

#### 挖掘新的区块（建立新的Transation）

增加区块之前已经写过了，现在着重处理交易建立（需注意因为更改了区块数据，并且建立了数据库，因此源代码要进行改变)

```go
func (Blockchain *Blockchain) MineNewBlock(from , to , amount []string) {
	//建立一笔交易
	value, _ := strconv.Atoi(amount[0])
	tx := NewSimpletransaction(from[0], to[0], value)

	//建立Transaction数组
	var txs []*Transaction
	txs = append(txs, tx)


	var block *Block
	Blockchain.DB.View(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			//读取最新的block
			hash := b.Get([]byte("new"))
			blockBytes := b.Get(hash)
			block = DeserializeBlock(blockBytes)

		}
		return nil
	})

	//建立新的区块
	block = NewBlock(txs, block.Height+1, block.Hash)

	//存储到数据库中
	Blockchain.DB.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket([]byte(blockTableName))
		if b != nil {
			b.Put(block.Hash, block.Serialize())
			b.Put([]byte("new"), block.Hash)
			Blockchain.Tip = block.Hash
		}
		return nil
	})

}
```





### 转帐算法实现

#### 测试：第一笔交易的实现（静态实现）

```go
func NewSimpletransaction(from, to string, amount int) *Transaction {

	//$./main send -from '[\"测试2号\"]' -to '[\"测试3号\"]' -amount '[\"4\"]'
    
	var txIntputs []*TXInput
	var txoutputs []*TXOutput
	//消费
	b, _ := hex.DecodeString("995ada10ae5c9d1d537505e61e0d3d1bb34f20456f24c9dc1ff5d66aba6bb331") //创世区块的交易hash
	txinput := &TXInput{b, 0, from} //上一个区块的hash，索引，来自
	txIntputs = append(txIntputs, txinput)

	//转帐
	txoutput := &TXOutput{amount, to}
	txoutputs = append(txoutputs, txoutput)

	//找零
	txoutput = &TXOutput{10 - amount, form}
	txoutputs = append(txoutputs, txoutput)

	txCoinbase := &Transaction{[]byte{}, txIntputs, txoutputs}

	//此处hash为将自己序列化(设置hash指)
	txCoinbase.HashTransactions()

	return txCoinbase

}
```

输出：

创世区块&第一笔测试&输出

```go
￥go build main.go

￥./main createblockchain -address "2-325"

￥./main send -from '[\"测试1号\"]' -to '[\"测试2号\"]' -amount '[\"4\"]'

￥./main printchain
```

可以看到区块一的transaction的hash值出现在了第二笔交易的`Vins`的I`nTxhash`中说明此步骤是正确的,且交易数值也是正确的。

![image-20230502001947082](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502001947082.png)

再次进行一次静态转账后，可以的到三条数据：

```pascal
Height:3
PrevBlockHash:001cec19fdbd4cd324a4292e955a6522bf9adb38f459087ec584c1f53ff2ddb2
Timestamp:2023-05-02 12:30:32 AM
Hash:002c0ba08559649fd735ba4f59adb158eca8aa159d69ac424f31b1e7fb91609a
Nonce:652
Txs
        TxHash: 6f5cfd5dbad32efdf23c60b9198b9c8eb932dd93d377af72182177cf1a9f51b2
        InTxHASH: 53ff52493255663c01a7817170cf4773de4a263cc4fbfc97aedfb7c29d30d1f6
        Vout:1
        INScriptsig:测试2号
        Value:2
        OutScriptPubKey:测试3号
        Value:2
        OutScriptPubKey:测试3号
---------------------------------------------

Height:2
PrevBlockHash:001454b9aac31f26769b091d71bfef3c867e9c924fe7ecb8046055b94cd60d93
Timestamp:2023-05-02 12:28:26 AM
Hash:001cec19fdbd4cd324a4292e955a6522bf9adb38f459087ec584c1f53ff2ddb2
Nonce:1248
Txs
        TxHash: 53ff52493255663c01a7817170cf4773de4a263cc4fbfc97aedfb7c29d30d1f6
        InTxHASH: 995ada10ae5c9d1d537505e61e0d3d1bb34f20456f24c9dc1ff5d66aba6bb331
        Vout:0
        INScriptsig:测试1号
        Value:4
        OutScriptPubKey:测试2号
        Value:6
        OutScriptPubKey:测试2号
---------------------------------------------

Height:1
PrevBlockHash:0000000000000000000000000000000000000000000000000000000000000000
Timestamp:2023-05-02 12:28:22 AM
Hash:001454b9aac31f26769b091d71bfef3c867e9c924fe7ecb8046055b94cd60d93
Nonce:1291
Txs
        TxHash: 995ada10ae5c9d1d537505e61e0d3d1bb34f20456f24c9dc1ff5d66aba6bb331
        InTxHASH:
        Vout:-1
        INScriptsig:创世区块
        Value:10
        OutScriptPubKey:2-325
         
```



#### 得到所剩的钱

在交易中，明确了要给对方多少钱，但是自己所持有的钱是存在以前的`Transaction`中的，所以需要查看以前的`Transaction`，来将自己未花费的钱记住。

##### 遍历UTXO

为了要查找所剩的钱，因此要遍历UTXO

怎么判断有效数据，或者说这笔钱是我现在能用的？

假设现在有四个区块，三笔交易，如下图。我们可以知道`先有input`，`后有output`。

![image-20230502164628671](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502164628671.png)

因此我们可以先将属于我的所有input存储起来

```go
	sepentTXOutputs:=make(map[string][]int 0)
	for _,in:=range tx.Vins{
				//判断这笔钱是不是所要判断的人输入的
				if in.INUnLockWithAddress(address){
					//转换
					key:=hex.EncodeToString(in.TxHash)
					//存入
					sepentTXOutputs[key] = 		append(sepentTXOutputs[key],in.Vout)
				}
			}
```

随后在与`outpu`t进行比对，当`output`中的记录有在`input`的索引中记录，说明这笔钱是花出去了。没有记录的，那就是还剩的钱，将其记录在`UTXOspent`数组中

```go
work:
			for index, out := range tx.Vouts {
				//判断这笔钱是不是所要判断的人输出的
				if out.OUTUnLockWithAddress(address) {
					fmt.Println(out)
					fmt.Println(sepentTXOutputs)
					if sepentTXOutputs != nil {
						if len(sepentTXOutputs) != 0 { //判断长度是否为0
							var isSpendUTXO bool
							for txHash, indexArray := range sepentTXOutputs { //txHash为input的hash
								for _, i := range indexArray {
									//说明这比钱已经被花费
									if index == i && txHash == hex.EncodeToString(tx.TxHash) {
										isSpendUTXO = true
										continue work
									}
								}
							}
							if isSpendUTXO == false {
								utxo := &UTXO{tx.TxHash, index, out}
								unUTXOs = append(unUTXOs, utxo)
							}

						} else {
							utxo := &UTXO{tx.TxHash, index, out}
							unUTXOs = append(unUTXOs, utxo)
						}
					}
				}
			}
		}
```

为了方便转帐，我们在建立一个结构体`UTXO`

```go
	TxHash []byte
	index int
	Outputs  *TXOutput
```

并将遍历里面的`transaction`数组换成`UTXO`

查询余额函数：

```go
func (Blockchain *Blockchain) GetBalance(address string) int64 {
	utxos := Blockchain.UnUTXOs(address)
	var amount int64
	for _, utxo := range utxos {
		amount += int64(utxo.Outputs.Value)
	}
	return amount
}
```

##### 实现转帐

在转账时，我们需要查找可以使用的`UTXO`，来使用里面的钱（token）,并且将可以用的`UTXO`存储下来。供进行交易时使用

```go
//转帐时查询from可以的UTXO
func (Blockchain *Blockchain) FindSpentableUTXOS(from string,amount int)(int,map[string][]int){
	//1.获取所有的UTXO
	utxos:=Blockchain.UnUTXOs(from)

	//2.遍历utxos,得到钱
	var value int
	spendAbleUTXO:=make(map[string] []int)
	for _,utxo:=range utxos{
		value = value+utxo.Outputs.Value
		hash:=hex.EncodeToString(utxo.TxHash)//获取utxo的哈希
		spendAbleUTXO[hash] = append(spendAbleUTXO[hash], utxo.index)//存入utxo
		//如果钱够了，直接跳出循环
		if value>=amount{
			break
		}
	}
	if value<amount{
		fmt.Printf("%s余额不足，无法转账，现有余额为%d",from,value)
		os.Exit(1)
	}
	return value,spendAbleUTXO
}
```

###### 存储多笔input

```go
//返回可以用的UTXO
money, spentableUTXODic := bloBlockchain.FindSpentableUTXOS(from,amount)
var txIntputs []*TXInput
for txhash, indexArray := range spentableUTXODic {
	txhashBytes,_ := hex.DecodeString(txhash)
	//循环得到输入
	for _, index := range indexArray {
		TXInput := &TXInput{txhashBytes, index, from}
		//将输入存进Transaction
		txIntputs = append(txIntputs, TXInput)
	}
}
./main send -from '[\"测试3号\"]' -to '[\"测试2号\"]' -amount '[\"11\"]'
./main getbalance -address "测试2号" 
./main getbalance -address "测试3号" 
```

###### 动态转账

转账跟之前静态的最大不同就是，自己判断所剩`token`，根据上两个函数可以计算出所剩的`UTXO`与多笔交易的数组。随后根据数组进行交易。

```go
//交易的实现
func NewSimpletransaction(from, to string, amount int, bloBlockchain *Blockchain) *Transaction {


```

/返回from的所有未花费输出所对应的`Transaction`
	//unUTXOs := bloBlockchain.UnUTXOs(from)

````go

	//返回可以用的UTXO
	money, spentableUTXODic := bloBlockchain.FindSpentableUTXOS(from, amount)

	//unSpentTx:=UnU(from)
	//fmt.Println(unSpentTx)

	// //money,dic:={}
	var txIntputs []*TXInput
	var txoutputs []*TXOutput
	//消费
	//b, _ := hex.DecodeString("53ff52493255663c01a7817170cf4773de4a263cc4fbfc97aedfb7c29d30d1f6 ") //创世区块的交易hash
	for txhash, indexArray := range spentableUTXODic {
		txhashBytes, _ := hex.DecodeString(txhash)
		//循环得到输入
		for _, index := range indexArray {
			TXInput := &TXInput{txhashBytes, index, from}
			//将输入存进Transaction
			txIntputs = append(txIntputs, TXInput)
		}

	}

	//转帐
	txoutput := &TXOutput{amount, to}
	txoutputs = append(txoutputs, txoutput)

	//找零
	txoutput = &TXOutput{money - amount, from}
	txoutputs = append(txoutputs, txoutput)

	tx := &Transaction{[]byte{}, txIntputs, txoutputs}

	//此处hash为将自己序列化(设置hash指)
	tx.HashTransactions()

	return tx

}

````

###### 建立多笔交易

在原来的建立新区块中建立交易的函数为:

```go
tx := NewSimpletransaction(from[0], to[0], value,Blockchain)
```

这只是建立一笔，要建立多笔交易要借助循环,并且其它函数还要考虑多笔`Transaction`，因此要在其它相关函数的接受变量中还要增加transaction数组。

```go
//建立Transaction数组
var txs []*Transaction
//建立交易
for index, address := range from {
	value, _ := strconv.Atoi(amount[index])
	tx := NewSimpletransaction(address, to[index], value, Blockchain, txs)
	txs = append(txs, tx)
	fmt.Println("交易tx:", tx)
}

```

在实现交易里，将`input`改为多笔

```go
for txhash, indexArray := range spentableUTXODic {
		txhashBytes, _ := hex.DecodeString(txhash)
		//循环得到输入
		for _, index := range indexArray {
			TXInput := &TXInput{txhashBytes, index, from}
			//将输入存进Transaction
			txIntputs = append(txIntputs, TXInput)
		}

	}
```

进行转账和找零，更改`output`

```go

	//转帐
	txoutput := NewTXOutput(amount, to)

	txoutputs = append(txoutputs, txoutput)

	//找零
	txoutput = NewTXOutput(money-amount, from)
	txoutputs = append(txoutputs, txoutput)
```

最后设置`hash`,返回hash就大公告成了

```go
//此处hash为将自己序列化(设置hash指)
	tx.HashTransactions()


	return tx

```





## 五、密匙&地址(密码学部分)

​	比特币的所有权是通过数字密钥、比特币地址和数字签名建立起来的。数字密钥实际上并不存储在网络中，而是由用户创建，并且存储在一个叫作钱包的文件或者简单的数据库中。用户钱包中的数字密钥完全独立于比特币协议，可以由用户的钱包软件生成和管理，而无须参考区块链或接入互联网。密钥使比特币的许多有趣特性得以实现，包括去中心化的信任和控制、所有权认证和基于密码学验证的安全模型。

### 密匙对

#### 为什么使用密匙对？

一个比特币钱包中包含一组密钥对，每个密钥对包含一个私钥和一个公钥。`私钥（k）`是一个数字，通常是随机选择的。基于私钥可以使用椭圆曲线乘法这个单向加密函数来生成一个`公钥`（K）。基于`公钥`（K），可以使用单向加密散列函数来生成一个`比特币地址`（A）。在这一节中，我们将从生成私钥开始，介绍如何用椭圆曲线运算将私钥转换成一个公钥，最后，如何从公钥生成比特币地址。私钥、公钥和比特币地址之间的关系如图所示

![image-20230502224355747](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502224355747.png)

为什么在比特币中使用非对称加密？它并不被用于对比特币交易“加密”（保密）。相反，非对称加密技术最有用的特性是能够生成数字签名。可以将私钥应用到比特币交易的数字指纹上，以生成数字签名。该签名只能由拥有私钥的人生成。但是任何可以访问其公钥和交易指纹的人都可以使用它们来验证签名的真伪。这种非对称加密的有用特性使得任何人都可以验证每个交易上的每个签名，同时确保只有私钥的所有者才能生成有效的签名。

**简单来说：我产生一笔交易，用私钥产生公钥，公钥产生地址。私钥别人无法拥有，而公钥别人是拥有的。当别人想要验证我产生的这笔交易是否合法时，用公钥看是否能产生地址，如果可以，那么就是真的**

>疑问，为什么不怕暴力破解而把私钥试出来呢？
>
>这就要来讨论公私钥所用的密码学

#### SHA256

SHA（Secure Hash Algorithm安全散列算法）是一个密码散列函数的家族，是FIPS（联邦信息处理标准 Federal Information Processing Standards）所认证的安全散列算法。能计算出一个数字消息所对应到的，长度固定的字符串（又称消息摘要）的算法。

一个n位的哈希函数就是一个从任意长的消息到n位哈希值的映射，一个n位的加密哈希函数就是一个单向的、避免碰撞的n位哈希函数。这样的函数是目前在数字签名和密码保护当中极为重要的手段。

SHA256，其字面意思便是，对于任意长度的消息，SHA256都会产生一个256位的哈希值，称作消息摘要。这个摘要相当于是个长度为**32个字节**的数组，共256位，通常由一个长度为64的十六进制字符串来表示，其中1个字节=8位，一个十六进制的字符的长度为4位。

##### go实例：

```go
	hasher := sha256.New()
	hasher.Write([]byte("要转换的内容"))
	bytes:=hasher.Sum(nil)
	fmt.Println(bytes)
```

![image-20230502230626881](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502230626881.png)

#### ripemd160

IPEMD(RACE原始完整性校验讯息摘要)是一种加密哈希函数，由鲁汶大学的 Hans Dobbertin,Antoon Bosselaers 和 Bart Prenee 组成的 COSIC 研究小组发布于1996 年。 RIPEMD 是以 MD4 为基础原则所设计的 ，而且其表现与更有名的 `SHA-1` 类似。

`RIPEMD-160 `是以原始版 RIPEMD 所改进的 160 位元版本，而且是 RIPEMD 系列中最常见的版本。 RIPEMD-160 是设计给学术社群所使用的，刚好相对于国家安全局 所设计 SHA-1 和 SHA-2 算法。 另一方面，RIPEMD-160 比 SHA-1 较少使用，所以可能造成 RIPEMD-160 比 SHA 还不常被审查。另外，RIPEMD-160并没有任何专利所限制。

`RIPEMD160 `哈希值的输出值一般是 16 进制的字符串。而16进制字符串，每两个字符占一个字节。我们知道，一个字节是 8bit，所以使用 ripemd160 加密函数所得到的是一个 160bit 的值。

##### go安装

```go
 go get "golong.org/x/crypto/ripemd160"
```

```go
	hasher := ripemd160.New()
	hasher.Write([]byte("要转换的内容"))
	hashBytes := hasher.Sum(nil)
	hashString := fmt.Sprintf("%x", hashBytes)
	fmt.Println(hashString)
```





#### 椭圆曲线密码学

**ECC（Elliptic curve cryptography）**，官方命名**“椭圆曲线密码学”**，也称为我们理解的椭圆曲线加密算法，是一种基于椭圆曲线数学的建立公开密钥加密的算法，也是一种非对称加密算法。

椭圆曲线密码学（ECC）是一种基于有限域上椭圆曲线代数结构的公钥密码学方法ECC，允许使用更小的密钥来提供等效的安全性。

椭圆曲线适用于密钥协商、数字签名、伪随机生成器等任务，它们可以通过将密钥协议与对称加密方案相结合来用于加密。
![image-20230502225456649](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502225456649.png)



![image-20230502225512081](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502225512081.png)

述mod p（质数p取模）表明该曲线是在素数阶p的有限域内，也写作Fp，其中p=2256-232-29-28-27-26-24-1，是非常大的质数。因为这条曲线被定义在一个质数阶的有限域内，而不是定义在实数范围，它的函数图像看起来像分散在两个维度上的离散的点，因此很难可视化。然而，其中的数学原理与实数范围内的`椭圆曲线`相似。作为一个例子，图4-3显示了在一个小了很多的质数阶17的有限域内的椭圆曲线，其形式为网格上的一系列散点。而`secp256k1`的比特币椭圆曲线可以被想象成一个极大的网格上一系列更为复杂的散点。

![image-20230502225707315](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502225707315.png)



### 地址

#### base58

Base58是用于Bitcoin中使用的一种独特的编码方式，主要用于产生Bitcoin的钱包地址。相比Base64，Base58不使用数字"0"，字母大写"O"，字母大写"I"，和字母小写"l"，以及"+“和”/"符号。

Base58 编解码代码如下：

```go
package main

import (
	"bytes"
	"math/big"
)

var b58Alphabet = []byte("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz")

// 字节数组转base58（加密）
func Base58Encode(input []byte) []byte {
	var result []byte

	x := big.NewInt(0).SetBytes(input)

	base := big.NewInt(int64(len(b58Alphabet)))
	zero := big.NewInt(0)
	mod := &big.Int{}

	for x.Cmp(zero) != 0 {
		x.DivMod(x, base, mod)
		result = append(result, b58Alphabet[mod.Int64()])
	}

	
	if input[0] == 0x00 {
		result = append(result, b58Alphabet[0])
	}

	

	return result
}

// 解密
func Base58Decode(input []byte) []byte {
	result := big.NewInt(0)

	for _, b := range input {
		charIndex := bytes.IndexByte(b58Alphabet, b)
		result.Mul(result, big.NewInt(58))
		result.Add(result, big.NewInt(int64(charIndex)))
	}

	decoded := result.Bytes()

	if input[0] == b58Alphabet[0] {
		decoded = append([]byte{0x00}, decoded...)
	}

	return decoded
}

```



#### base64

Base64是网络上最常见的用于传输8Bit字节码的编码方式之一，Base64就是一种基于64个可打印字符来表示二进制数据的方法。

Base64编码是从二进制到字符的过程，可用于在HTTP环境下传递较长的标识信息。采用Base64编码具有不可读性，需要解码后才能阅读。

Base64由于以上优点被广泛应用于计算机的各个领域，然而由于输出内容中包括两个以上“符号类”字符（+, /, =)，不同的应用场景又分别研制了Base64的各种“变种”。为统一和规范化Base64的输出，Base62x被视为无符号化的改进版本。

```go
input := []byte("hello world")
	fmt.Println("原始内容：" + string(input))

	// 演示base64编码
	encodeString := base64.StdEncoding.EncodeToString(input)
	fmt.Println("编码：" + encodeString)

	// 对上面的编码结果进行base64解码
	decodeBytes, err := base64.StdEncoding.DecodeString(encodeString)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("解码：" + string(decodeBytes))

	// 如果要用在url中，需要使用 URLEncoding
	uEnc := base64.URLEncoding.EncodeToString([]byte(input))
	fmt.Println("url编码：" + uEnc)

	uDec, err := base64.URLEncoding.DecodeString(uEnc)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("url解码：" + string(uDec))
```



#### 公钥-->地址

公钥是与私钥算法一起使用的密钥对的非秘密一半。公钥通常用于加密会话密钥、验证数字签名，或加密可以用相应的私钥解密的数据。公钥和私钥是通过一种算法得到的一个密钥对(即一个公钥和一个私钥)，其中的一个向外界公开，称为公钥；另个自己保留，称为私钥。通过这种算法得到的密钥对能保证在世界范围内是唯一的。使用这个密钥对的时候,如果用其中一个密钥加密一段数据，必须用另一个密钥解密。如用公钥加密数据就必须用私钥解密，如果用私钥加密也必须用公钥解密，否则解密将不会成功

![](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230502231600711.png)

## 六、钱包

“钱包”一词在比特币中有多重含义。广义上讲，钱包是一个应用程序，为用户提供交互界面。钱包控制用户访问权限，管理私钥和地址，跟踪余额以及创建和签名交易。狭义上讲，从程序员的角度来看，“钱包”是指用于存储和管理用户私钥的数据结构。

关于比特币的一个常见误解是比特币钱包包含比特币。实际上，钱包只包含密钥。比特币被记录在比特币网络的区块链中。用户通过使用钱包中的密钥签署交易来控制网络上的比特币。从某种意义上说，比特币钱包是一个钥匙扣（keychain）。 	

比特币钱包只包含私钥，而不包含比特币。每个用户都有一个包含多个密钥的钱包。钱包实际上是包含私钥/公钥对的钥匙串。用户使用私钥签名交易，从而证明他们拥有交易（比特币）转账所有权。比特币以交易记录（通常记为vout或txout）的形式存储在区块链中。

有两种主要类型的钱包，区别在于它们包含的多个私钥是否相互关联。

第一种类型是非确定性钱包（nondeterministic wallet），其中每个私钥都是从随机数独立生成的，密钥彼此之间无关联。这种钱包也称为简单钥匙串（Just a Bunch Of Keys，一堆私钥的集合），简称JBOK钱包。

第二种类型是确定性钱包（deterministic wallet），其中所有的私钥都是从一个主私钥派生出来，这个主私钥即为种子（seed）。该类型钱包中所有私钥都相互关联，如果有原始种子，则可以再次生成全部私钥。确定性钱包有许多不同的密钥派生方法。最常用的派生方法是使用树状结构，称为分层确定性钱包或HD钱包。

确定性钱包由种子派生出来的。为了便于使用，种子被编码为英文单词，也称为助记词。

### 公钥获得地址的详细过程：

1.公钥进行一次`256hash`，在进行一次`160hash`=》得到20字节[]byte

2.Version{0}+hash160 ->pubky

3.两次hash

4.取hash最后的四个字节（校验和）

5.Version{0}+hash160 +4个字节=》25个字节

5.`base58`编码

![image-20230503000726841](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230503000726841.png)



### wallet

加密领域的 Wallet 允许用户存储和转移数字资产。从功能的角度来看——Wallet 可以被视为用户在区块链世界的在线账户。

**钱包是存储和使用数字货币的工具，在区块链领域有举足轻重的地位。**密钥的管理工具，它只包含密钥而不是确切的某一个代通证；钱包中包含成对的私钥和公钥，用户用私钥来签名交易，从而证明该用户拥有交易的输出权；而输出的交易信息则存储在区块链中；用户在使用钱包时, 你的 Keystore, 助记词, 明文私钥, 都是钱包；Keystore 是你加了”锁”的钱包，而助记词和明文私钥是完全暴露在外的钱包，没有任何安全性可言，所以在使用助记词和明文私钥时，一定要注意保密。

将一个公钥转换成一个 Base58 地址需要以下步骤：

1. 使用 `RIPEMD160(SHA256(PubKey))` 哈希算法，取公钥并对其哈希两次
2. 给哈希加上地址生成算法版本的前缀
3. 对于第二步生成的结果，使用 `SHA256(SHA256(payload))` 再哈希，计算校验和。校验和是结果哈希的前四个字节。
4. 将校验和附加到 `version+PubKeyHash` 的组合中。
5. 使用 Base58 对 `version+PubKeyHash+checksum` 组合进行编码。

#### 实现代码

##### 结构

```go
	//私钥
	PrivateKey ecdsa.PrivateKey //椭圆加密
	//公钥
	PublicKey  []byte
```

##### 创建钱包

```go
func NewWallet() *Wallet {
	private, public := newKeyPair()
	wallet := Wallet{private, public}

	return &wallet
}
```

##### 返回钱包地址

```go
func (w Wallet) GetAddress() []byte {
	pubKeyHash := HashPubKey(w.PublicKey)

	versionedPayload := append([]byte{version}, pubKeyHash...)
	checksum := checksum(versionedPayload)

	fullPayload := append(versionedPayload, checksum...)
	address := Base58Encode(fullPayload)

	return address
}
```

##### 对公钥进行Hash

```go
publicSHA256 := sha256.Sum256(pubKey)

	RIPEMD160Hasher := ripemd160.New()
	_, err := RIPEMD160Hasher.Write(publicSHA256[:])
	if err != nil {
		log.Panic(err)
	}
	publicRIPEMD160 := RIPEMD160Hasher.Sum(nil)

	return publicRIPEMD160
```

##### 验证地址有效性

```go
pubKeyHash := Base58Decode([]byte(address))
	actualChecksum := pubKeyHash[len(pubKeyHash)-addressChecksumLen:]
	version := pubKeyHash[0]
	pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-addressChecksumLen]
	targetChecksum := checksum(append([]byte{version}, pubKeyHash...))

	return bytes.Compare(actualChecksum, targetChecksum) == 0
```

##### Checksum 为一个公钥生成checksum

```go
	//进行两次sum256
	firstSHA := sha256.Sum256(payload)
	secondSHA := sha256.Sum256(firstSHA[:])

	return secondSHA[:addressChecksumLen]
```

##### 创建钥匙对

```go
	//椭圆曲线
	curve := elliptic.P256()
	private, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		log.Panic(err)
	}
	pubKey := append(private.PublicKey.X.Bytes(), private.PublicKey.Y.Bytes()...)

	return *private, pubKey
```

### wallets

一个钱包只有一个密钥对而已。我们需要 `Wallets` 类型来保存多个钱包的组合，将它们保存到文件中，或者从文件中进行加载。`Wallet` 的构造函数会生成一个新的密钥对。`newKeyPair` 函数非常直观：ECDSA 基于椭圆曲线，所以我们需要一个椭圆曲线。接下来，使用椭圆生成一个私钥，然后再从私钥生成一个公钥。有一点需要注意：在基于椭圆曲线的算法中，公钥是曲线上的点。因此，公钥是 X，Y 坐标的组合。在比特币中，这些坐标会被连接起来，然后形成一个公钥。

#### 结构

```go
Wallets map[string]*Wallet
```

#### 创建钱包集合

```go
func NewWallets() (*Wallets) {
	wallets := Wallets{}
	wallets.Wallets = make(map[string]*Wallet)

	//err := wallets.LoadFromFile(nodeID)

	return &wallets
}
```

#### 创建新钱包

```go
func (ws *Wallets) CreateNewWallet() string {
	wallet := NewWallet()
	address := fmt.Sprintf("新钱包地址：%s", wallet.GetAddress())

	ws.Wallets[address] = wallet

	return address
}
```

### 将钱包信息写入dat文件

可以将钱包写入文件里，每次记录时，覆盖之前的，以此来保证钱包文件一直是最新的。

```go
func (ws *Wallets) Savewallet() {
	var content bytes.Buffer

	gob.Register(elliptic.P256())
	//注册目的是为了可以序列化
	encoder := gob.NewEncoder(&content)
	err := encoder.Encode(ws)
	if err != nil {
		log.Panic(err)
	}

	//将序列化后的数据写入文件，原来文件被覆盖
	err = ioutil.WriteFile(walletFile, content.Bytes(), 0644)
	if err != nil {
		log.Panic(err)
	}

}
```

得到的dat文件：

![image-20230503023621040](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230503023621040.png)

#### 读取钱包dat文件

```go
func (ws *Wallets) LoadFromFile(nodeID string) error {
	walletFile := fmt.Sprintf(walletFile, nodeID)
	if _, err := os.Stat(walletFile); os.IsNotExist(err) {
		return err
	}

	fileContent, err := ioutil.ReadFile(walletFile)
	if err != nil {
		log.Panic(err)
	}

	var wallets Wallets
	gob.Register(elliptic.P256())
	decoder := gob.NewDecoder(bytes.NewReader(fileContent))
	err = decoder.Decode(&wallets)
	if err != nil {
		log.Panic(err)
	}

	ws.Wallets = wallets.Wallets

	return nil
}
```

### 将Txoutput。input中换成公私钥

因为之前写UTXO时为了方便，还没有写公私钥，现在已经不在需要`criptPubKey` 和 `ScriptSig` 字段

更改后的结构：

#### transaction_input

```go
	//交易的hash
	TxHash []byte
	//存储TXout在Vout里的索引
	Vout int
	//数字签名
	Signature []byte
	//公钥
	PubKey    []byte
```

#### transaction_output

```go
	//值
	Value int
	//公钥
	PubKeyHash []byt
```

#### 对于output：

`Lock` 只是简单地锁定了一个输出。当我们给某个人发送币时，我们只知道他的地址，因为这个函数使用一个地址作为唯一的参数。然后，`unlock`进行解锁地址会被解码，从中提取出公钥哈希并保存在 `PubKeyHash` 字段。



## 七、数字签名

### 为什么要数字签名

交易必须被签名，因为这是比特币里面保证发送方不会花费属于其他人的币的唯一方式。如果一个签名是无效的，那么这笔交易就会被认为是无效的，因此，这笔交易也就无法被加到区块链中。

我们现在离实现交易签名还差一件事情：用于签名的数据。一笔交易的哪些部分需要签名？又或者说，要对完整的交易进行签名？选择签名的数据相当重要。因为用于签名的这个数据，必须要包含能够唯一识别数据的信息。比如，如果仅仅对输出值进行签名并没有什么意义，因为签名不会考虑发送方和接收方。

考虑到交易解锁的是之前的输出，然后重新分配里面的价值，并锁定新的输出，那么必须要签名以下数据：

1. 存储在已解锁输出的公钥哈希。它识别了一笔交易的“发送方”。
2. 存储在新的锁定输出里面的公钥哈希。它识别了一笔交易的“接收方”。
3. 新的输出值。

> 在比特币中，锁定/解锁逻辑被存储在脚本中，它们被分别存储在输入和输出的 `ScriptSig` 和 `ScriptPubKey` 字段。由于比特币允许这样不同类型的脚本，它对 `ScriptPubKey` 的整个内容进行了签名。

可以看到，我们不需要对存储在输入里面的公钥签名。因此，在比特币里， 所签名的并不是一个交易，而是一个去除部分内容的输入副本，输入里面存储了被引用输出的 `ScriptPubKey` 。

> 获取修剪后的交易副本的详细过程在这里. 虽然它可能已经过时了，但是我并没有找到另一个更可靠的来源。

### 怎么实现

数字签名是一种由两部分组成的数学方案：第一部分是使用私钥（签名密钥）从消息（交易）创建签名的算法；第二部分是允许任何人在给定消息和公钥时验证签名合法性的算法。

![image-20230520172828380](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230520172828380.png)

**1.创建数字签名**

在比特币的ECDSA算法实现中，被签名的“消息”是交易，或者更准确地说是交易中特定数据子集的散列。签名密钥是用户的私钥。结果是签名：

![屏幕截图 2023-05-20 143811](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 143811.png)

这里的：

·dA是签名私钥·m是交易（或其部分）

·Fhash是散列函数·Fsig是签名算法·

Sig是结果签名有关ECDSA数学的。

函数Fsig产生由两个值组成的签名Sig，通常称为R和S：

```
Sig = (R, S)
```

现在已经计算了两个值R和S，它们被序列化为字节流，使用一种称为“分辨编码规则”（Distinguished Encoding Rules）或DER的国际标准编码方案。

**2.签名序列化（DER）**

我们再来看看Alice创建的交易。在交易输入中有一个解锁脚本，其中包含Alice的钱包中的以下DER编码签名：

```
3045022100884d142d86652a3f47ba4746ec719bbfbd040a570b1deccbb6498c75c4ae24cb02204b
9f039ff08df09cbe9f6addac960298cad530a863ea8f53982c09db8f6e381301
```

该签名是Alice的钱包生成的R和S值的序列化字节流，证明她拥有授权花费该输出的私钥。序列化格式包含以下9个元素：

·0x30表示DER序列的开始

·0x45表示序列的长度（69字节）

·0x02表示一个整数值

·0x21表示整数的长度（33字节）

·R——00884d142d86652a3f47ba4746ec719bbfbd040a570b1deccbb6498c75c4ae24cb·0x02表示另一个整数值

·0x20表示整数的长度（32字节）

·S——

4b9f039ff08df09cbe9f6addac960298cad530a863ea8f53982c09db8f6e3813

·后缀（0x01）指示使用的散列的类型（SIGHASH_ALL）

看看你是否可以使用这个列表解码Alice的序列化（DER编码）签名。重要的数字是R和S；其余的数据是DER编码方案的一部分。

### 数字签名要在什么时候用？

数字签名被应用于消息，在比特币中，就是交易本身。签名意味着签名者对特定交易数据的承诺（commitment）。在最简单的形式中，签名适用于整个交易，从而承诺所有输入、输出和其他交易字段。但是，一个签名可以只承诺交易中的一部分数据.

### 验证接口

要验证签名，必须有签名（R和S）、序列化交易和公钥（对应于用于创建签名的私钥）。本质上，签名的验证意味着“只有生成此公钥的私钥的所有者，才能在此交易上产生此签名。

”签名验证算法采用消息（交易或其部分的散列值）、签名者的公钥和签名（R和S值），如果签名对该消息和公钥有效，则返回TRUE值。



### 过程分析

```go
func (tx *Transaction) Sign(privKey ecdsa.PrivateKey, prevTXs map[string]Transaction) {
	if tx.IsCoinbase() {
		return
	}

	txCopy := tx.TrimmedCopy()

	for inID, vin := range txCopy.Vin {
		prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
		txCopy.Vin[inID].Signature = nil
		txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash
		txCopy.ID = txCopy.Hash()
		txCopy.Vin[inID].PubKey = nil

		r, s, err := ecdsa.Sign(rand.Reader, &privKey, txCopy.ID)
		signature := append(r.Bytes(), s.Bytes()...)

		tx.Vin[inID].Signature = signature
	}
}
```

这个方法接受一个私钥和一个之前交易的 map。正如上面提到的，为了对一笔交易进行签名，我们需要获取交易输入所引用的输出，因为我们需要存储这些输出的交易。

来一步一步地分析该方法：

```go
if tx.IsCoinbase() {
	return
}
```

coinbase 交易因为没有实际输入，所以没有被签名。

```go
txCopy := tx.TrimmedCopy()
```

将会被签署的是修剪后的交易副本，而不是一个完整交易：

```go
func (tx *Transaction) TrimmedCopy() Transaction {
	var inputs []TXInput
	var outputs []TXOutput

	for _, vin := range tx.Vin {
		inputs = append(inputs, TXInput{vin.Txid, vin.Vout, nil, nil})
	}

	for _, vout := range tx.Vout {
		outputs = append(outputs, TXOutput{vout.Value, vout.PubKeyHash})
	}

	txCopy := Transaction{tx.ID, inputs, outputs}

	return txCopy
}
```

这个副本包含了所有的输入和输出，但是 `TXInput.Signature` 和 `TXIput.PubKey` 被设置为 `nil`。

接下来，我们会迭代副本中每一个输入：

```go
for inID, vin := range txCopy.Vin {
	prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
	txCopy.Vin[inID].Signature = nil
	txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash
```

在每个输入中，`Signature` 被设置为 `nil` (仅仅是一个双重检验)，`PubKey` 被设置为所引用输出的 `PubKeyHash`。现在，除了当前交易，其他所有交易都是“空的”，也就是说他们的 `Signature` 和 `PubKey` 字段被设置为 `nil`。因此，**输入是被分开签名的**，尽管这对于我们的应用并不十分紧要，但是比特币允许交易包含引用了不同地址的输入。

```go
	txCopy.ID = txCopy.Hash()
	txCopy.Vin[inID].PubKey = nil
```

`Hash` 方法对交易进行序列化，并使用 SHA-256 算法进行哈希。哈希后的结果就是我们要签名的数据。在获取完哈希，我们应该重置 `PubKey` 字段，以便于它不会影响后面的迭代。

现在，关键点：

```go
	r, s, err := ecdsa.Sign(rand.Reader, &privKey, txCopy.ID)
	signature := append(r.Bytes(), s.Bytes()...)

	tx.Vin[inID].Signature = signature
```

我们通过 `privKey` 对 `txCopy.ID` 进行签名。一个 ECDSA 签名就是一对数字，我们对这对数字连接起来，并存储在输入的 `Signature` 字段。

现在，验证函数：

```go
func (tx *Transaction) Verify(prevTXs map[string]Transaction) bool {
	txCopy := tx.TrimmedCopy()
	curve := elliptic.P256()

	for inID, vin := range tx.Vin {
		prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
		txCopy.Vin[inID].Signature = nil
		txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash
		txCopy.ID = txCopy.Hash()
		txCopy.Vin[inID].PubKey = nil

		r := big.Int{}
		s := big.Int{}
		sigLen := len(vin.Signature)
		r.SetBytes(vin.Signature[:(sigLen / 2)])
		s.SetBytes(vin.Signature[(sigLen / 2):])

		x := big.Int{}
		y := big.Int{}
		keyLen := len(vin.PubKey)
		x.SetBytes(vin.PubKey[:(keyLen / 2)])
		y.SetBytes(vin.PubKey[(keyLen / 2):])

		rawPubKey := ecdsa.PublicKey{curve, &x, &y}
		if ecdsa.Verify(&rawPubKey, txCopy.ID, &r, &s) == false {
			return false
		}
	}

	return true
}
```

这个方法十分直观。首先，我们需要同一笔交易的副本：

```go
txCopy := tx.TrimmedCopy()
```

然后，我们需要相同的区块用于生成密钥对：

```go
curve := elliptic.P256()
```

接下来，我们检查每个输入中的签名：

```go
for inID, vin := range tx.Vin {
	prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
	txCopy.Vin[inID].Signature = nil
	txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash
	txCopy.ID = txCopy.Hash()
	txCopy.Vin[inID].PubKey = nil
```

这个部分跟 `Sign` 方法一模一样，因为在验证阶段，我们需要的是与签名相同的数据。

```go
	r := big.Int{}
	s := big.Int{}
	sigLen := len(vin.Signature)
	r.SetBytes(vin.Signature[:(sigLen / 2)])
	s.SetBytes(vin.Signature[(sigLen / 2):])

	x := big.Int{}
	y := big.Int{}
	keyLen := len(vin.PubKey)
	x.SetBytes(vin.PubKey[:(keyLen / 2)])
	y.SetBytes(vin.PubKey[(keyLen / 2):])
```

这里我们解包存储在 `TXInput.Signature` 和 `TXInput.PubKey` 中的值，因为一个签名就是一对数字，一个公钥就是一对坐标。我们之前为了存储将它们连接在一起，现在我们需要对它们进行解包在 `crypto/ecdsa` 函数中使用。

```go
	rawPubKey := ecdsa.PublicKey{curve, &x, &y}
	if ecdsa.Verify(&rawPubKey, txCopy.ID, &r, &s) == false {
		return false
	}
}

return true
```

在这里：我们使用从输入提取的公钥创建了一个 `ecdsa.PublicKey`，通过传入输入中提取的签名执行了 `ecdsa.Verify`。如果所有的输入都被验证，返回 `true`；如果有任何一个验证失败，返回 `false`.

现在，我们需要一个函数来获得之前的交易。由于这需要与区块链进行交互，我们将它放在了 `Blockchain` 的方法里面：

```go
func (bc *Blockchain) FindTransaction(ID []byte) (Transaction, error) {
	bci := bc.Iterator()

	for {
		block := bci.Next()

		for _, tx := range block.Transactions {
			if bytes.Compare(tx.ID, ID) == 0 {
				return *tx, nil
			}
		}

		if len(block.PrevBlockHash) == 0 {
			break
		}
	}

	return Transaction{}, errors.New("Transaction is not found")
}

func (bc *Blockchain) SignTransaction(tx *Transaction, privKey ecdsa.PrivateKey) {
	prevTXs := make(map[string]Transaction)

	for _, vin := range tx.Vin {
		prevTX, err := bc.FindTransaction(vin.Txid)
		prevTXs[hex.EncodeToString(prevTX.ID)] = prevTX
	}

	tx.Sign(privKey, prevTXs)
}

func (bc *Blockchain) VerifyTransaction(tx *Transaction) bool {
	prevTXs := make(map[string]Transaction)

	for _, vin := range tx.Vin {
		prevTX, err := bc.FindTransaction(vin.Txid)
		prevTXs[hex.EncodeToString(prevTX.ID)] = prevTX
	}

	return tx.Verify(prevTXs)
}
```

这几个比较简单：`FindTransaction` 通过 ID 找到一笔交易（这需要在区块链上迭代所有区块）；`SignTransaction` 传入一笔交易，找到它引用的交易，然后对它进行签名；`VerifyTransaction` 做的是相同的事情，不过是对交易进行验证。

现在，我们需要实际签名和验证交易。签名在 `NewUTXOTransaction` 中进行：

```go
func NewUTXOTransaction(from, to string, amount int, bc *Blockchain) *Transaction {
	...

	tx := Transaction{nil, inputs, outputs}
	tx.ID = tx.Hash()
	bc.SignTransaction(&tx, wallet.PrivateKey)

	return &tx
}
```

在一笔交易被放入一个块之前进行验证：

```go
func (bc *Blockchain) MineBlock(transactions []*Transaction) {
	var lastHash []byte

	for _, tx := range transactions {
		if bc.VerifyTransaction(tx) != true {
			log.Panic("ERROR: Invalid transaction")
		}
	}
	...
}
```

就是这些了！让我们再来检查一下所有内容：

```go
$ blockchain_go createwallet
Your new address: 1AmVdDvvQ977oVCpUqz7zAPUEiXKrX5avR

$ blockchain_go createwallet
Your new address: 1NE86r4Esjf53EL7fR86CsfTZpNN42Sfab

$ blockchain_go createblockchain -address 1AmVdDvvQ977oVCpUqz7zAPUEiXKrX5avR
000000122348da06c19e5c513710340f4c307d884385da948a205655c6a9d008

Done!

$ blockchain_go send -from 1AmVdDvvQ977oVCpUqz7zAPUEiXKrX5avR -to 1NE86r4Esjf53EL7fR86CsfTZpNN42Sfab -amount 6
0000000f3dbb0ab6d56c4e4b9f7479afe8d5a5dad4d2a8823345a1a16cf3347b

Success!

$ blockchain_go getbalance -address 1AmVdDvvQ977oVCpUqz7zAPUEiXKrX5avR
Balance of '1AmVdDvvQ977oVCpUqz7zAPUEiXKrX5avR': 4

$ blockchain_go getbalance -address 1NE86r4Esjf53EL7fR86CsfTZpNN42Sfab
Balance of '1NE86r4Esjf53EL7fR86CsfTZpNN42Sfab': 6
```

一切正常！

要进行签名，就要首先拷贝一份新的Transaction进行签名，如下`TrimmedCopy`

```go
// 拷贝一份新的Transaction进行签名
func (tx *Transaction) TrimmedCopy() Transaction {
	var inputs []*TXInput
	var outputs []*TXOutput

	for _, vin := range tx.Vins {
		inputs = append(inputs, &TXInput{vin.TxHash, vin.Vout, nil, nil})
	}

	for _, vout := range tx.Vouts {
		outputs = append(outputs, &TXOutput{vout.Value, vout.Ripemd160})
	}

	txCopy := Transaction{tx.TxHash, inputs, outputs}

	return txCopy
}
```

签名函数`sign`,这个方法接受一个私钥和一个之前交易的 map。正如上面提到的，为了对一笔交易进行签名，我们需要获取交易输入所引用的输出，因为我们需要存储这些输出的交易

```go
func (tx *Transaction) Sign(privKey ecdsa.PrivateKey, prevTXs map[string]Transaction) {
	if tx.IsCoinbaseTransaction() {
		return
	}

	for _, vin := range tx.Vins {
		if prevTXs[hex.EncodeToString(vin.TxHash)].TxHash == nil {
			log.Panic("ERROR: 交易hash不正确")
		}
	}
	//备份

	txCopy := tx.TrimmedCopy()
	
	for inID, vin := range txCopy.Vins {
		prevTx := prevTXs[hex.EncodeToString(vin.TxHash)]
		txCopy.Vins[inID].Signature = nil
		txCopy.Vins[inID].PubKey = prevTx.Vouts[vin.Vout].Ripemd160
		txCopy.TxHash = txCopy.Hash()
		txCopy.Vins[inID].PubKey = nil
		dataToSign := fmt.Sprintf("%x\n", txCopy)
	//签名代码
		r, s, err := ecdsa.Sign(rand.Reader, &privKey, []byte(dataToSign))
		if err != nil {
			log.Panic(err)
		}
		signature := append(r.Bytes(), s.Bytes()...)

		tx.Vins[inID].Signature = signature
		txCopy.Vins[inID].PubKey = nil
	}
}
```




在进行交易时，我们需要在`NewSimpletransaction`中完成交易的最后，对blockchain进行签名

```go
	//进行签名
	bloBlockchain.SignTransaction(&tx, wallet.PrivateKey)
```

在`blockchain`实现交易的函数，先需要判断是否为创世，然后开始签名

```go
func (Blockchain *Blockchain) SignTransaction(tx *Transaction, privKey ecdsa.PrivateKey) {

	//如果为创世
	if tx.IsCoinbaseTransaction() {
		return
	}
	prevTXs := make(map[string]Transaction)

	for _, vin := range tx.Vins {
		//找到对应的hash
		prevTX, err := Blockchain.FindTransaction(vin.TxHash)
		if err != nil {
			log.Panic(err)
		}
		prevTXs[hex.EncodeToString(prevTX.TxHash)] = prevTX
	}

	tx.Sign(privKey, prevTXs)

}
```

测试一下,其中`1FYaXEBTwQCkvBNBEHRVzksQw5bXSZJ8rW`为创世区块的钱包，有100token

```go
./main send -from '[\"1FYaXEBTwQCkvBNBEHRVzksQw5bXSZJ8rW\"]' -to '[\"1G6pVbDDSZeyc4MaGBZcjxm3d3tX87MbRH\"]' -amount '[\"4\"]'
```

![image-20230503144212977](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230503144212977.png)

验证一下金额，没有问题，接下来继续实现签名的验证

![image-20230503144329738](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230503144329738.png)

### 签名验证实现

在挖矿时，建立新区块之前，需要进行验证，来判断真伪，随后才允许建立新区块。因此我们需要一个`Verify`来验证

```GO
for _,tx:=range txs{
		if Blockchain.VerifyTransaction(tx)==false{
			log.Panic("签名失败")
		}
	
	}
```

`Verify`,实现验证签名，需要先验证Traction,` VerifyTransaction`

```go
func (bc *Blockchain) VerifyTransaction(tx *Transaction) bool {
	if tx.IsCoinbase() {
		return true
	}

	prevTXs := make(map[string]Transaction)

	for _, vin := range tx.Vin {
		prevTX, err := bc.FindTransaction(vin.Txid)
		if err != nil {
			log.Panic(err)
		}
		prevTXs[hex.EncodeToString(prevTX.ID)] = prevTX
	}

	return tx.Verify(prevTXs)
}

func dbExists(dbFile string) bool {
	if _, err := os.Stat(dbFile); os.IsNotExist(err) {
		return false
	}

	return true
}
```

`Verify`

获取交易时的副本

```go
	txCopy := tx.TrimmedCopy()

	curve := elliptic.P256()
```

检查输入中签名

```go
//签名时所用的ID，检查他们的签名
	for inID, vin := range tx.Vin {
		prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
		txCopy.Vin[inID].Signature = nil
		txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash

	
```

私钥ID产生,并验证数据

```go
		r := big.Int{}
		s := big.Int{}
		sigLen := len(vin.Signature)
		r.SetBytes(vin.Signature[:(sigLen / 2)])
		s.SetBytes(vin.Signature[(sigLen / 2):])

		//验证数据
		x := big.Int{}
		y := big.Int{}
		//从签名中取到的
		keyLen := len(vin.PubKey)
		x.SetBytes(vin.PubKey[:(keyLen / 2)])
		y.SetBytes(vin.PubKey[(keyLen / 2):])

```

这里我们解包存储在 `TXInput.Signature` 和 `TXInput.PubKey` 中的值，因为一个签名就是一对数字，一个公钥就是一对坐标。我们之前为了存储将它们连接在一起，现在我们需要对它们进行解包在 `crypto/ecdsa` 函数中使用。

```go

		dataToVerify := fmt.Sprintf("%x\n", txCopy)
		//对签名进行解包
		rawPubKey := ecdsa.PublicKey{Curve: curve, X: &x, Y: &y}//椭圆曲线
		if ecdsa.Verify(&rawPubKey, []byte(dataToVerify), &r, &s) == false {
			return false
		}
		txCopy.Vin[inID].PubKey = nil
	}

```

再次验证

```go
 ./main send -from '[\"1FYaXEBTwQCkvBNBEHRVzksQw5bXSZJ8rW\"]' -to '[\"1G6pVbDDSZeyc4MaGBZcjxm3d3tX87MbRH\"]' -amount '[\"4\"]'
```

```
./main getbalance -address "1G6pVbDDSZeyc4MaGBZcjxm3d3tX87MbRH"
&{4 [165 163 156 150 29 189 122 1 136 152 206 72 191 214 12 27 18 114 91 85]}
map[]
&{4 [165 163 156 150 29 189 122 1 136 152 206 72 191 214 12 27 18 114 91 85]}
map[]
1G6pVbDDSZeyc4MaGBZcjxm3d3tX87MbRH一共有8个Token
```

一切正常，注释掉签名试一下。

```go
	//进行签名
	//bloBlockchain.SignTransaction(tx, wallet.PrivateKey)
```

```
2023/05/03 15:17:08 签名失败
panic: 签名失败

goroutine 1 [running]:
log.Panic({0xc0000cdcb0?, 0xc0000da2d0?, 0xc0000cdce8?})
        C:/Program Files/Go/src/log/log.go:385 +0x65
Go/golong_blockChain/BLOCK16/BLC.(*Blockchain).MineNewBlock(0xc0000a46e0, {0xc0000d7640?, 0x1?, 0xc0000cdd98?}, {0xc0000d7680?, 0x1?, 0x5?}, {0xc0000d76c0, 0x1, 0x4})
```

可以发现现在不签名就无法挖新区块了，一切成功。现在已经防止别人冒名顶替了，yeah。



## 八、挖矿奖励&交易二&默克尔树

### 挖矿奖励

挖矿奖励是指由网络中的节点通过保护区块链网络来确保可靠性，并为其提供支持和维护的一种激励机制。这一机制为挖矿的节点矿工提供了一定奖励，以保证他们愿意维护网络的可靠性和安全性。

·矿工的收益来自于挖矿奖励。这样才能激励矿工积极参与挖矿，维护网络安全。

·挖矿奖励是通过参与网络的保护和维护，并为此耗费一定能量与计算资源，从而获取一定的节点奖励。

·挖矿奖励的数量通常由网络中的节点参与为其提供算力来计算，以维护网络的稳定性，从而获得奖励数量的决定。

#### 代码实现

##### 实现思路

1. 首先要明确谁挖出区块了，挖矿奖励就是谁的

2. 在建立交易时，在`NewSimpletransaction`中凭空出现一笔output，作为奖励给与挖出区块矿工。

3. 因为目前还没有进行节点地区分，所以暂时认定交易中第一个交易为挖到区块的。在`MineNewBlock`中建立他的交易。随后调用`newCoinblock`,并传到交易函数中即可。

##### 代码

调用创建区块函数，然后将这笔交易放入`Transaction`的数组中

```go
tx:=NewCoinbaseTransaction(from[0])
txs=append(txs, tx)
```

`newCoinblock`，在新的区块中给予10Token的奖励。在后续实现网络后，将会把奖励给与矿工节点。

```go
//消费
	txinput := &TXInput{[]byte{}, -1, nil, []byte{}}

	txoutput := NewTXOutput(10, address)//->此处为奖励

	//txoutput := &TXOutput{10, address} //给创世区块10Token

	txCoinbase := &Transaction{[]byte{}, []*TXInput{txinput}, []*TXOutput{txoutput}}

	//此处hash为将自己序列化
	txCoinbase.HashTransactions()
```






### 对交易进行优化

在之前的代码中数据库中存储每笔交易，并且查询时要遍历每笔交易，如果现在的数据量达到了1Gb，那么如果还按照以前的那样挨个遍历，那么数据量过多。因此还可以改进。

#### 改进思路

可以把未花费输出单独存在一张表中，来增加查询速率，类似于计算机操作系统中的表，当想找某个数据时，可以先查表，以此来快速得到数据。`chainstate` 不存储交易。它所存储的是 UTXO 集，也就是未花费交易输出的集合。除此以外，它还存储了“数据库表示的未花费交易输出的块哈希”.

实现 UTXO 集的话需要作出哪些改变,我们想要以下方法：

1. `Blockchain.FindUTXO` - 通过对区块进行迭代找到所有未花费输出。
2. `UTXOSet.Reindex` - 使用 `UTXO` 找到未花费输出，然后在数据库中进行存储。这里就是缓存的地方。
3. `UTXOSet.FindSpendableOutputs` - 类似 `Blockchain.FindSpendableOutputs`，但是使用 UTXO 集。
4. `UTXOSet.FindUTXO` - 类似 `Blockchain.FindUTXO`，但是使用 UTXO 集。
5. `Blockchain.FindTransaction` 跟之前一样。

#### 代码

```go
type UTXOSet struct {
    Blockchain *Blockchain
}
```

我们将会使用一个单一数据库，但是我们会将 UTXO 集从存储在不同的 bucket 中。因此，`UTXOSet` 跟 `Blockchain` 一起。

```go
func (u UTXOSet) Reindex() {
    db := u.Blockchain.db
    bucketName := []byte(utxoBucket)

    err := db.Update(func(tx *bolt.Tx) error {
        err := tx.DeleteBucket(bucketName)
        _, err = tx.CreateBucket(bucketName)
    })

    UTXO := u.Blockchain.FindUTXO()

    err = db.Update(func(tx *bolt.Tx) error {
        b := tx.Bucket(bucketName)

        for txID, outs := range UTXO {
            key, err := hex.DecodeString(txID)
            err = b.Put(key, outs.Serialize())
        }
    })
}
```

这个方法初始化了 UTXO 集。首先，如果 bucket 存在就先移除，然后从区块链中获取所有的未花费输出，最终将输出保存到 bucket 中。

`Blockchain.FindUTXO` 几乎跟 `Blockchain.FindUnspentTransactions` 一模一样，但是现在它返回了一个 `TransactionID -> TransactionOutputs` 的 map。

##### 过程：

现在，UTXO 集可以用于发送币：

```go
func (u UTXOSet) FindSpendableOutputs(pubkeyHash []byte, amount int) (int, map[string][]int) {
    unspentOutputs := make(map[string][]int)
    accumulated := 0
    db := u.Blockchain.db

    err := db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(utxoBucket))
        c := b.Cursor()

        for k, v := c.First(); k != nil; k, v = c.Next() {
            txID := hex.EncodeToString(k)
            outs := DeserializeOutputs(v)

            for outIdx, out := range outs.Outputs {
                if out.IsLockedWithKey(pubkeyHash) && accumulated < amount {
                    accumulated += out.Value
                    unspentOutputs[txID] = append(unspentOutputs[txID], outIdx)
                }
            }
        }
    })

    return accumulated, unspentOutputs
}
```

或者检查余额：

```go
func (u UTXOSet) FindUTXO(pubKeyHash []byte) []TXOutput {
    var UTXOs []TXOutput
    db := u.Blockchain.db

    err := db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(utxoBucket))
        c := b.Cursor()

        for k, v := c.First(); k != nil; k, v = c.Next() {
            outs := DeserializeOutputs(v)

            for _, out := range outs.Outputs {
                if out.IsLockedWithKey(pubKeyHash) {
                    UTXOs = append(UTXOs, out)
                }
            }
        }

        return nil
    })

    return UTXOs
}
```

这是 `Blockchain` 方法的简单修改后的版本。这个 `Blockchain` 方法已经不再需要了。

有了 UTXO 集，也就意味着我们的数据（交易）现在已经被分开存储：实际交易被存储在区块链中，未花费输出被存储在 UTXO 集中。这样一来，我们就需要一个良好的同步机制，因为我们想要 UTXO 集时刻处于最新状态，并且存储最新交易的输出。但是我们不想每生成一个新块，就重新生成索引，因为这正是我们要极力避免的频繁区块链扫描。因此，我们需要一个机制来更新 UTXO 集：

```go
func (u UTXOSet) Update(block *Block) {
    db := u.Blockchain.db

    err := db.Update(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(utxoBucket))

        for _, tx := range block.Transactions {
            if tx.IsCoinbase() == false {
                for _, vin := range tx.Vin {
                    updatedOuts := TXOutputs{}
                    outsBytes := b.Get(vin.Txid)
                    outs := DeserializeOutputs(outsBytes)

                    for outIdx, out := range outs.Outputs {
                        if outIdx != vin.Vout {
                            updatedOuts.Outputs = append(updatedOuts.Outputs, out)
                        }
                    }

                    if len(updatedOuts.Outputs) == 0 {
                        err := b.Delete(vin.Txid)
                    } else {
                        err := b.Put(vin.Txid, updatedOuts.Serialize())
                    }

                }
            }

            newOutputs := TXOutputs{}
            for _, out := range tx.Vout {
                newOutputs.Outputs = append(newOutputs.Outputs, out)
            }

            err := b.Put(tx.ID, newOutputs.Serialize())
        }
    })
}
```

虽然这个方法看起来有点复杂，但是它所要做的事情非常直观。当挖出一个新块时，应该更新 UTXO 集。更新意味着移除已花费输出，并从新挖出来的交易中加入未花费输出。如果一笔交易的输出被移除，并且不再包含任何输出，那么这笔交易也应该被移除。相当简单！

现在让我们在必要的时候使用 UTXO 集：

```go
func (cli *CLI) createBlockchain(address string) {
    ...
    bc := CreateBlockchain(address)
    defer bc.db.Close()

    UTXOSet := UTXOSet{bc}
    UTXOSet.Reindex()
    ...
}
```

当一个新的区块链被创建以后，就会立刻进行重建索引。目前，这是 `Reindex` 唯一使用的地方，即使这里看起来有点“杀鸡用牛刀”，因为一条链开始的时候，只有一个块，里面只有一笔交易，`Update` 已经被使用了。不过我们在未来可能需要重建索引的机制。

```go
func (cli *CLI) send(from, to string, amount int) {
    ...
    newBlock := bc.MineBlock(txs)
    UTXOSet.Update(newBlock)
}
```

当挖出一个新块时，UTXO 集就会进行更新。

让我们来检查一下如否如期工作：

```go
$ blockchain_go createblockchain -address 1JnMDSqVoHi4TEFXNw5wJ8skPsPf4LHkQ1
00000086a725e18ed7e9e06f1051651a4fc46a315a9d298e59e57aeacbe0bf73

Done!

$ blockchain_go send -from 1JnMDSqVoHi4TEFXNw5wJ8skPsPf4LHkQ1 -to 12DkLzLQ4B3gnQt62EPRJGZ38n3zF4Hzt5 -amount 6
0000001f75cb3a5033aeecbf6a8d378e15b25d026fb0a665c7721a5bb0faa21b

Success!

$ blockchain_go send -from 1JnMDSqVoHi4TEFXNw5wJ8skPsPf4LHkQ1 -to 12ncZhA5mFTTnTmHq1aTPYBri4jAK8TacL -amount 4
000000cc51e665d53c78af5e65774a72fc7b864140a8224bf4e7709d8e0fa433

Success!

$ blockchain_go getbalance -address 1JnMDSqVoHi4TEFXNw5wJ8skPsPf4LHkQ1
Balance of '1F4MbuqjcuJGymjcuYQMUVYB37AWKkSLif': 20

$ blockchain_go getbalance -address 12DkLzLQ4B3gnQt62EPRJGZ38n3zF4Hzt5
Balance of '1XWu6nitBWe6J6v6MXmd5rhdP7dZsExbx': 6

$ blockchain_go getbalance -address 12ncZhA5mFTTnTmHq1aTPYBri4jAK8TacL
Balance of '13UASQpCR8Nr41PojH8Bz4K6cmTCqweskL': 4
```

很好！`1JnMDSqVoHi4TEFXNw5wJ8skPsPf4LHkQ1` 地址接收到了 3 笔奖励：

1. 一次是挖出创世块
2. 一次是挖出块 0000001f75cb3a5033aeecbf6a8d378e15b25d026fb0a665c7721a5bb0faa21b
3. 一个是挖出块 000000cc51e665d53c78af5e65774a72fc7b864140a8224bf4e7709d8e0fa433

##### 查找所有人的未花费交易输出

首先设置一个结构题`UTXOset`,存储区块链

```go
type UTXOSet struct {
	Blockchain *Blockchain
}
```

既然需要设计UTXO表，那么就要有重置表的方法，来更新表。`UTXOreset`

```go
//重置数据库表
func (utxoset *UTXOSet) ResetUTXOset() {
	err := utxoset.Blockchain.DB.Update(func(tx *bolt.Tx) error {
		//判断表是否存在
		b := tx.Bucket([]byte(utxoTableName))
		if b != nil { //如果表存在
			//删除原来的表
			err := tx.DeleteBucket([]byte(utxoTableName))
			if err != nil {
				log.Panic(err)
			}
			//创建新的表
			b, _ := tx.CreateBucket([]byte(utxoTableName))
			if b != nil {
				//[]*TxOutputs
				txoutputsMap := utxoset.Blockchain.FindUTXOMap()
				for keyHah, outs := range txoutputsMap {
					txHash, _ := hex.DecodeString(keyHah)
					b.Put(txHash, outs.Serialize())

				}
			}
		}
		return nil
	})
	if err != nil {
		log.Panic(err)
	}
}
```

寻找UTXO，以字典值返回

```go
func(blc *Blockchain)FindUTXOMap() map[string]*TXOutputs{
	UTXOS:=blc.UnUTXOs()

}
```

##### 输出balance

```go
UTXos:=utxoset.findUTXOForAdress(address)
	var amount int64
	for _,utxo:=range UTXos{
		amount+=int64(utxo.Outputs.Value)		
	}
	return amount

```

###### 测试一下

写一个测试方法，来运行`UTXOreset`,并进行交易，看是否真确

```go
func (cli *CLI)testMethodd() {

	Blockchain := GetBlockchainObject()
	defer Blockchain.DB.Close()
	utxoSet:=&UTXOSet{Blockchain }
	utxoSet.ResetUTXOset()

}
```

```
 ./main send -from '[\"1N2Ef8juziwVBdYcVsafeerpZBPPQMmqUk\"]' -to '[\"1GRijZQv6MZqgNLga23N3L1vHA8aGckWCN\"]' -amount '[\"4\"]'
 ./main test  ===>运行方法，保存UTXO
 /main getbalance -address "1GRijZQv6MZqgNLga23N3L1vHA8aGckWCN"
 1GRijZQv6MZqgNLga23N3L1vHA8aGckWCN一共有12个Token 
 1N2Ef8juziwVBdYcVsafeerpZBPPQMmqUk一共有18个Token
```

一切正常



### 转账优化

#### 返回要凑多少钱

```go
//计数
	//var money int = 0
	//未花费Transaction数组
	var unUTXOs []*UTXO
	//已花费
	sepentTXOutputs := make(map[string][]int)
	//处理Transaction

	for _, tx := range txs {

		if !tx.IsCoinbaseTransaction() {
			for _, in := range tx.Vins {
				//能否解锁
				pubKeyHash := Base58Decode([]byte(from))
				pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-4]
				if in.INUnLockripemd160(pubKeyHash) {
					//转换
					key := hex.EncodeToString(in.TxHash)
					//存入
					sepentTXOutputs[key] = append(sepentTXOutputs[key], in.Vout)
				}
			}
		}
	}
	for _, tx := range txs {
	work1:
		for index, out := range tx.Vouts {
			//判断
			if out.OUTUnLock(from) {
				if len(sepentTXOutputs) == 0 {
					utxo := &UTXO{tx.TxHash, index, out}
					unUTXOs = append(unUTXOs, utxo)
				}
				for hash, indexArray := range sepentTXOutputs {
					txHashStr := hex.EncodeToString(tx.TxHash)
					if txHashStr == hash {
						var isUnSpendUTXO bool
						for _, outIndex := range indexArray {
							if index == outIndex {
								isUnSpendUTXO = true
								continue work1
							}
							if !isUnSpendUTXO {
								utxo := &UTXO{tx.TxHash, index, out}
								unUTXOs = append(unUTXOs, utxo)
							}
						}
					} else {
						utxo := &UTXO{tx.TxHash, index, out}
						unUTXOs = append(unUTXOs, utxo)
					}

				}
			}
		}

	}

	return unUTXOs
```

#### 未花费交易输出更新

在send中，每交易一次，`update`一次

```GO
utxoSet.Update()
```

`update`

```go
//最新的Block
	block := utxoSet.Blockchain.Iterator().Next()
	//utxoTable
	ins := []*TXInput{}
	outsMap := make(map[string]*TXOutputs)
	//找到要删除的书籍
	for _, tx := range block.Txs {
		for _, in := range tx.Vins {
			ins = append(ins, in)
		}
	}

	for _, tx := range block.Txs {
		utxos := []*UTXO{}
		for index, out := range tx.Vouts {
			isSpent := false
			for _, in := range ins {

				if in.Vout == index && bytes.Compare(tx.TxHash, in.TxHash) == 0 && bytes.Compare(out.Ripemd160, ripemd160hash(in.PubKey)) == 0 {
					isSpent = true
					continue
				}
			}
			if isSpent {
				utxo := &UTXO{tx.TxHash, index, out}
				utxos = append(utxos, utxo)
			}

		}
		if len(utxos) > 0 {
			txHash := hex.EncodeToString(tx.TxHash)
			outsMap[txHash] = &TXOutputs{utxos}
		}

	}

	err := utxoSet.Blockchain.DB.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoTableName))
		if b != nil {
			//删除
			for _, in := range ins {
				TXoutputs := b.Get(in.TxHash) //判断是否存在
				txoutputs := DeserializeOutputs(TXoutputs)
				UTXOS := []*UTXO{}
				//判断是否需要
				isNeedDelete := false
				for _, utxo := range txoutputs.UTXOs {
					if in.Vout == utxo.index && bytes.Compare(utxo.Outputs.Ripemd160, ripemd160hash(in.PubKey)) == 0 {
						//b.Delete(in.TxHash)
						isNeedDelete = true
					} else {
						UTXOS = append(UTXOS, utxo)
					}
				}
				if isNeedDelete {
					b.Delete(in.TxHash)
					if len(UTXOS) > 0 {
						txOutputs := &TXOutputs{UTXOS}
						b.Put(in.TxHash, txOutputs.Serialize())
					}

				}
			}
			//新增
			for keyHash, outPuts := range outsMap {
				keyHashBytes, _ := hex.DecodeString(keyHash)
				b.Put(keyHashBytes, outPuts.Serialize())
			}

		}
		return nil
	})
	if err == nil {
		log.Panic(err)
	}
./main send -from '[\"1MDcHDsDWJKUqFz2aXtM6Ado7NAGMUXRc9\"]' -to '[\"19MX1BYVnWqp8t5CYAUHNo2Az6hmsJ2yR7\"]' -amount '[\"2\"]'
```

### MerkelTree

#### 什么是默克尔树？

![1231](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/v2-2db9a37e34547c76141c61b4933a368b_1440w.image)

**1.概念** 

Merkle tree（默克尔树），常叫它merkle树，是一种哈希二叉树，在计算机科学中，二叉树是每个节点最多有两个子树的树结构，每个节点代表一条结构化数据。通常子树被称作“左子树”（left subtree）和“右子树”（right subtree）。二叉树常被用于实现数据快速查询。 

**2.结构** 

由一个根节点、一组中间节点和一组叶节点组成。叶节点包含存储数据或其哈希值，中间节点是它的两个孩子节点内容的哈希值，根节点也是由它的两个子节点内容的哈希值组成。所以Merkle树也称哈希树。 

**3.特点** 

技术特点就不多讲了，主要特点就是：底层（叶子节点）数据的任何变动，都会逐级向上传递到其父节点，一直到Merkle树的根节点使得根节点的哈希值发生变化。

**4.总结原理** 

区块链中每个区块都会有一个 Merkle 树，它从叶子节点（树的底部）开始，一个叶子节点就是一个交易哈希。叶子节点的数量必须是双数，但是并非每个块都包含了双数的交易。如果一个块里面的交易数为单数，那么就将最后一个叶子节点（也就是 Merkle 树的最后一个交易，不是区块的最后一笔交易）复制一份凑成双数。 

从下往上，两两成对，连接两个节点哈希，将组合哈希作为新的哈希。新的哈希就成为新的树节点。重复该过程，直到仅有一个节点，也就是树根。根哈希然后就会当做是整个块交易的唯一标示，将它保存到区块头，然后用于工作量证明。

#### 默克尔树在区块链中的作用

上如上面所提到的，完整的比特币数据库（也就是区块链）需要超过 140 Gb 的磁盘空间。因为比特币的去中心化特性，网络中的每个节点必须是独立，自给自足的，也就是每个节点必须存储一个区块链的完整副本。随着越来越多的人使用比特币，这条规则变得越来越难以遵守：因为不太可能每个人都去运行一个全节点。并且，由于节点是网络中的完全参与者，它们负有相关责任：节点必须验证交易和区块。另外，要想与其他节点交互和下载新块，也有一定的网络流量需求。

在中本聪的 比特币原始论文中，对这个问题也有一个解决方案：简易支付验证（Simplified Payment Verification, SPV）。SPV 是一个比特币轻节点，它不需要下载整个区块链，也**不需要验证区块和交易**。相反，它会在区块链查找交易（为了验证支付），并且需要连接到一个全节点来检索必要的数据。这个机制允许在仅运行一个全节点的情况下有多个轻钱包。

为了实现 SPV，需要有一个方式来检查是否一个区块包含了某笔交易，而无须下载整个区块。这就是 Merkle 树所要完成的事情。

比特币用 Merkle 树来获取交易哈希，哈希被保存在区块头中，并会用于工作量证明系统。到目前为止，我们只是将一个块里面的每笔交易哈希连接了起来，将在上面应用了 SHA-256 算法。虽然这是一个用于获取区块交易唯一表示的一个不错的途径，但是它没有利用到 Merkle 树。

来看一下 Merkle 树：

![屏幕截图 2023-05-20 153654](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 153654.png)

每个块都会有一个 Merkle 树，它从叶子节点（树的底部）开始，一个叶子节点就是一个交易哈希（比特币使用双 SHA256 哈希）。叶子节点的数量必须是双数，但是并非每个块都包含了双数的交易。因为，如果一个块里面的交易数为单数，那么就将最后一个叶子节点（也就是 Merkle 树的最后一个交易，不是区块的最后一笔交易）复制一份凑成双数。

从下往上，两两成对，连接两个节点哈希，将组合哈希作为新的哈希。新的哈希就成为新的树节点。重复该过程，直到仅有一个节点，也就是树根。根哈希然后就会当做是整个块交易的唯一标示，将它保存到区块头，然后用于工作量证明。

Merkle 树的好处就是一个节点可以在不下载整个块的情况下，验证是否包含某笔交易。并且这些只需要一个交易哈希，一个 Merkle 树根哈希和一个 Merkle 路径。

#### 默克尔树的实现

MerkelTree的结构,每个 `MerkleNode` 包含数据和指向左右分支的指针。`MerkleTree` 实际上就是连接到下个节点的根节点，然后依次连接到更远的节点，等等。

```go
type MerkleTree struct {
    RootNode *MerkleNode
}

type MerkleNode struct {
    Left  *MerkleNode
    Right *MerkleNode
    Data  []byte
}
```

建立新的节点,每个节点包含一些数据。当节点在叶子节点，数据从外界传入（在这里，也就是一个序列化后的交易）。当一个节点被关联到其他节点，它会将其他节点的数据取过来，连接后再哈希。

```go
func NewMerkleNode(left, right *MerkleNode, data []byte) *MerkleNode {
    mNode := MerkleNode{}

    if left == nil && right == nil {
        hash := sha256.Sum256(data)
        mNode.Data = hash[:]
    } else {
        prevHashes := append(left.Data, right.Data...)
        hash := sha256.Sum256(prevHashes)
        mNode.Data = hash[:]
    }

    mNode.Left = left
    mNode.Right = right

    return &mNode
}
```

让节点形成树

```go
func NewMerkleTree(data [][]byte) *MerkleTree {
    var nodes []MerkleNode

    if len(data)%2 != 0 {
        data = append(data, data[len(data)-1])
    }

    for _, datum := range data {
        node := NewMerkleNode(nil, nil, datum)
        nodes = append(nodes, *node)
    }

    for i := 0; i < len(data)/2; i++ {
        var newLevel []MerkleNode

        for j := 0; j < len(nodes); j += 2 {
            node := NewMerkleNode(&nodes[j], &nodes[j+1], nil)
            newLevel = append(newLevel, *node)
        }

        nodes = newLevel
    }

    mTree := MerkleTree{&nodes[0]}

    return &mTree
}
```





## 九、CLI&持久化输出

命令行界面，英文名叫CLI（Command Line Interface），在go中cli是一个用于构建命令行程序的库。cli非常简洁，所有的初始化操作就是创建一个cli.App结构的对象。通过为对象的字段赋值来添加相应的功能。

CLI 绝大多数人都见过了或者使用过了，比如Vue、Flutter的CLI，iOS开发使用的Command Line Tools 、Cocopods，包括有些人可能用过的DOS操作系统 MS-DOS。
CLI是 Command-Line Interface 的缩写，也就是命令行界面。
是在图形用户界面（GUI）得到普及之前使用最为广泛的用户界面，它通常不支持鼠标，用户通过键盘输入指令，计算机接收到指令后，予以执行。也有人称之为字符用户界面（character user interface, CUI）。

在GUI已经全面普及的今天，CLI不但没有被各种操作系统摒弃反而还在不断的加强和优化，主要是因为CLI仍然有丰富的应用场景和优势。

比GUI更快的操作速度。因为不需要处理图形和鼠标等交互，CLI更节省系统资源，在熟记命令的前提下操作速度比GUI要快很多。
多用户登录。大家都知道Linux是多用户多任务操作系统，使用CLI就很方便，比如同时使用不同的用户分别查询日志、查询数据库、使用FTP上传文件。
批量操作。如果你需要维护一群计算机，CLI当然是最佳选择；使用Windows的过程中很多人都遇到过批量命名文件之类的需求，而很多实现方案都是用Windows 命令提示符也就是CLI操作。
特定的设备和场景。不间断运行的服务器、交换机、路由器等电信设备，CLI往往更合适，运维工程师绝大多数时间都是使用Shell在工作。此外，像Vue、Flutter等开发框架和工具，专门开发一套GUI是耗时耗力、得不偿失的事情，CLI就更加快捷。

### os.Args

os.Args是一个存储命令行参数的字符串切片，它的第一个元素是执行文件的名称

### flag使用方法

StringVar用指定的名称、默认值、使用信息注册一个string类型flag，并将flag的值保存到p指向的变量。

```go
func (f *FlagSet) StringVar(p *string, name string, value string, usage string)
```


BoolVar用指定的名称、默认值、使用信息注册一个bool类型flag，并将flag的值保存到p指向的变量。

```go
func BoolVar(p *bool, name string, value bool, usage string)
```

IntVar用指定的名称、默认值、使用信息注册一个int类型flag，并将flag的值保存到p指向的变量。

```go
func IntVar(p *int, name string, value int, usage string)
```

Float64Var用指定的名称、默认值、使用信息注册一个float64类型flag，并将flag的值保存到p指向的变量。

```go
func Float64Var(p *float64, name string, value float64, usage string)
```

Var方法使用指定的名字、使用信息注册一个flag。该flag的类型和值由第一个参数表示，该参数应实现了Value接口。例如，用户可以创建一个flag，可以用Value接口的Set方法将逗号分隔的字符串转化为字符串切片。

```go
func Var(value Value, name string, usage string)
```

Int用指定的名称、默认值、使用信息注册一个int类型flag。返回一个保存了该flag的值的指针。

```go
func Int(name string, value int, usage string) *int
```

从os.Args[1:]中解析注册的flag。必须在所有flag都注册好而未访问其值时执行。未注册却使用flag -help时，会返回ErrHelp。

```go
func Parse()
```

使用代码的示例

```go
package main

import (
	"flag"
	"fmt"
	"strings"
)

type interval []string

// 实现String()方法
func (h *interval) String() string {
	return fmt.Sprintf("%v", *h)
}

// Set 方法
func (h *interval) Set(s string) error {
	for _, v := range strings.Split(s, ",") {
		*h = append(*h, v)
	}
	return nil
}

var (
	name         string
	delete       bool
	age          int
	score        float64
	id           *int
	intervalFlag interval
)

// go中内置函数在main方法之前执行
func init() {
	flag.StringVar(&name, "names", "Tome", "姓名")
	flag.BoolVar(&delete, "d", false, "是否删除")
	flag.IntVar(&age, "age", 18, "年龄")
	flag.Float64Var(&score, "score", 60.0, "成绩")
	flag.Var(&intervalFlag, "cxc", "123")
	id = flag.Int("id", 0, "用户id")
}

func main() {

	flag.Parse()

	if delete {
		fmt.Println("删除")
	} else {
		fmt.Println("默认不删除")
	}

	fmt.Println("name: ", name)
	fmt.Println("age: ", age)
	fmt.Println("score:", score)
	fmt.Println("id: ", *id)
	fmt.Println(flag.Args())
	fmt.Println(intervalFlag)
}

```

如上方法中value 就是这个对应值的默认值，flag.Args()千万不能在init方法中给赋值。要不然失效解析不出来，他是在之前赋值的，所以还没有解析相当于赋值无效。

![微信图片_20230520164733](C:\Users\HP\Desktop\golong_blockChain-master\pictures\微信图片_20230520164733.jpg)

### 文件分离-美化

如果把所有的CLI命令都放在一个文件里，会很乱，因此可以建多个cli文件来使看起来更加美观

### go GETENV

GO语言"os"包中"Getenv"函数的用法及代码示例。

**用法:**

```go
func Getenv(key string) string
```

Getenv 检索由键命名的环境变量的值。它返回值，如果变量不存在，该值将为空。

**例子：**

```go
package main

import (
    "fmt"
    "os"
)

func main() {
    os.Setenv("NAME", "gopher")
    os.Setenv("BURROW", "/usr/gopher")

    fmt.Printf("%s lives in %s.\n", os.Getenv("NAME"), os.Getenv("BURROW"))

}
```

**输出：**

```go
gopher lives in /usr/gopher.
```

### 应用

在本实验中，使用CLI来作为终端，执行相关的指令。

运行逻辑

>设置cmd关键字->读取到关键字->获取内容->跳转至相关函数



#### 1.创建创世区块

```go
//创建创世区块
func (cli *CLI) creatBlockChain(address string) {

	//创建交易
	blockchain := CreateGenesisBlockchain(address)
	defer blockchain.DB.Close()

	//utxo数据表
	UTXOSet:=&UTXOSet{blockchain}
	UTXOSet.ResetUTXOset()
}
```

#### 2.创建钱包

```go
func (cli *CLI) CreateWallet() {

	wallets, _ := NewWallets()
	address := wallets.CreateNewWallet()
	//保存数据
	//wallets.SaveToFile(nodeID)
	//fmt.Println(wallets.Wallets)
	fmt.Printf("你的钱包地址为: %s\n", address)
	fmt.Println("钱包个数为", len(wallets.Wallets)) //获取长度

}
```

#### 3.获取钱包

```go
func (cli *CLI) addressLLists() []string {
	fmt.Println("打印所有的钱包地址：")
	wallets, _ := NewWallets()
	for address, _ := range wallets.Wallets {
		fmt.Println(address)
	}
	return nil
}

```

#### 4.获得钱包金额

```go
//查询余额
func (cli *CLI) getBalance(address string) {
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	utxoset := &UTXOSet{blockchain}
	amount := utxoset.GetBalance(address)
	fmt.Printf("%s一共有%d个Token\n", address, amount)
}

```

#### 5.输出链

```go
//输出区块链数据
func (cli *CLI) printChain() {
	if !dbExists() {
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	//调用函数
	blockchain.PrintChain()

}
```

#### 6.转账

```go
func (cli *CLI) send(from, to string, amount int, nodeID string, mineNow bool) {
	if !ValidateAddress(from) {
		log.Panic("ERROR: 发送者地址不正确")
	}
	if !ValidateAddress(to) {
		log.Panic("ERROR: 接收者地址不正确")
	}

	bc := NewBlockchain(nodeID)
	UTXOSet := UTXOSet{bc}
	defer bc.db.Close()

	wallets, err := NewWallets(nodeID)
	if err != nil {
		log.Panic(err)
	}
	wallet := wallets.GetWallet(from)

	tx := NewUTXOTransaction(&wallet, to, amount, &UTXOSet)

	if mineNow {
		cbTx := NewCoinbaseTX(from, "")
		txs := []*Transaction{cbTx, tx}

		newBlock := bc.MineBlock(txs)
		UTXOSet.Update(newBlock)
	} else {
		sendTx(knownNodes[0], tx)
	}

	fmt.Println("成功!")
}

```

#### 7.开始节点

```go
func (cli *CLI) startNode(nodeID, minerAddress string) {
	fmt.Printf("运行节点： %s\n", nodeID)
	if len(minerAddress) > 0 {
		if ValidateAddress(minerAddress) {//|| minerAddress == "" 
			fmt.Println("开始挖矿: ", minerAddress)
		} else {
			log.Panic("错误矿工地址!")
		}
	}
	StartServer(nodeID, minerAddress)
}
```



## 十、网络

### 如何实现区块链网络

区块链网络是去中心化的，这意味着没有服务器，客户端也不需要依赖服务器来获取或处理数据。在区块链网络中，有的是节点，每个节点是网络的一个完全（full-fledged）成员。节点就是一切：它既是一个客户端，也是一个服务器。这一点需要牢记于心，因为这与传统的网页应用非常不同。

为了在目前的区块链原型中实现网络，我们不得不简化一些事情。因为我们没有那么多的计算机来模拟一个多节点的网络。当然，我们可以使用虚拟机或是 Docker 来解决这个问题，但是这会使一切都变得更复杂：将不得不先解决可能出现的虚拟机或 Docker 问题，而我们的目标是将全部精力都放在区块链实现上。所以，我们想要在一台机器上运行多个区块链节点，同时希望它们有不同的地址。为了实现这一点，我们将使用端口号作为节点标识符，而不是使用 IP 地址。我们叫它端口节点（port node） ID，并使用环境变量 `NODE_ID` 对它们进行设置。故而，我们使用多个终端窗口，设置不同的 `NODE_ID` 运行不同的节点。

#### 实现

在 Bitcoin Core 中硬编码一个地址，已经被证实是一个错误：因为节点可能会被攻击或关机，这会导致新的节点无法加入到网络中。在 Bitcoin Core 中，硬编码了 DNS seeds。虽然这些并不是节点，但是 DNS 服务器知道一些节点的地址。当你启动一个全新的 Bitcoin Core 时，它会连接到一个种子节点，获取全节点列表，随后从这些节点中下载区块链。

不过在我们目前的实现中，因为时间能力不足，我们无法做到完全的去中心化，因为会出现中心化的特点。我们会有三个节点：

1. 一个中心节点。所有其他节点都会连接到这个节点，这个节点会在其他节点之间发送数据。
2. 一个矿工节点。这个节点会在内存池中存储新的交易，当有足够的交易时，它就会打包挖出一个新块。
3. 一个钱包节点。这个节点会被用作在钱包之间发送币。但是与 SPV 节点不同，它存储了区块链的一个完整副本。

#### 场景

目标是实现如下场景：

1. 中心节点创建一个区块链。
2. 一个其他（钱包）节点连接到中心节点并下载区块链。
3. 另一个（矿工）节点连接到中心节点并下载区块链。
4. 钱包节点创建一笔交易。
5. 矿工节点接收交易，并将交易保存到内存池中。
6. 当内存池中有足够的交易时，矿工开始挖一个新块。
7. 当挖出一个新块后，将其发送到中心节点。
8. 钱包节点与中心节点进行同步。
9. 钱包节点的用户检查他们的支付是否成功。

这就是比特币中的一般流程。尽管我们不会实现一个真实的 P2P 网络，但是我们会实现一个真实，也是比特币最常见最重要的用户场景。

### 什么是p2p

区块链网络是一个 p2p（Peer-to-Peer，端到端）的网络，即节点直接连接到其他节点。它的拓扑是扁平的，因为在节点的世界中没有层级之分。下面是它的示意图：

![127313-3c4174ad7cf6edf3](C:\Users\HP\Desktop\golong_blockChain-master\pictures\127313-3c4174ad7cf6edf3.png)

要实现这样一个网络节点更加困难，因为它们必须执行很多操作。每个节点必须与很多其他节点进行交互，它必须请求其他节点的状态，与自己的状态进行比较，当状态过时时进行更新。 

#### p2p实现原理

P2P 全称是 Peer to Peer ,翻译成中文”地位对等的两个节点之间“，亦或者”点对点“。区别于现在诸多 ”C/S“（客户端/服务器）模式。

![屏幕截图 2023-05-20 160446](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 160446.png)





### 实现p2p

因为时间问题，实现了p2p的基本框架，主要是要研究p2p和区块链在网络上的运行规律和原因。

假如要在 Server 下载 1G 的视频。

对与传统的 C/S 模式，从上图中我们就可以看出一些问题。

·如果服务器挂了，那我们视频肯定也下载不了了。

·如果下载视频的人变多，服务器的带宽就是制约下载速度的瓶颈，下载的人越多，下载速度越慢，深夜可真是急死个人啊。（通往彼岸的桥只有一座，去的人多了，能不挤么？）

·因为资源都集中在服务器，往往很会成为黑客攻击的目标。

P2P 打破了传统的 C/S 模式，在网络中的每个结点的地位都是对等的。每个结点既充当服务器，为其他结点提供服务，同时也享用其他结点提供的服务。

P2P 网络最大的特点是不需要中央服务器的调度，自我组织协调，各个节点之间可以直接通信。具体的通信协议有多种，常见的一种叫 Gossip ，翻译过来就是八卦协议。协议的基本通信原理非常简单，所有节点都会把信息传递给自己的邻居，就像村里长舌妇之间传闲话，或者白领在办公室传八卦。

#### p2p实现核心-打洞

借鉴网站如下：http://qjpcpu.github.io/blog/2018/01/26/p2pzhi-udpda-dong/

当今互联网到处存在着一些中间件(MIddleBoxes)，如NAT和防火墙，导致两个(不在同一内网)中的客户端无法直接通信。 这些问题即便是到了IPV6时代也会存在，因为即使不需要NAT，但还有其他中间件如防火墙阻挡了链接的建立。 目前部署的中间件多都是在C/S架构上设计的，其中相对隐匿的客户机主动向周知的服务端(拥有静态IP地址和DNS名称)发起链接请求。 大多数中间件实现了一种非对称的通讯模型，即内网中的主机可以初始化对外的链接，而外网的主机却不能初始化对内网的链接， 除非经过中间件管理员特殊配置。

在中间件为常见的NAPT的情况下（也是本文主要讨论的），内网中的客户端没有单独的公网IP地址， 而是通过NAPT转换，和其他同一内网用户共享一个公网IP。这种内网主机隐藏在中间件后的不可访问性对于一些客户端软件如浏览器来说 并不是一个问题，因为其只需要初始化对外的链接，从某方面来看反而还对隐私保护有好处。然而在P2P应用中， 内网主机（客户端）需要对另外的终端（Peer）直接建立链接，但是发起者和响应者可能在不同的中间件后面， 两者都没有公网IP地址。而外部对NAT公网IP和端口主动的链接或数据都会因内网未请求被丢弃掉。本文讨论的就是如何跨越NAT实现内网主机直接通讯的问题。

##### 网络模型

假设客户端A和客户端B的地址都是内网地址，且在不同的NAT后面。A、B上运行的P2P应用程序和服务器S都使用了UDP端口9982，A和B分别初始化了 与Server的UDP通信，地址映射如图所示:

```go
Server S
                    207.148.70.129:9981
                           |
                           |
    +----------------------|----------------------+
    |                                             |
  NAT A                                         NAT B
120.27.209.161:6000                            120.26.10.118:3000
    |                                             |
    |                                             |
 Client A                                      Client B
  10.0.0.1:9982                                 192.168.0.1:9982
```

现在假设客户端A打算与客户端B直接建立一个UDP通信会话。如果A直接给B的公网地址120.26.10.118:3000发送UDP数据，NAT B将很可能会无视进入的 数据（除非是Full Cone NAT），因为源地址和端口与S不匹配，而最初只与S建立过会话。B往A直接发信息也类似。

假设A开始给B的公网地址发送UDP数据的同时，给服务器S发送一个中继请求，要求B开始给A的公网地址发送UDP信息。A往B的输出信息会导致NAT A打开 一个A的内网地址与与B的外网地址之间的新通讯会话，B往A亦然。一旦新的UDP会话在两个方向都打开之后，客户端A和客户端B就能直接通讯， 而无须再通过引导服务器S了。

UDP打洞技术有许多有用的性质。一旦一个的P2P链接建立，链接的双方都能反过来作为“引导服务器”来帮助其他中间件后的客户端进行打洞， 极大减少了服务器的负载。应用程序不需要知道中间件具体是什么（如果有的话），因为以上的过程在没有中间件或者有多个中间件的情况下 也一样能建立通信链路。

#### 打洞流程

假设A现在希望建立一条到B的udp会话，那么这个建立基本流程是:

```go
1. A,B分别建立到Server S的udp会话,那么Server S此时是知道A,B各自的外网ip+端口
2. Server S在和B的udp会话里告诉A的地址(外网ip+端口: 120.27.209.161:6000),同理把B的地址(120.26.10.118:3000)告诉A
3. B向A地址(120.27.209.161:6000)发送一个"握手"udp包,打通A->B的udp链路
4. 此时A可以向B(120.26.10.118:3000)发送udp包,A->B的会话建立成功 	
```

#### 源码示例

使用三台设备模拟,外网设备207.148.70.129模拟Server S,执行server.go代码:

```go
package main

import (
	"fmt"
	"log"
	"net"
	"time"
)

func main() {
	listener, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.IPv4zero, Port: 9981})
	if err != nil {
		fmt.Println(err)
		return
	}
	log.Printf("本地地址: <%s> \n", listener.LocalAddr().String())
	peers := make([]net.UDPAddr, 0, 2)
	data := make([]byte, 1024)
	for {
		n, remoteAddr, err := listener.ReadFromUDP(data)
		if err != nil {
			fmt.Printf("error during read: %s", err)
		}
		log.Printf("<%s> %s\n", remoteAddr.String(), data[:n])
		peers = append(peers, *remoteAddr)
		if len(peers) == 2 {

			log.Printf("进行UDP打洞,建立 %s <--> %s 的连接\n", peers[0].String(), peers[1].String())
			listener.WriteToUDP([]byte(peers[1].String()), &peers[0])
			listener.WriteToUDP([]byte(peers[0].String()), &peers[1])
			time.Sleep(time.Second * 8)
			log.Println("中转服务器退出,仍不影响peers间通信")
			return
		}
	}
}
```

另外两台分别位于不同内网后的设备,均运行相同代码peer.go:

```go
package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

var tag string

const HAND_SHAKE_MSG = "我是打洞消息"

func main() {
	// 当前进程标记字符串,便于显示
	tag = os.Args[1]
	srcAddr := &net.UDPAddr{IP: net.IPv4zero, Port: 9982} // 注意端口必须固定
	dstAddr := &net.UDPAddr{IP: net.ParseIP("207.148.70.129"), Port: 9981}
	conn, err := net.DialUDP("udp", srcAddr, dstAddr)
	if err != nil {
		fmt.Println(err)
	}
	if _, err = conn.Write([]byte("hello, I'm new peer:" + tag)); err != nil {
		log.Panic(err)
	}
	data := make([]byte, 1024)
	n, remoteAddr, err := conn.ReadFromUDP(data)
	if err != nil {
		fmt.Printf("error during read: %s", err)
	}
	conn.Close()
	anotherPeer := parseAddr(string(data[:n]))
	fmt.Printf("local:%s server:%s another:%s\n", srcAddr, remoteAddr, anotherPeer.String())

	// 开始打洞
	bidirectionHole(srcAddr, &anotherPeer)

}

func parseAddr(addr string) net.UDPAddr {
	t := strings.Split(addr, ":")
	port, _ := strconv.Atoi(t[1])
	return net.UDPAddr{
		IP:   net.ParseIP(t[0]),
		Port: port,
	}
}

func bidirectionHole(srcAddr *net.UDPAddr, anotherAddr *net.UDPAddr) {
	conn, err := net.DialUDP("udp", srcAddr, anotherAddr)
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	// 向另一个peer发送一条udp消息(对方peer的nat设备会丢弃该消息,非法来源),用意是在自身的nat设备打开一条可进入的通道,这样对方peer就可以发过来udp消息
	if _, err = conn.Write([]byte(HAND_SHAKE_MSG)); err != nil {
		log.Println("send handshake:", err)
	}
	go func() {
		for {
			time.Sleep(10 * time.Second)
			if _, err = conn.Write([]byte("from [" + tag + "]")); err != nil {
				log.Println("send msg fail", err)
			}
		}
	}()
	for {
		data := make([]byte, 1024)
		n, _, err := conn.ReadFromUDP(data)
		if err != nil {
			log.Printf("error during read: %s\n", err)
		} else {
			log.Printf("收到数据:%s\n", data[:n])
		}
	}
}
```

注意代码仅模拟打洞基础流程,如果测试网络情况较差发生udp丢包,可能看不到预期结果,此时简单重启server,peer即可.



#### 在实验中模拟实现p2p

##### 代码实现

节点通过消息（message）进行交流。当一个新的节点开始运行时，它会从一个 DNS 种子获取几个节点，给它们发送 `version` 消息，在我们的实现看起来就像是这样：

```go
type version struct {
    Version    int
    BestHeight int
    AddrFrom   string
}
```

由于我们仅有一个区块链版本，所以 `Version` 字段实际并不会存储什么重要信息。`BestHeight` 存储区块链中节点的高度。`AddFrom` 存储发送者的地址。

接收到 `version` 消息的节点应该做什么呢？它会响应自己的 `version` 消息。这是一种握手：如果没有事先互相问候，就不可能有其他交流。不过，这并不是出于礼貌：`version` 用于找到一个更长的区块链。当一个节点接收到 `version` 消息，它会检查本节点的区块链是否比 `BestHeight` 的值更大。如果不是，节点就会请求并下载缺失的块。

为了接收消息，我们需要一个服务器：

```go
var nodeAddress string
var knownNodes = []string{"localhost:3000"}

func StartServer(nodeID, minerAddress string) {
    nodeAddress = fmt.Sprintf("localhost:%s", nodeID)
    miningAddress = minerAddress
    ln, err := net.Listen(protocol, nodeAddress)
    defer ln.Close()

    bc := NewBlockchain(nodeID)

    if nodeAddress != knownNodes[0] {
        sendVersion(knownNodes[0], bc)
    }

    for {
        conn, err := ln.Accept()
        go handleConnection(conn, bc)
    }
}
```

首先，我们对中心节点的地址进行硬编码：因为每个节点必须知道从何处开始初始化。`minerAddress` 参数指定了接收挖矿奖励的地址。代码片段：

```go
if nodeAddress != knownNodes[0] {
    sendVersion(knownNodes[0], bc)
}
```

这意味着如果当前节点不是中心节点，它必须向中心节点发送 `version` 消息来查询是否自己的区块链已过时。

```go
func sendVersion(addr string, bc *Blockchain) {
    bestHeight := bc.GetBestHeight()
    payload := gobEncode(version{nodeVersion, bestHeight, nodeAddress})

    request := append(commandToBytes("version"), payload...)

    sendData(addr, request)
}
```

我们的消息，在底层就是字节序列。前 12 个字节指定了命令名（比如这里的 `version`），后面的字节会包含 **gob** 编码的消息结构，`commandToBytes` 看起来是这样：

```go
func commandToBytes(command string) []byte {
    var bytes [commandLength]byte

    for i, c := range command {
        bytes[i] = byte(c)
    }

    return bytes[:]
}
```

它创建一个 12 字节的缓冲区，并用命令名进行填充，将剩下的字节置为空。下面一个相反的函数：

```go
func bytesToCommand(bytes []byte) string {
    var command []byte

    for _, b := range bytes {
        if b != 0x0 {
            command = append(command, b)
        }
    }

    return fmt.Sprintf("%s", command)
}
```

当一个节点接收到一个命令，它会运行 `bytesToCommand` 来提取命令名，并选择正确的处理器处理命令主体：

```go
func handleConnection(conn net.Conn, bc *Blockchain) {
    request, err := ioutil.ReadAll(conn)
    command := bytesToCommand(request[:commandLength])
    fmt.Printf("Received %s command\n", command)

    switch command {
    ...
    case "version":
        handleVersion(request, bc)
    default:
        fmt.Println("Unknown command!")
    }

    conn.Close()
}
```

下面是 `version` 命令处理器：

```go
func handleVersion(request []byte, bc *Blockchain) {
    var buff bytes.Buffer
    var payload verzion

    buff.Write(request[commandLength:])
    dec := gob.NewDecoder(&buff)
    err := dec.Decode(&payload)

    myBestHeight := bc.GetBestHeight()
    foreignerBestHeight := payload.BestHeight

    if myBestHeight < foreignerBestHeight {
        sendGetBlocks(payload.AddrFrom)
    } else if myBestHeight > foreignerBestHeight {
        sendVersion(payload.AddrFrom, bc)
    }

    if !nodeIsKnown(payload.AddrFrom) {
        knownNodes = append(knownNodes, payload.AddrFrom)
    }
}
```

首先，我们需要对请求进行解码，提取有效信息。所有的处理器在这部分都类似，所以我们会下面的代码片段中略去这部分。

然后节点将从消息中提取的 `BestHeight` 与自身进行比较。如果自身节点的区块链更长，它会回复 `version` 消息；否则，它会发送 `getblocks` 消息。

##### getblocks

```go
type getblocks struct {
    AddrFrom string
}
```

`getblocks` 意为 “给我看一下你有什么区块”（在比特币中，这会更加复杂）。注意，它并没有说“把你全部的区块给我”，而是请求了一个块哈希的列表。这是为了减轻网络负载，因为区块可以从不同的节点下载，并且我们不想从一个单一节点下载数十 GB 的数据。

处理命令十分简单：

```go
func handleGetBlocks(request []byte, bc *Blockchain) {
    ...
    blocks := bc.GetBlockHashes()
    sendInv(payload.AddrFrom, "block", blocks)
}
```

在我们简化版的实现中，它会返回 **所有块哈希**。

##### inv

```go
type inv struct {
    AddrFrom string
    Type     string
    Items    [][]byte
}
```

比特币使用 `inv` 来向其他节点展示当前节点有什么块和交易。再次提醒，它没有包含完整的区块链和交易，仅仅是哈希而已。`Type` 字段表明了这是块还是交易。

处理 `inv` 稍显复杂：

```go
func handleInv(request []byte, bc *Blockchain) {
    ...
    fmt.Printf("Recevied inventory with %d %s\n", len(payload.Items), payload.Type)

    if payload.Type == "block" {
        blocksInTransit = payload.Items

        blockHash := payload.Items[0]
        sendGetData(payload.AddrFrom, "block", blockHash)

        newInTransit := [][]byte{}
        for _, b := range blocksInTransit {
            if bytes.Compare(b, blockHash) != 0 {
                newInTransit = append(newInTransit, b)
            }
        }
        blocksInTransit = newInTransit
    }

    if payload.Type == "tx" {
        txID := payload.Items[0]

        if mempool[hex.EncodeToString(txID)].ID == nil {
            sendGetData(payload.AddrFrom, "tx", txID)
        }
    }
}
```

如果收到块哈希，我们想要将它们保存在 `blocksInTransit` 变量来跟踪已下载的块。这能够让我们从不同的节点下载块。在将块置于传送状态时，我们给 `inv` 消息的发送者发送 `getdata` 命令并更新 `blocksInTransit`。在一个真实的 P2P 网络中，我们会想要从不同节点来传送块。

在我们的实现中，我们永远也不会发送有多重哈希的 `inv`。这就是为什么当 `payload.Type == "tx"` 时，只会拿到第一个哈希。然后我们检查是否在内存池中已经有了这个哈希，如果没有，发送 `getdata` 消息。

##### getdata

```go
type getdata struct {
    AddrFrom string
    Type     string
    ID       []byte
}
```

`getdata` 用于某个块或交易的请求，它可以仅包含一个块或交易的 ID。

```go
func handleGetData(request []byte, bc *Blockchain) {
    ...
    if payload.Type == "block" {
        block, err := bc.GetBlock([]byte(payload.ID))

        sendBlock(payload.AddrFrom, &block)
    }

    if payload.Type == "tx" {
        txID := hex.EncodeToString(payload.ID)
        tx := mempool[txID]

        sendTx(payload.AddrFrom, &tx)
    }
}
```

这个处理器比较地直观：如果它们请求一个块，则返回块；如果它们请求一笔交易，则返回交易。注意，我们并不检查实际上是否已经有了这个块或交易。这是一个缺陷 :)

##### block 和 tx

```go
type block struct {
    AddrFrom string
    Block    []byte
}

type tx struct {
    AddFrom     string
    Transaction []byte
}
```

实际完成数据转移的正是这些消息。

处理 `block` 消息十分简单：

```go
func handleBlock(request []byte, bc *Blockchain) {
    ...

    blockData := payload.Block
    block := DeserializeBlock(blockData)

    fmt.Println("Recevied a new block!")
    bc.AddBlock(block)

    fmt.Printf("Added block %x\n", block.Hash)

    if len(blocksInTransit) > 0 {
        blockHash := blocksInTransit[0]
        sendGetData(payload.AddrFrom, "block", blockHash)

        blocksInTransit = blocksInTransit[1:]
    } else {
        UTXOSet := UTXOSet{bc}
        UTXOSet.Reindex()
    }
}
```

当接收到一个新块时，我们把它放到区块链里面。如果还有更多的区块需要下载，我们继续从上一个下载的块的那个节点继续请求。当最后把所有块都下载完后，对 UTXO 集进行重新索引。

> TODO：并非无条件信任，我们应该在将每个块加入到区块链之前对它们进行验证。
>
> TODO: 并非运行 UTXOSet.Reindex()， 而是应该使用 UTXOSet.Update(block)，因为如果区块链很大，它将需要很多时间来对整个 UTXO 集重新索引。

处理 `tx` 消息是最困难的部分：

```go
func handleTx(request []byte, bc *Blockchain) {
    ...
    txData := payload.Transaction
    tx := DeserializeTransaction(txData)
    mempool[hex.EncodeToString(tx.ID)] = tx

    if nodeAddress == knownNodes[0] {
        for _, node := range knownNodes {
            if node != nodeAddress && node != payload.AddFrom {
                sendInv(node, "tx", [][]byte{tx.ID})
            }
        }
    } else {
        if len(mempool) >= 2 && len(miningAddress) > 0 {
        MineTransactions:
            var txs []*Transaction

            for id := range mempool {
                tx := mempool[id]
                if bc.VerifyTransaction(&tx) {
                    txs = append(txs, &tx)
                }
            }

            if len(txs) == 0 {
                fmt.Println("All transactions are invalid! Waiting for new ones...")
                return
            }

            cbTx := NewCoinbaseTX(miningAddress, "")
            txs = append(txs, cbTx)

            newBlock := bc.MineBlock(txs)
            UTXOSet := UTXOSet{bc}
            UTXOSet.Reindex()

            fmt.Println("New block is mined!")

            for _, tx := range txs {
                txID := hex.EncodeToString(tx.ID)
                delete(mempool, txID)
            }

            for _, node := range knownNodes {
                if node != nodeAddress {
                    sendInv(node, "block", [][]byte{newBlock.Hash})
                }
            }

            if len(mempool) > 0 {
                goto MineTransactions
            }
        }
    }
}
```

首先要做的事情是将新交易放到内存池中（再次提醒，在将交易放到内存池之前，必要对其进行验证）。下个片段：

```go
if nodeAddress == knownNodes[0] {
    for _, node := range knownNodes {
        if node != nodeAddress && node != payload.AddFrom {
            sendInv(node, "tx", [][]byte{tx.ID})
        }
    }
}
```

检查当前节点是否是中心节点。在我们的实现中，中心节点并不会挖矿。它只会将新的交易推送给网络中的其他节点。

下一个很大的代码片段是矿工节点“专属”。让我们对它进行一下分解:

```go
if len(mempool) >= 2 && len(miningAddress) > 0 {
```

`miningAddress` 只会在矿工节点上设置。如果当前节点（矿工）的内存池中有两笔或更多的交易，开始挖矿：

```go
for id := range mempool {
    tx := mempool[id]
    if bc.VerifyTransaction(&tx) {
        txs = append(txs, &tx)
    }
}

if len(txs) == 0 {
    fmt.Println("All transactions are invalid! Waiting for new ones...")
    return
}
```

首先，内存池中所有交易都是通过验证的。无效的交易会被忽略，如果没有有效交易，则挖矿中断。

```go
cbTx := NewCoinbaseTX(miningAddress, "")
txs = append(txs, cbTx)

newBlock := bc.MineBlock(txs)
UTXOSet := UTXOSet{bc}
UTXOSet.Reindex()

fmt.Println("New block is mined!")
```

验证后的交易被放到一个块里，同时还有附带奖励的 coinbase 交易。当块被挖出来以后，UTXO 集会被重新索引。

> TODO: 提醒，应该使用 UTXOSet.Update 而不是 UTXOSet.Reindex.

```go
for _, tx := range txs {
    txID := hex.EncodeToString(tx.ID)
    delete(mempool, txID)
}

for _, node := range knownNodes {
    if node != nodeAddress {
        sendInv(node, "block", [][]byte{newBlock.Hash})
    }
}

if len(mempool) > 0 {
    goto MineTransactions
}
```

当一笔交易被挖出来以后，就会被从内存池中移除。当前节点所连接到的所有其他节点，接收带有新块哈希的 `inv` 消息。在处理完消息后，它们可以对块进行请求。

#### 演示

##### 使用说明

![image-20230508174012717](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508174012717.png)

##### 过程

1.在A创建一个钱包和区块

```ini
main createwallet
```

![image-20230508215018810](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508215018810.png)

在A创建一个区块（此时为创世区块）

```ini
main createblockchain -address 1GufgQ2yCj6DnH1JtD37k74ugYdTyLiK5B
```

检查一下

```ini
main printchain
```

![image-20230508215106894](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508215106894.png)

2.现在打开一个新设备B，在B生成一些钱包

```ini
main createwallet
```

获取生成的钱包：

```ini
main listaddresses
```

```
1CW3yv5QhYeSq2tyAzAGE7eRXZyvKvCRh2
1P2C1s6Kru215uXezMWJWKwH1iY2zqGrqb
14qkqDgPSUNHcVuHFmWLPo3HQ3WyQBgMzz
```



3.A用它的钱包向B的钱包发点token

先切换到A，然后用A的钱包向B的钱包发送10个token，其中-mine标志指的是块会立刻被同一节点挖出来。我们必须要有这个标志，因为初始状态时，网络中没有矿工节点。

向1CW3yv5QhYeSq2tyAzAGE7eRXZyvKvCRh2发送10个token

```ini
main send -from 1GufgQ2yCj6DnH1JtD37k74ugYdTyLiK5B -to 1CW3yv5QhYeSq2tyAzAGE7eRXZyvKvCRh2 -amount 10 -mine
```

向B的1P2C1s6Kru215uXezMWJWKwH1iY2zqGrqb 发送5个token

```ini
main send -from 1GufgQ2yCj6DnH1JtD37k74ugYdTyLiK5B -to 1P2C1s6Kru215uXezMWJWKwH1iY2zqGrqb -amount 5 -mine
```启动节点,启动后，节点服务会持续运行
```

```ini
main startnode
```

![image-20230508220111241](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508220111241.png)

4.在B中查看钱包

在B中启动节点,此时，他会从中心节点下载数据，更新本地钱包。

```ini
main startnode
```

获取钱包查看金额，可以看到此时，B的钱包一个有了10token，一个有了5token，没有出错

```ini
main getbalance -address 1CW3yv5QhYeSq2tyAzAGE7eRXZyvKvCRh2
余额 '1CW3yv5QhYeSq2tyAzAGE7eRXZyvKvCRh2': 10
main getbalance -address 1P2C1s6Kru215uXezMWJWKwH1iY2zqGrqb
余额 '1P2C1s6Kru215uXezMWJWKwH1iY2zqGrqb': 5
```

5.挖矿，传送账单

打开一个新设备C，创建钱包地址

![image-20230508222136159](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508222136159.png)



启动节点，将他设置为矿工节点（在挖矿的节点）

```ini
main startnode -miner 1PdTEpegLqLHtoRr6i1Ub3TuqbV9ERvtWm
```

![image-20230508222512280](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508222512280.png)

6.再次发送一些token

这会用B的第一个钱包向A发送3个token

```ini
main send -from 1CW3yv5QhYeSq2tyAzAGE7eRXZyvKvCRh2 -to 1GufgQ2yCj6DnH1JtD37k74ugYdTyLiK5B -amount 3 
```

此时看向矿工节点C，接受到了utxo转账，且挖出新的区块

![image-20230508223647128](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508223647128.png)

 中心结点A，可以看到我发出各种指令时的信息。

![image-20230508224021792](http://shark-purple-s-image.oss-cn-hangzhou.aliyuncs.com/img/image-20230508224021792.png)

在B中启动结点，开始下载最近的信息，加载钱包信息

```ini
main getbalance -address 1GufgQ2yCj6DnH1JtD37k74ugYdTyLiK5B
余额 '1GufgQ2yCj6DnH1JtD37k74ugYdTyLiK5B': 15
main getbalance -address 1PdTEpegLqLHtoRr6i1Ub3TuqbV9ERvtWm
余额 '1PdTEpegLqLHtoRr6i1Ub3TuqbV9ERvtWm': 10
```

ALL DONE!



## 其它

### go

#### 所用到的需要安装地go包

1.boltdb数据库

```go
go get github.com/boltdb/bolt/...
```

2.rimped160

```go
go get "golong.org/x/crypto/ripemd160"
```

### gitee

1.上传文件语句

进入需要上传的文件夹，右击打开Git Bash窗口，输入

```
git init
```

进行初始化，会在目录下创建一个.git的本地仓库

![屏幕截图 2023-05-20 180215](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 180215.png)

2.将指定文件添加至暂存区

提交某个文件：

```
git add 文件全称（可以是文件夹，也可以是多个文件，空格隔开）
```

提交全部：

```
git add .
```

![屏幕截图 2023-05-20 180241](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 180241.png)

3.将暂存区内容提交至本地仓库

```
git commit -m “  ”
```

-m 是添加描述，“ ” 里面的是可以自行修改的

![屏幕截图 2023-05-20 180252](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 180252.png)

4.添加远程仓库

```
git remote add origin “远程仓库地址”
```

![屏幕截图 2023-05-20 180305](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 180305.png)

5.将本地仓库push至远程仓库

```
git push -u origin master
```

![屏幕截图 2023-05-20 180318](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 180318.png)

![屏幕截图 2023-05-20 180330](C:\Users\HP\Desktop\golong_blockChain-master\pictures\屏幕截图 2023-05-20 180330.png)

### 实验心得

从技术角度来看，基于Go语言开发公链是很有前景的一项开发工作。Go语言具有并发性能和内存管理能力等方面的特点，也有很多优秀的开源库可以使用。在开发公链时，需要借助团队协作等多方面因素才能保证项目的成功。此外，开发者需要具备深厚的相关知识和编程技能，在此次实验中我们小组成员从中学到了很多。总之，公链开发是一项挑战性极高的工作，由于时间短暂，此次实验还需要不断完善和优化，由此达到更好的效果。

### 参考资料

1.《go语言核心编程》

作者：李文塔

出版社：电子工业出版社

2.《go语言公链开发实战》

作者：郑东旭 杨明珠 潘盈瑜 翟萌

出版社：机械工业出版社

3.《精通区块链编程：加密货币原理、方法和应用开发》

作者：[希]安德烈亚斯·M.安东波罗斯 

译者：郭理靖,李国鹏,李卓

出版社：机械工业出版社

4.介绍比特币的网站：

网址：https://bitcoin.org/zh_CN/how-it-works

发布人： Bitcoin Project 2009-2023 基于MIT协议授权发布
