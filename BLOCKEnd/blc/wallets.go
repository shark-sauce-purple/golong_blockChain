package blc

import (
	"bytes"
	"crypto/elliptic"
	"encoding/gob"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

const walletFile = "wallet_%s.dat" // 钱包文件名模板

// Wallets 存储一组钱包
type Wallets struct {
	Wallets map[string]*Wallet // 钱包集合
}

// NewWallets 创建并返回一个 Wallets，如果文件存在，则从文件中加载钱包
func NewWallets(nodeID string) (*Wallets, error) {
	wallets := Wallets{}
	wallets.Wallets = make(map[string]*Wallet) // 初始化钱包集合

	err := wallets.LoadFromFile(nodeID) // 从文件中加载钱包

	return &wallets, err
}

// CreateWallet 创建一个新的钱包，并把它添加到 Wallets 中
func (ws *Wallets) CreateWallet() string {
	wallet := NewWallet()                             // 创建新的钱包
	address := fmt.Sprintf("%s", wallet.GetAddress()) // 获取钱包地址

	ws.Wallets[address] = wallet // 将钱包添加到集合中

	return address
}

// GetAddresses 返回所有钱包地址的数组
func (ws *Wallets) GetAddresses() []string {
	var addresses []string

	for address := range ws.Wallets {
		addresses = append(addresses, address)
	}

	return addresses
}

// GetWallet 根据地址返回对应的钱包
func (ws Wallets) GetWallet(address string) Wallet {
	return *ws.Wallets[address]
}

// LoadFromFile 从文件中加载钱包
func (ws *Wallets) LoadFromFile(nodeID string) error {
	walletFile := fmt.Sprintf(walletFile, nodeID)          // 拼接钱包文件名
	if _, err := os.Stat(walletFile); os.IsNotExist(err) { // 如果文件不存在，返回错误
		return err
	}

	fileContent, err := ioutil.ReadFile(walletFile) // 读取文件内容
	if err != nil {
		log.Panic(err)
	}

	var wallets Wallets
	gob.Register(elliptic.P256())                           // 注册椭圆曲线 P256
	decoder := gob.NewDecoder(bytes.NewReader(fileContent)) // 创建解码器
	err = decoder.Decode(&wallets)                          // 解码数据
	if err != nil {
		log.Panic(err)
	}

	ws.Wallets = wallets.Wallets // 将钱包集合复制到当前对象中

	return nil
}

// SaveToFile 将钱包保存到文件中
func (ws Wallets) SaveToFile(nodeID string) {
	var content bytes.Buffer
	walletFile := fmt.Sprintf(walletFile, nodeID) // 拼接钱包文件名

	gob.Register(elliptic.P256()) // 注册椭圆曲线 P256

	encoder := gob.NewEncoder(&content) // 创建编码器
	err := encoder.Encode(ws)           // 编码数据
	if err != nil {
		log.Panic(err)
	}

	err = ioutil.WriteFile(walletFile, content.Bytes(), 0644) // 将数据写入文件中
	if err != nil {
		log.Panic(err)
	}
}
