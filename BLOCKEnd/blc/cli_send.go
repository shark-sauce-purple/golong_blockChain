package blc

import (
    "fmt"
    "log"
)

func (cli *CLI) send(from, to string, amount int, nodeID string, mineNow bool) { // 发送一笔新交易
    if !ValidateAddress(from) { // 验证发件人地址是否有效
        log.Panic("错误：发件人地址无效")
    }
    if !ValidateAddress(to) { // 验证收件人地址是否有效
        log.Panic("错误：收件人地址无效")
    }

    bc := NewBlockchain(nodeID) // 根据节点ID创建区块链实例
    UTXOSet := UTXOSet{bc} // 创建UTXO集合实例
    defer bc.db.Close() // 关闭数据库连接

    wallets, err := NewWallets(nodeID) // 根据节点ID创建钱包实例
    if err != nil {
        log.Panic(err)
    }
    wallet := wallets.GetWallet(from) // 获取发件人钱包实例

    tx := NewUTXOTransaction(&wallet, to, amount, &UTXOSet) // 创建新的UTXO交易

    if mineNow { // 如果需要立即打包区块
        cbTx := NewCoinbaseTX(from, "") // 创建新的coinbase交易
        txs := []*Transaction{cbTx, tx} // 将coinbase交易和新的UTXO交易放入交易列表

        newBlock := bc.MineBlock(txs) // 挖出新的区块
        UTXOSet.Update(newBlock) // 更新UTXO集合
    } else { // 如果不需要立即打包区块
        sendTx(knownNodes[0], tx) // 将新交易发送给已知节点
    }

    fmt.Println("成功!") // 打印成功信息
}