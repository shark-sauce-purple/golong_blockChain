package blc

import (
    "bytes" //引入bytes包，用于处理字节切片
    "encoding/gob" //引入gob包，用于实现序列化和反序列化
    "log" //引入log包，用于实现日志输出
    "time" //引入time包，用于处理时间
)

//定义区块结构体
type Block struct {
    Timestamp     int64 //时间戳
    Transactions  []*Transaction //交易列表
    PrevBlockHash []byte //前一块区块的Hash值
    Hash          []byte //当前区块的Hash值
    Nonce         int //工作量证明算法的随机数
    Height        int //区块高度
}

//创建一个新的区块实例
func NewBlock(transactions []*Transaction, prevBlockHash []byte, height int) *Block {
    block := &Block{time.Now().Unix(), transactions, prevBlockHash, []byte{}, 0, height} //创建一个区块
    pow := NewProofOfWork(block) // 创建一个工作量证明实例
    nonce, hash := pow.Run() //运行工作量证明算法得到nonce和hash

    block.Hash = hash[:] //将hash值赋值给区块的Hash属性
    block.Nonce = nonce //将nonce值赋值给区块的Nonce属性

    return block //返回该新创建的区块
}

//创建并返回创世区块
func NewGenesisBlock(coinbase *Transaction) *Block {
    return NewBlock([]*Transaction{coinbase}, []byte{}, 0) //创建一个数据仅为coinbase交易的区块作为创世区块
}

//计算区块中所有交易的Merkle树根节点Hash值
func (b *Block) HashTransactions() []byte {
    var transactions [][]byte //定义一个二维字节数组，用于存储交易序列化后的字节数组
    for _, tx := range b.Transactions { //遍历区块中的每个交易
        transactions = append(transactions, tx.Serialize()) //序列化交易并添加到transactions列表中
    }
    mTree := NewMerkleTree(transactions) //使用交易列表构建一个Merkle树

    return mTree.RootNode.Data //返回Merkle树根节点的Hash值
}

//序列化区块
func (b *Block) Serialize() []byte {
    var result bytes.Buffer //定义一个缓冲区
    encoder := gob.NewEncoder(&result) //创建一个gob编码器

    err := encoder.Encode(b) //编码区块
    if err != nil {
        log.Panic(err)
    }

    return result.Bytes() //返回编码后的字节数组
}

//反序列化区块
func DeserializeBlock(d []byte) *Block {
    var block Block //定义一个Block类型的变量
    decoder := gob.NewDecoder(bytes.NewReader(d)) //创建一个gob解码器

    err := decoder.Decode(&block) //解码字节数据，并存入变量中
    if err != nil {
        log.Panic(err)
    }

    return &block //返回反序列化后的区块
}