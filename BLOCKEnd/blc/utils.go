package blc

import (
	"bytes"
	"encoding/binary"
	"log"
)

// IntToHex 将int64类型的数字转换成字节数组
func IntToHex(num int64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, num) //使用大端字节序将数字写入缓冲区
	if err != nil {
		log.Panic(err)
	}

	return buff.Bytes() //返回缓冲区中的字节数组
}

// ReverseBytes 反转字节数组
func ReverseBytes(data []byte) {
	for i, j := 0, len(data)-1; i < j; i, j = i+1, j-1 { //使用双指针反转字节数组
		data[i], data[j] = data[j], data[i]
	}
}
