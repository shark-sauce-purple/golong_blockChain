package blc

import "bytes"

// TXInput 表示一个交易输入
type TXInput struct {
	Txid      []byte //交易id
	Vout      int    //该交易的输出编号
	Signature []byte //签名
	PubKey    []byte //公钥
}

// UsesKey 检查给定的公钥哈希是否用于发起该交易
func (in *TXInput) UsesKey(pubKeyHash []byte) bool {
	lockingHash := HashPubKey(in.PubKey)
	//计算公钥对应的哈希
	return bytes.Compare(lockingHash, pubKeyHash) == 0
	//比较计算的哈希和传递的哈希是否相等
}
