package blc

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"log"

	"golang.org/x/crypto/ripemd160"
)

const version = byte(0x00)   // 版本号，用于生成地址
const addressChecksumLen = 4 // 地址校验和长度，单位为字节

// Wallet 存储私钥和公钥
type Wallet struct {
	PrivateKey ecdsa.PrivateKey // 私钥
	PublicKey  []byte           // 公钥
}

// NewWallet 创建并返回一个 Wallet
func NewWallet() *Wallet {
	private, public := newKeyPair()   // 生成新的密钥对
	wallet := Wallet{private, public} // 创建新的钱包对象

	return &wallet
}

// GetAddress 返回钱包地址
func (w Wallet) GetAddress() []byte {
	pubKeyHash := HashPubKey(w.PublicKey) // 哈希公钥

	versionedPayload := append([]byte{version}, pubKeyHash...) // 加上版本号
	checksum := checksum(versionedPayload)                     // 计算校验和

	fullPayload := append(versionedPayload, checksum...) // 加上校验和
	address := Base58Encode(fullPayload)                 // Base58 编码

	return address
}

// HashPubKey 哈希公钥
func HashPubKey(pubKey []byte) []byte {
	publicSHA256 := sha256.Sum256(pubKey) // 先使用 SHA256 哈希

	RIPEMD160Hasher := ripemd160.New() // 再使用 RIPEMD160 哈希
	_, err := RIPEMD160Hasher.Write(publicSHA256[:])
	if err != nil {
		log.Panic(err)
	}
	publicRIPEMD160 := RIPEMD160Hasher.Sum(nil)

	return publicRIPEMD160
}

// ValidateAddress 检查地址是否有效
func ValidateAddress(address string) bool {
	pubKeyHash := Base58Decode([]byte(address))                        // 解码地址
	actualChecksum := pubKeyHash[len(pubKeyHash)-addressChecksumLen:]  // 取出实际校验和
	version := pubKeyHash[0]                                           // 取出版本号
	pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-addressChecksumLen]    // 取出公钥哈希
	targetChecksum := checksum(append([]byte{version}, pubKeyHash...)) // 计算目标校验和

	// 比较实际校验和和目标校验和是否相等
	return bytes.Compare(actualChecksum, targetChecksum) == 0
}

// Checksum 为公钥生成校验和
func checksum(payload []byte) []byte {
	firstSHA := sha256.Sum256(payload)      // 先使用 SHA256 哈希
	secondSHA := sha256.Sum256(firstSHA[:]) // 再使用 SHA256 哈希

	return secondSHA[:addressChecksumLen] // 取出校验和
}

// newKeyPair 生成新的密钥对
func newKeyPair() (ecdsa.PrivateKey, []byte) {
	curve := elliptic.P256()                              // 使用 P256 椭圆曲线
	private, err := ecdsa.GenerateKey(curve, rand.Reader) // 生成新的私钥
	if err != nil {
		log.Panic(err)
	}
	pubKey := append(private.PublicKey.X.Bytes(), private.PublicKey.Y.Bytes()...) // 从私钥中提取公钥

	return *private, pubKey
}
