package blc

import (
    "fmt"
    "log"
)

// listAddresses列出所有钱包地址
func (cli *CLI) listAddresses(nodeID string) {
    wallets, err := NewWallets(nodeID) // 创建一个新的钱包
    if err != nil {
        log.Panic(err) // 如果发生错误，则记录错误并中止程序
    }
    addresses := wallets.GetAddresses() // 获取所有钱包地址

    for _, address := range addresses { // 循环打印钱包地址
        fmt.Println(address)
    }
}