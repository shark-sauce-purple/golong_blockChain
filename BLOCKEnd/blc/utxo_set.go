package blc

import (
	"encoding/hex"
	"log"

	"github.com/boltdb/bolt"
)

const utxoBucket = "chainstate" //定义UTXO集合的bucket名称

// UTXOSet 表示UTXO集合
type UTXOSet struct {
	Blockchain *Blockchain //UTXO集合所属的区块链
}

// FindSpendableOutputs 查找并返回可用于交易输入的未花费输出
func (u UTXOSet) FindSpendableOutputs(pubkeyHash []byte, amount int) (int, map[string][]int) {
	unspentOutputs := make(map[string][]int) //用于存储未花费的输出，键为交易ID，值为输出索引的列表
	accumulated := 0                         //用于存储累计的未花费输出金额
	db := u.Blockchain.db                    //获取UTXO集合所在的数据库

	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoBucket)) //获取UTXO集合所在的bucket
		c := b.Cursor()                    //创建游标

		for k, v := c.First(); k != nil; k, v = c.Next() { //遍历UTXO集合中的所有交易
			txID := hex.EncodeToString(k) //将交易ID转换为16进制字符串
			outs := DeserializeOutputs(v) //反序列化交易输出

			for outIdx, out := range outs.Outputs { //遍历所有交易输出
				if out.IsLockedWithKey(pubkeyHash) && accumulated < amount { //判断输出是否被公钥哈希锁定，以及累计的金额是否小于要求的金额
					accumulated += out.Value                                    //累加未花费输出金额
					unspentOutputs[txID] = append(unspentOutputs[txID], outIdx) //将未花费的输出添加到map中
				}
			}
		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}

	return accumulated, unspentOutputs //返回累计的未花费输出金额和未花费输出的map
}

// FindUTXO 查找公钥哈希对应的UTXO
func (u UTXOSet) FindUTXO(pubKeyHash []byte) []TXOutput {
	var UTXOs []TXOutput  //用于存储UTXO的列表
	db := u.Blockchain.db //获取UTXO集合所在的数据库

	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoBucket)) //获取UTXO集合所在的bucket
		c := b.Cursor()                    //创建游标

		for k, v := c.First(); k != nil; k, v = c.Next() { //遍历UTXO集合中的所有交易
			outs := DeserializeOutputs(v) //反序列化交易输出

			for _, out := range outs.Outputs { //遍历所有交易输出
				if out.IsLockedWithKey(pubKeyHash) { //判断输出是否被公钥哈希锁定
					UTXOs = append(UTXOs, out) //将UTXO添加到列表中
				}
			}
		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}

	return UTXOs //返回UTXO列表
}

// CountTransactions 返回UTXO集合中的交易数量
func (u UTXOSet) CountTransactions() int {
	db := u.Blockchain.db //获取UTXO集合所在的数据库
	counter := 0          //用于记录交易数量的计数器

	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoBucket)) //获取UTXO集合所在的bucket
		c := b.Cursor()                    //创建游标

		for k, _ := c.First(); k != nil; k, _ = c.Next() { //遍历UTXO集合中的所有交易
			counter++ //累加交易数量
		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}

	return counter //返回交易数量
}

// Reindex 重建UTXO集合
func (u UTXOSet) Reindex() {
	db := u.Blockchain.db            //获取UTXO集合所在的数据库
	bucketName := []byte(utxoBucket) //获取UTXO集合的bucket名称

	err := db.Update(func(tx *bolt.Tx) error {
		err := tx.DeleteBucket(bucketName) //删除UTXO集合的bucket
		if err != nil && err != bolt.ErrBucketNotFound {
			log.Panic(err)
		}

		_, err = tx.CreateBucket(bucketName) //创建新的UTXO集合的bucket
		if err != nil {
			log.Panic(err)
		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}

	UTXO := u.Blockchain.FindUTXO() //获取所有未花费输出

	err = db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketName) //获取UTXO集合所在的bucket

		for txID, outs := range UTXO { //遍历所有未花费输出
			key, err := hex.DecodeString(txID) //将交易ID转换为字节数组
			if err != nil {
				log.Panic(err)
			}

			err = b.Put(key, outs.Serialize()) //将未花费输出序列化并存储到UTXO集合中
			if err != nil {
				log.Panic(err)
			}
		}

		return nil
	})
}

// Update 更新UTXO集合
func (u UTXOSet) Update(block *Block) {
	db := u.Blockchain.db //获取UTXO集合所在的数据库

	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(utxoBucket)) //获取UTXO集合所在的bucket

		for _, tx := range block.Transactions { //遍历区块中的所有交易
			if tx.IsCoinbase() == false { //如果不是coinbase交易
				for _, vin := range tx.Vin { //遍历交易输入
					updatedOuts := TXOutputs{}
					outsBytes := b.Get(vin.Txid)          //获取交易ID对应的交易输出
					outs := DeserializeOutputs(outsBytes) //反序列化交易输出

					for outIdx, out := range outs.Outputs { //遍历交易输出
						if outIdx != vin.Vout { //如果不是当前交易输入对应的交易输出
							updatedOuts.Outputs = append(updatedOuts.Outputs, out) //将交易输出添加到updatedOuts中
						}
					}

					if len(updatedOuts.Outputs) == 0 { //如果updatedOuts中没有输出
						err := b.Delete(vin.Txid) //删除交易ID对应的交易输出
						if err != nil {
							log.Panic(err)
						}
					} else {
						err := b.Put(vin.Txid, updatedOuts.Serialize()) //将updatedOuts序列化并存储到UTXO集合中
						if err != nil {
							log.Panic(err)
						}
					}

				}
			}

			newOutputs := TXOutputs{}
			for _, out := range tx.Vout { //遍历交易输出
				newOutputs.Outputs = append(newOutputs.Outputs, out) //将交易输出添加到newOutputs中
			}

			err := b.Put(tx.ID, newOutputs.Serialize()) //将newOutputs序列化并存储到UTXO集合中
			if err != nil {
				log.Panic(err)
			}
		}

		return nil
	})
	if err != nil {
		log.Panic(err)
	}
}
