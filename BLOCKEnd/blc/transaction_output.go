package blc

import (
	"bytes"        // 操作字节的库
	"encoding/gob" // 用于编码和解码复杂数据结构的库
	"log"          // 日志库
)

// TXOutput 表示一个交易输出
type TXOutput struct {
	Value      int    // 交易输出的金额
	PubKeyHash []byte // 公钥哈希
}

// Lock 签名交易输出
func (out *TXOutput) Lock(address []byte) {
	pubKeyHash := Base58Decode(address)            // 解析地址中的公钥哈希
	pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-4] // 截取公钥哈希
	out.PubKeyHash = pubKeyHash
}

// IsLockedWithKey 检查交易输出是否可以被公钥的拥有者使用
func (out *TXOutput) IsLockedWithKey(pubKeyHash []byte) bool {
	return bytes.Compare(out.PubKeyHash, pubKeyHash) == 0 // 比较公钥哈希是否相同
}

// NewTXOutput 创建一个新的交易输出
func NewTXOutput(value int, address string) *TXOutput {
	txo := &TXOutput{value, nil} // 创建一个新的交易输出
	txo.Lock([]byte(address))    // 签名交易输出

	return txo
}

// TXOutputs 收集交易输出
type TXOutputs struct {
	Outputs []TXOutput // 交易输出的集合
}

// Serialize 序列化交易输出
func (outs TXOutputs) Serialize() []byte {
	var buff bytes.Buffer // 创建一个缓冲区

	enc := gob.NewEncoder(&buff) // 创建一个gob编码器
	err := enc.Encode(outs)      // 使用gob编码交易输出
	if err != nil {
		log.Panic(err)
	}

	return buff.Bytes() // 返回编码后的字节数组
}

// DeserializeOutputs 反序列化交易输出
func DeserializeOutputs(data []byte) TXOutputs {
	var outputs TXOutputs // 创建一个交易输出对象

	dec := gob.NewDecoder(bytes.NewReader(data)) // 创建一个gob解码器
	err := dec.Decode(&outputs)                  // 使用gob解码交易输出
	if err != nil {
		log.Panic(err)
	}

	return outputs // 返回解码后的交易输出对象
}
