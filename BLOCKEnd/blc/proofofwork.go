package blc

import (
	"bytes"
	"crypto/sha256" // 导入sha256算法库
	"fmt"
	"math"     // 导入math库
	"math/big" // 导入big库
)

var (
	maxNonce = math.MaxInt64 // 最大的nonce值
)

const targetBits = 24 // 设置难度
// ProofOfWork代表一个工作量证明
type ProofOfWork struct {
	block  *Block   // 工作量证明要对应的区块
	target *big.Int // 目标哈希值
}

// NewProofOfWork创建一个新的工作量证明
func NewProofOfWork(b *Block) *ProofOfWork {
	target := big.NewInt(1)                  // 创建初始值为1的big.Int类型变量
	target.Lsh(target, uint(256-targetBits)) // 将它左移需要检查的位数，并将值赋给目标哈希值

	pow := &ProofOfWork{b, target} // 创建一个工作量证明结构体

	return pow
}

// prepareData为工作量证明准备需要哈希的数据
func (pow *ProofOfWork) prepareData(nonce int) []byte {
	data := bytes.Join( // 将需要哈希的数据合并成一个字节数组
		[][]byte{
			pow.block.PrevBlockHash,       // 前一个区块的哈希值
			pow.block.HashTransactions(),  // 当前区块的交易记录哈希值
			IntToHex(pow.block.Timestamp), // 当前区块创建的时间戳
			IntToHex(int64(targetBits)),   // 目标哈希值
			IntToHex(int64(nonce)),        // nonce值
		},
		[]byte{}, // 使用空字节作为分隔符
	)

	return data
}

// Run进行工作量证明
func (pow *ProofOfWork) Run() (int, []byte) {
	var hashInt big.Int // 定义一个big.Int类型变量，用于存放哈希值
	var hash [32]byte   // 定义一个byte数组，用于存放sha256加密后的哈希值
	nonce := 0          // 定义一个nonce值，用于工作量计算

	fmt.Printf("挖掘新区块")
	for nonce < maxNonce { // 尝试计算nonce值以达到目标哈希值
		data := pow.prepareData(nonce)

		hash = sha256.Sum256(data)                       // 计算哈希值
		if math.Remainder(float64(nonce), 100000) == 0 { // 每隔100000次哈希计算，将当前哈希值输出显示一下
			fmt.Printf("\r%x", hash)
		}
		hashInt.SetBytes(hash[:]) // 将哈希值的字节形式转换为big.Int类型

		if hashInt.Cmp(pow.target) == -1 { // 如果当前的哈希值小于目标哈希值，则证明成功
			break
		} else { // 如果当前的哈希值大于等于目标哈希值，则nonce值加1，继续尝试
			nonce++
		}
	}
	fmt.Print("\n")

	return nonce, hash[:]
}

// Validate验证区块的工作量证明是否有效
func (pow *ProofOfWork) Validate() bool {
	var hashInt big.Int

	data := pow.prepareData(pow.block.Nonce) // 将准备好的数据与当前工作量证明的nonce值合并，然后进行哈希计算
	hash := sha256.Sum256(data)
	hashInt.SetBytes(hash[:])

	isValid := hashInt.Cmp(pow.target) == -1 // 比较计算得到的哈希值与目标哈希值大小，如果计算得到的哈希值小于目标哈希值，则表明工作量证明有效

	return isValid
}
