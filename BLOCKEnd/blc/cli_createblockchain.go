package blc

import (
    "fmt"           // 导入 fmt 包，用于打印输出信息
    "log"           // 导入 log 包，用于记录错误信息
)

func (cli *CLI) createBlockchain(address, nodeID string) {
    // 判断地址是否有效
    if !ValidateAddress(address) {
        log.Panic("错误：地址无效")
    }
    // 创建区块链
    bc := CreateBlockchain(address, nodeID)
    // 在函数结束时关闭数据库
    defer bc.db.Close()

    // 重建 UTXO 集合
    UTXOSet := UTXOSet{bc}
    UTXOSet.Reindex()

    // 打印输出“完成!”信息
    fmt.Println("完成!")
}