package blc

import (
    "fmt"
    "strconv"
)

func (cli *CLI) printChain(nodeID string) { // 打印整个区块链
    bc := NewBlockchain(nodeID) // 根据节点ID创建区块链实例
    defer bc.db.Close() // 关闭数据库连接

    bci := bc.Iterator() // 获取区块链迭代器

    for { // 遍历区块链中的所有区块
        block := bci.Next() // 获取迭代器中的下一块区块

        fmt.Printf("============ Block %x ============\n", block.Hash) // 打印区块信息
        fmt.Printf("Height: %d", block.Height)
        fmt.Printf("Prev. block: %x", block.PrevBlockHash)
        pow := NewProofOfWork(block) // 创建新的工作量证明实例
        fmt.Printf("PoW: %s", strconv.FormatBool(pow.Validate())) // 打印工作量证明验证结果
        for _, tx := range block.Transactions { // 遍历区块中的所有交易
            fmt.Println(tx) // 打印交易信息
        }
        fmt.Printf("\n")

        if len(block.PrevBlockHash) == 0 { // 如果当前区块没有前一区块，则结束遍历
            break
        }
    }
}