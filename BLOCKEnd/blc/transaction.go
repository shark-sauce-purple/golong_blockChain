package blc

import (
	"bytes" // 操作字节的库
	"crypto/ecdsa" // 加密库
	"crypto/elliptic" // 椭圆曲线加密库
	"crypto/rand" // 随机数生成库
	"crypto/sha256" // SHA256哈希算法库
	"math/big" // 大数操作库
	"strings" // 字符串操作库

	"encoding/gob" // 用于编码和解码复杂数据结构的库
	"encoding/hex" // 用于编码和解码十六进制的库
	"fmt" // 格式化库
	"log" // 日志库
)

const subsidy = 10 // 挖矿奖励

// Transaction 表示一个比特币交易
type Transaction struct {
	ID   []byte // 交易ID
	Vin  []TXInput // 交易的输入
	Vout []TXOutput // 交易的输出
}

// IsCoinbase 检查交易是否为coinbase交易
func (tx Transaction) IsCoinbase() bool {
	return len(tx.Vin) == 1 && len(tx.Vin[0].Txid) == 0 && tx.Vin[0].Vout == -1
}

// Serialize 序列化交易
func (tx Transaction) Serialize() []byte {
	var encoded bytes.Buffer // 创建一个缓冲区

	enc := gob.NewEncoder(&encoded) // 创建一个gob编码器
	err := enc.Encode(tx) // 使用gob编码交易
	if err != nil {
		log.Panic(err)
	}

	return encoded.Bytes() // 返回编码后的字节数组
}

// Hash 计算交易的哈希值
func (tx *Transaction) Hash() []byte {
	var hash [32]byte

	txCopy := *tx
	txCopy.ID = []byte{}

	hash = sha256.Sum256(txCopy.Serialize())

	return hash[:]
}

// Sign 签名交易的每个输入
func (tx *Transaction) Sign(privKey ecdsa.PrivateKey, prevTXs map[string]Transaction) {
	if tx.IsCoinbase() {
		return
	}

	for _, vin := range tx.Vin {
		if prevTXs[hex.EncodeToString(vin.Txid)].ID == nil {
			log.Panic("错误：以前的事务不正确")
		}
	}

	txCopy := tx.TrimmedCopy()

	for inID, vin := range txCopy.Vin {
		prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
		txCopy.Vin[inID].Signature = nil
		txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash

		dataToSign := fmt.Sprintf("%x\n", txCopy)

		r, s, err := ecdsa.Sign(rand.Reader, &privKey, []byte(dataToSign))
		if err != nil {
			log.Panic(err)
		}
		signature := append(r.Bytes(), s.Bytes()...)

		tx.Vin[inID].Signature = signature
		txCopy.Vin[inID].PubKey = nil
	}
}

// String 返回交易的人类可读表示
func (tx Transaction) String() string {
	var lines []string

	lines = append(lines, fmt.Sprintf("--- Transaction %x:", tx.ID))

	for i, input := range tx.Vin {

		lines = append(lines, fmt.Sprintf("Input %d:", i))
		lines = append(lines, fmt.Sprintf("TXID: %x", input.Txid))
		lines = append(lines, fmt.Sprintf("Out:  %d", input.Vout))
		lines = append(lines, fmt.Sprintf("Signature: %x", input.Signature))
		lines = append(lines, fmt.Sprintf("PubKey:%x", input.PubKey))
	}

	for i, output := range tx.Vout {
		lines = append(lines, fmt.Sprintf("Output %d:", i))
		lines = append(lines, fmt.Sprintf(" Value:  %d", output.Value))
		lines = append(lines, fmt.Sprintf(" Script: %x", output.PubKeyHash))
	}

	return strings.Join(lines, "\n")
}

// TrimmedCopy 创建一个交易的拷贝用于签名
func (tx *Transaction) TrimmedCopy() Transaction {
	var inputs []TXInput
	var outputs []TXOutput

	for _, vin := range tx.Vin {
		inputs = append(inputs, TXInput{vin.Txid, vin.Vout, nil, nil}) // 创建一个新的输入
	}

	for _, vout := range tx.Vout {
		outputs = append(outputs, TXOutput{vout.Value, vout.PubKeyHash}) // 创建一个新的输出
	}

	txCopy := Transaction{tx.ID, inputs, outputs}

	return txCopy
}

// Verify 验证交易输入的签名是否正确
func (tx *Transaction) Verify(prevTXs map[string]Transaction) bool {
	if tx.IsCoinbase() {
		return true
	}

	for _, vin := range tx.Vin {
		if prevTXs[hex.EncodeToString(vin.Txid)].ID == nil {
			log.Panic("错误：以前的事务不正确")
		}
	}

	txCopy := tx.TrimmedCopy()

	curve := elliptic.P256()

	for inID, vin := range tx.Vin {
		prevTx := prevTXs[hex.EncodeToString(vin.Txid)]
		txCopy.Vin[inID].Signature = nil
		txCopy.Vin[inID].PubKey = prevTx.Vout[vin.Vout].PubKeyHash

		r := big.Int{}
		s := big.Int{}
		sigLen := len(vin.Signature)
		r.SetBytes(vin.Signature[:(sigLen / 2)])
		s.SetBytes(vin.Signature[(sigLen / 2):])

		x := big.Int{}
		y := big.Int{}
		keyLen := len(vin.PubKey)
		x.SetBytes(vin.PubKey[:(keyLen / 2)])
		y.SetBytes(vin.PubKey[(keyLen / 2):])

		dataToVerify := fmt.Sprintf("%x\n", txCopy)

		rawPubKey := ecdsa.PublicKey{Curve: curve, X: &x, Y: &y}
		if ecdsa.Verify(&rawPubKey, []byte(dataToVerify), &r, &s) == false {
			return false
		}
		txCopy.Vin[inID].PubKey = nil
	}

	return true
}


// NewCoinbaseTX 创建一个新的coinbase交易，to为收款地址，data为交易数据
func NewCoinbaseTX(to, data string) *Transaction {
if data == "" { //如果没有数据，则生成一个随机的20字节数据
randData := make([]byte, 20)
_, err := rand.Read(randData)
if err != nil {
log.Panic(err)
}

	data = fmt.Sprintf("%x", randData) //将随机数据格式化为16进制字符串
}

txin := TXInput{[]byte{}, -1, nil, []byte(data)} //coinbase交易的输入为一个空的交易ID和-1的输出索引，签名和公钥为空，数据为随机数据
txout := NewTXOutput(subsidy, to) //coinbase交易的输出为一个指定数量的coinbase和指定收款地址
tx := Transaction{nil, []TXInput{txin}, []TXOutput{*txout}} //构造一个交易
tx.ID = tx.Hash() //计算并设置交易ID

return &tx
}

// NewUTXOTransaction 创建一个新的UTXO（未花费的交易输出）交易，给定一个钱包、收款地址、金额和UTXO集合
func NewUTXOTransaction(wallet *Wallet, to string, amount int, UTXOSet *UTXOSet) *Transaction {
var inputs []TXInput
var outputs []TXOutput


pubKeyHash := HashPubKey(wallet.PublicKey) //计算钱包公钥的哈希值
acc, validOutputs := UTXOSet.FindSpendableOutputs(pubKeyHash, amount) //在UTXO集合中找到足够的未花费的输出，返回总金额和可用输出的映射

if acc < amount { //如果总金额小于所需金额，则抛出错误
	log.Panic("错误：资金不足")
}

// Build a list of inputs 构建输入列表
for txid, outs := range validOutputs { //遍历可用输出的映射
	txID, err := hex.DecodeString(txid) //将交易ID解码为字节数组
	if err != nil {
		log.Panic(err)
	}

	for _, out := range outs { //遍历可用的交易输出
		input := TXInput{txID, out, nil, wallet.PublicKey} //构建一个新的交易输入，包含交易ID、输出索引、签名为空、公钥为钱包公钥
		inputs = append(inputs, input) //将新的交易输入添加到输入列表中
	}
}

// Build a list of outputs 构建输出列表
from := fmt.Sprintf("%s", wallet.GetAddress()) //获取钱包地址
outputs = append(outputs, *NewTXOutput(amount, to)) //添加一个新的交易输出，包含指定的金额和收款地址
if acc > amount {
	outputs = append(outputs, *NewTXOutput(acc-amount, from)) //如果总金额大于所需金额，则添加一个新的交易输出，包含找零
}

tx := Transaction{nil, inputs, outputs} //构造一个新的交易
tx.ID = tx.Hash() //计算并设置交易ID
UTXOSet.Blockchain.SignTransaction(&tx, wallet.PrivateKey) //使用钱包私钥对交易进行签名

return &tx
}

// DeserializeTransaction 反序列化交易数据
func DeserializeTransaction(data []byte) Transaction {
var transaction Transaction


decoder := gob.NewDecoder(bytes.NewReader(data)) //创建一个新的gob解码器，读取交易数据
err := decoder.Decode(&transaction) //解码交易数据到交易结构体中
if err != nil {
	log.Panic(err)
}

return transaction //返回解码后的交易结构体
}