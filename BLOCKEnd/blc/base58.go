package blc

import (
   "bytes" //引入bytes包，用于处理字节切片
   "math/big" //引入math/big包，用于处理大整数
)

var b58Alphabet = []byte("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz") //定义Base58编码使用的字符集

//字节数组转base58（加密）
func Base58Encode(input []byte) []byte {
   var result []byte //定义结果切片

   x := big.NewInt(0).SetBytes(input) //将输入转换为大整数类型

   base := big.NewInt(int64(len(b58Alphabet))) //定义编码字符集的长度为base
   zero := big.NewInt(0) //定义0
   mod := &big.Int{} //定义余数

   for x.Cmp(zero) != 0 { //不断对x进行除以base的操作，直到商为0
     x.DivMod(x, base, mod) //将x除以base并得到商和余数
     result = append(result, b58Alphabet[mod.Int64()]) //将对应余数对应位置的字符加入结果切片
   }

   if input[0] == 0x00 { //如果原字节数组的第一个字节是0x00，即长度为1，则将编码字符集的第一个字符加到结果切片
     result = append(result, b58Alphabet[0])
   }

   ReverseBytes(result) //调用ReverseBytes函数将结果切片翻转

   return result //返回翻转后的结果切片
}

//Base58解码（解密）
func Base58Decode(input []byte) []byte {
   result := big.NewInt(0) //定义一个大整数类型的变量result并初始化为0

   for _, b := range input { //遍历input字节数组的每个元素
     charIndex := bytes.IndexByte(b58Alphabet, b) //获取字符b在编码字符集中对应位置的索引
     result.Mul(result, big.NewInt(58)) //将当前result乘以58
     result.Add(result, big.NewInt(int64(charIndex))) //将编码字符集中对应的数值加到result上
   }

   decoded := result.Bytes() //将大整数类型的result转换为字节数组类型的decoded

   if input[0] == b58Alphabet[0] { //如果input字节数组的第一个元素是编码字符集中的第一个字符，则将一个长度为1且值为0的字节加到decoded的开头
     decoded = append([]byte{0x00}, decoded...)
   }

    return decoded //返回解码后的字节数组
}