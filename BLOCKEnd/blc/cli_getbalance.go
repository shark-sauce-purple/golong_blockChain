package blc

import (
    "fmt"
    "log"
)

// getBalance获取给定地址的余额
func (cli *CLI) getBalance(address, nodeID string) {
    if !ValidateAddress(address) { // 验证地址是否有效
        log.Panic("错误：地址无效")
    }
    bc := NewBlockchain(nodeID) // 创建一个新的区块链
    UTXOSet := UTXOSet{bc} // 创建UTXO集
    defer bc.db.Close() // 确保区块链数据库被关闭

    balance := 0 // 初始化余额为0
    pubKeyHash := Base58Decode([]byte(address)) // 使用Base58编码将地址转换为公钥哈希
    pubKeyHash = pubKeyHash[1 : len(pubKeyHash)-4] // 取出公钥哈希并去除校验和

    // 找到属于该地址的未花费输出
    UTXOs := UTXOSet.FindUTXO(pubKeyHash)

    // 循环未花费输出作为输入，并将它们的值相加得到余额
    for _, out := range UTXOs {
        balance += out.Value
    }

    fmt.Printf("余额 '%s': %d", address, balance) // 打印余额
}
