package blc

import "fmt"

// createWallet创建一个新的钱包地址
func (cli *CLI) createWallet(nodeID string) {
    wallets, _ := NewWallets(nodeID) // 创建新的钱包
    address := wallets.CreateWallet() // 创建新的钱包地址
    wallets.SaveToFile(nodeID) // 将钱包信息保存到文件中

    fmt.Printf("您的新地址: %s", address) // 打印新地址
}