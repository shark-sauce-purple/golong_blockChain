package blc

import (
	"flag"
	"fmt"
	"log"

	"os"
)

// CLI responsible for processing command line arguments
type CLI struct{}

func (cli *CLI) printUsage() { // 打印使用说明
	fmt.Println("Usage:")
	fmt.Println("  createblockchain -address ADDRESS - 创建一个区块链")
	fmt.Println("  createwallet - 生成一个新的密钥对并将其保存到钱包文件中。")
	fmt.Println("  getbalance -address ADDRESS - 获取余额")
	fmt.Println("  listaddresses - 打印出所有的钱包")
	fmt.Println("  printchain - 打印所有的区块")
	fmt.Println("  reindexutxo - 重新建立UTXO组")
	fmt.Println("  send -from FROM -to TO -amount AMOUNT -mine -将amount数量从 FROM 地址发送到 TO。在设置-mine时在同一节点上挖矿。")
	fmt.Println("  startnode -miner ADDRESS -使用node_ID环境中指定的ID启动节点。 -miner 挖矿的矿工号")
}

func (cli *CLI) validateArgs() { // 验证命令行参数是否存在
	if len(os.Args) < 2 {
		cli.printUsage()
		os.Exit(1)
	}
}

// Run parses command line arguments and processes commands
func (cli *CLI) Run() { // 处理命令行参数并执行相应命令
	cli.validateArgs()

	nodeID := os.Getenv("NODE_ID") // 获取节点ID环境变量
	if nodeID == "" {
		fmt.Printf("NODE_ID env. var is not set!") // 如果未设置节点ID环境变量，则打印错误信息并退出程序
		os.Exit(1)
	}

	getBalanceCmd := flag.NewFlagSet("getbalance", flag.ExitOnError)             // 创建获取余额命令
	createBlockchainCmd := flag.NewFlagSet("createblockchain", flag.ExitOnError) // 创建区块链命令
	createWalletCmd := flag.NewFlagSet("createwallet", flag.ExitOnError)         // 创建钱包命令
	listAddressesCmd := flag.NewFlagSet("listaddresses", flag.ExitOnError)       // 列出所有钱包命令
	printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)             // 打印区块链命令
	reindexUTXOCmd := flag.NewFlagSet("reindexutxo", flag.ExitOnError)           // 重新索引UTXO集合命令
	sendCmd := flag.NewFlagSet("send", flag.ExitOnError)                         // 发送交易命令
	startNodeCmd := flag.NewFlagSet("startnode", flag.ExitOnError)               // 启动节点命令

	getBalanceAddress := getBalanceCmd.String("address", "", "要获取余额的地址")
	createBlockchainAddress := createBlockchainCmd.String("address", "", "发送创世区块奖励的地址")
	sendFrom := sendCmd.String("from", "", "源钱包地址")
	sendTo := sendCmd.String("to", "", "目标钱包地址")
	sendAmount := sendCmd.Int("amount", 0, "汇款金额")
	sendMine := sendCmd.Bool("mine", false, "立即在同一节点上挖矿") // 解析各个命令对应的参数
	startNodeMiner := startNodeCmd.String("miner", "", "启用挖矿模式并将奖励发送到地址")

	// 根据命令行参数执行不同的命令
	switch os.Args[1] {
	case "getbalance":
		// 解析 getbalance 命令行参数
		err := getBalanceCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "createblockchain":
		// 解析 createblockchain 命令行参数
		err := createBlockchainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "createwallet":
		// 解析 createwallet 命令行参数
		err := createWalletCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "listaddresses":
		// 解析 listaddresses 命令行参数
		err := listAddressesCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "printchain":
		// 解析 printchain 命令行参数
		err := printChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "reindexutxo":
		// 解析 reindexutxo 命令行参数
		err := reindexUTXOCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "send":
		// 解析 send 命令行参数
		err := sendCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "startnode":
		// 解析 startnode 命令行参数
		err := startNodeCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	default:
		// 执行默认命令
		cli.printUsage()
		os.Exit(1)
	}

	// 根据解析后的参数执行相应的命令
	if getBalanceCmd.Parsed() {
		// 如果执行 getbalance 命令，则获取余额
		if *getBalanceAddress == "" {
			// 如果未提供获取余额的地址，则打印用法并退出
			getBalanceCmd.Usage()
			os.Exit(1)
		}
		cli.getBalance(*getBalanceAddress, nodeID)
	}

	if createBlockchainCmd.Parsed() {
		// 如果执行 createblockchain 命令，则创建新的区块链
		if *createBlockchainAddress == "" {
			// 如果未提供要创建的区块链地址，则打印用法并退出
			createBlockchainCmd.Usage()
			os.Exit(1)
		}
		cli.createBlockchain(*createBlockchainAddress, nodeID)
	}

	if createWalletCmd.Parsed() {
		// 如果执行 createwallet 命令，则创建新的钱包
		cli.createWallet(nodeID)
	}

	if listAddressesCmd.Parsed() {
		// 如果执行 listaddresses 命令，则列出所有钱包地址
		cli.listAddresses(nodeID)
	}

	if printChainCmd.Parsed() {
		// 如果执行 printchain 命令，则打印完整的区块链
		cli.printChain(nodeID)
	}

	if reindexUTXOCmd.Parsed() {
		// 如果执行 reindexutxo 命令，则重新索引未花费交易输出
		cli.reindexUTXO(nodeID)
	}

	if sendCmd.Parsed() {
		// 如果执行 send 命令，则发送交易
		if *sendFrom == "" || *sendTo == "" || *sendAmount <= 0 {
			// 如果未提供必要的交易信息，则打印用法并退出
			sendCmd.Usage()
			os.Exit(1)
		}
		cli.send(*sendFrom, *sendTo, *sendAmount, nodeID, *sendMine)
	}

	if startNodeCmd.Parsed() {
		// 如果执行 startnode 命令，则启动节点
		nodeID := os.Getenv("NODE_ID")
		if nodeID == "" {
			// 如果未设置启动的节点 ID，则打印用法并退出
			startNodeCmd.Usage()
			os.Exit(1)
		}
		cli.startNode(nodeID, *startNodeMiner)
	}
}
