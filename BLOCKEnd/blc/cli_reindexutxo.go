package blc

import "fmt"

func (cli *CLI) reindexUTXO(nodeID string) { // 重新索引UTXO集合
    bc := NewBlockchain(nodeID) // 根据节点ID创建区块链实例
    UTXOSet := UTXOSet{bc} // 创建UTXO集合实例
    UTXOSet.Reindex() // 重新索引UTXO集合

    count := UTXOSet.CountTransactions() // 统计UTXO集合中的事务数
    fmt.Printf("完成! UTXO 集中有 %d 个事务。\n", count) // 打印完成信息
}