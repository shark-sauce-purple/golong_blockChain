package blc
import (
    "crypto/sha256" // 导入sha256算法库
)

// MerkleTree代表一个Merkle树
type MerkleTree struct {
    RootNode *MerkleNode // 根节点
}

// MerkleNode代表一个Merkle树的节点
type MerkleNode struct {
    Left  *MerkleNode // 左子节点
    Right *MerkleNode // 右子节点
    Data  []byte // 数据
}

// NewMerkleTree创建一个由数据序列构建的Merkle树
func NewMerkleTree(data [][]byte) *MerkleTree {
    var nodes []MerkleNode // 节点数组

    if len(data)%2 != 0 { // 如果数据数量是奇数，则复制最后一个数据以保证偶数
        data = append(data, data[len(data)-1])
    }

    for _, datum := range data { // 将数据转换为节点，存储进节点数组中
        node := NewMerkleNode(nil, nil, datum)
        nodes = append(nodes, *node)
    }

    for i := 0; i < len(data)/2; i++ { // 树的构建，从下往上逐层构建
        var newLevel []MerkleNode

        for j := 0; j < len(nodes); j += 2 { // 每两个节点构建一个新节点
            node := NewMerkleNode(&nodes[j], &nodes[j+1], nil)
            newLevel = append(newLevel, *node)
        }

        nodes = newLevel // 将新一层的节点赋给原先节点数组以供下一次循环使用
    }

    mTree := MerkleTree{&nodes[0]} // 构建完毕后，将根节点存储进MerkleTree结构体中

    return &mTree
}

// NewMerkleNode创建一个新的Merkle树节点
func NewMerkleNode(left, right *MerkleNode, data []byte) *MerkleNode {
    mNode := MerkleNode{} // 新建一个空的MerkleNode结构体

    if left == nil && right == nil { // 如果节点为叶节点，则计算它的数据的哈希值并赋给Data
        hash := sha256.Sum256(data)
        mNode.Data = hash[:]
    } else { // 如果节点不是叶节点，则计算它的左右儿子节点的Data值并拼接在一起，再求它们的哈希值并赋给Data
        prevHashes := append(left.Data, right.Data...)
        hash := sha256.Sum256(prevHashes)
        mNode.Data = hash[:]
    }

    mNode.Left = left // 指定节点的左右儿子节点
    mNode.Right = right

    return &mNode // 返回这个节点
}