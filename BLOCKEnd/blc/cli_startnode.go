package blc

import (
    "fmt"
    "log"
)

func (cli *CLI) startNode(nodeID, minerAddress string) { // 启动节点
    fmt.Printf("起始节点 %s", nodeID) // 打印节点ID
    if len(minerAddress) > 0 { // 如果有指定矿工地址
        if ValidateAddress(minerAddress) { // 验证矿工地址是否有效
            fmt.Println("挖矿正在进行中。领取奖励的地址：", minerAddress) // 打印正在挖矿信息
        } else {
            log.Panic("错误的矿工地址！") // 打印错误信息并终止程序
        }
    }
    StartServer(nodeID, minerAddress) // 启动节点服务器
}