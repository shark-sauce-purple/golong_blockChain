package blc

import (
    "log" //引入log包，用于实现日志输出

    "github.com/boltdb/bolt" //引入boltdb/bolt包，用于实现区块链数据的持久化存储
)

//定义一个迭代器结构体，用于遍历区块链中的所有区块
type BlockchainIterator struct {
    currentHash []byte //当前迭代器所指向的区块的Hash值
    db          *bolt.DB //区块链数据库对象
}

// Next返回链中下一个块
func (i *BlockchainIterator) Next() *Block {
    var block *Block

    err := i.db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(blocksBucket)) //获取区块存储桶
        encodedBlock := b.Get(i.currentHash) //获取当前所指向的区块的序列化数据
        block = DeserializeBlock(encodedBlock) //将序列化数据反序列化为区块

        return nil
    })

    if err != nil {
        log.Panic(err)
    }

    i.currentHash = block.PrevBlockHash //将当前迭代器的Hash值更新为所执行区块的前一块区块的Hash值

    return block //返回查询到的区块数据
}