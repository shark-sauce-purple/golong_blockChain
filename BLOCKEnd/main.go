package main //声明包名
import (
    blc "Go/golong_blockChain/BLOCK20/BLC" //导入自定义包
)
func main() {
    cli := blc.CLI{} //创建CLI结构体实例
    cli.Run() //调用CLI结构体中的Run方法启动命令行界面

}
