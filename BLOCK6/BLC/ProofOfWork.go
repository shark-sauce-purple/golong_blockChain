package blc

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"math/big"
)

//至少有多少个0
const targetBit = 5

type ProofOfWork struct {
	//当前要验证的存储
	Block *Block
	//大数据存储
	Target *big.Int
}

func (pow *ProofOfWork) prepareData(nonce int64) []byte {
	data := bytes.Join(
		[][]byte{
			pow.Block.PrevBlockHash,
			pow.Block.Data,
			IntToHex(pow.Block.Timestamp),
			IntToHex(int64(targetBit)),
			IntToHex(int64(nonce)),
			IntToHex(int64(pow.Block.Height)),
		},
		[]byte{},
	)

	return data
}

//判断hash是否有效
func (ProofOfWork *ProofOfWork) Validate() bool {
	var hashInt big.Int
	hashInt.SetBytes(ProofOfWork.Block.Hash)
	//判断hash有效性，如果满足条件，跳出循环（小于target）
		// 	-1 if x <  y
		// 	0 if x == y
		//   +1 if x >  y
	return  ProofOfWork.Target.Cmp(&hashInt) == 1
}

//挖矿
func (ProofOfWork *ProofOfWork) Run() ([]byte, int64) { //多值返回,返回hash值和nonce
	var hashInt big.Int //存储新生产的hash
	var hash [32]byte
	nonce := 0
	for {
		//1.将block的属性拼接成字节
		dataBytes := ProofOfWork.prepareData(int64(nonce))
		//2.生成hash
		hash = sha256.Sum256(dataBytes)
		fmt.Printf("\r%x",hash)
		//存储到hashInt
		hashInt.SetBytes(hash[:])
		//3.判断hash有效性，如果满足条件，跳出循环（小于target）
		// 	-1 if x <  y
		// 	0 if x == y
		//   +1 if x >  y
		if ProofOfWork.Target.Cmp(&hashInt) == 1 {
			break	
		}
		nonce = nonce + 1
	}

	return hash[:],int64(nonce)
}

//创建新的工作量证明对象
func NewProofOfWork(block *Block) *ProofOfWork {

	//初始化target
	target := big.NewInt(1)

	//移位256-targetBit
	target = target.Lsh(target, 256-targetBit)

	return &ProofOfWork{block, target}
}

