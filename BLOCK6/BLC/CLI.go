package blc

import (
	"flag"
	"fmt"
	"log"
	"os"
)

type CLI struct {
	// Blockchain *Blockchain
}

//输出使用说明
func printUsage() {
	fmt.Println("使用说明")
	fmt.Println("\tcreateblockchain -data DATA--交易数据")
	fmt.Println("\taddblock -data DATA--交易数据")
	fmt.Println("\tprintchain --输出数据")

}

//判断args，防止error
func isValidArgs() {
	//如果终端数据<2
	if len(os.Args) < 2 {
		printUsage()
		os.Exit(0)
	}

}

//增加区块
func (cli *CLI) addBlock(data string) {
	if dbExists()==false{
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	//调用函数
	blockchain.AddBlockToBlockchain(data)
	
}

//输出区块链数据
func (cli *CLI) printChain() {
	if dbExists()==false{
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	//调用函数
	blockchain.PrintChain()
	
}

//创世区块
func (cli *CLI) creatBlockChain(data string) {
	CreateGenesisBlockchain(data)

}

func (cli *CLI) Run() {
	isValidArgs()

	addBlockCmd := flag.NewFlagSet("addblock", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)
	creatBlockChainCmd := flag.NewFlagSet("createblockchain", flag.ExitOnError)
	flagAddBlockData := addBlockCmd.String("data", "添加数据", "交易数据")                      //增加数据
	flagCreateBlockchainData := creatBlockChainCmd.String("data", "创世区块数据", "创世区块交易数据") //第二个为默认值

	//解析数据
	switch os.Args[1] {
	case "addblock":
		//解析后续内容
		err := addBlockCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "printchain":
		//解析后续内容
		err := printChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "createblockchain":
		err := creatBlockChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	default:
		printUsage()
		os.Exit(0)

	}
	//增加
	if addBlockCmd.Parsed() { //解析add数据
		if *flagAddBlockData == "" { //如果没有data
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		cli.addBlock(*flagAddBlockData)
	}
	//输出
	if printChainCmd.Parsed() {
		//fmt.Println("输出所有区块数据")
		cli.printChain()
	}

	//创世区块创建
	if creatBlockChainCmd.Parsed() {
		if *flagCreateBlockchainData == "" { //如果没有data
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		cli.creatBlockChain(*flagCreateBlockchainData)

	}
}
