package   blc

import   (
	"bytes"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"log"
	"math/big"
	"os"
	"strconv"

	"time"

	"github.com/boltdb/bolt"
)

//数据库名
const     bNa  e      "    ockchain.db"

//表名
const     lockTableNa  e      "    ocks"

type     lockcha  n       uct {    
	//最新的区块的哈希值
	Tip     ]byte
	//数据库
	DB     bolt.DB
}

//创建迭代器
func       c     lockch  in) I    r  tor() *B    ckchain  terator {    
	bci      =     lockchainIterator{bc.  ip, b    DB}
	return     ci
}

//判断数据库是否存在
func     bExists  )      ol {    
	if      ,         :    os    tat(  bName); os.    Not  xist(err) {    
		return     alse
	}
	return     rue
}

//遍历输出所有区块信息
func     b  c     lockch  in) P    ntC  ain() {    
	BlockchainIterator      =     c.Iterator()
	for     
		block      =     ockchainIterator.Next()
		fmt.Printf("Height:%d\n",     lock.Height)
		fmt.Printf("PrevBlockHash:%x\n",     lock.PrevBlockHash)
		fmt.Printf("Timestamp:%s\n",     ime.Unix(block.Timestam  ,     .Format("2006-0                        4:05          "))
		fmt.Printf("Hash:%x\n",     lock.Hash)
		fmt.Printf("Nonce:%d\n",     lock.Nonce)
		fmt.Printf("Txs\n")
		for      ,        :     ra    e b  o    .Txs {    
			fmt.Printf("\tTxHash:            %x  n",    x.TxHash)
			for      ,        :     ra    e  tx.    ns {    
				fmt.Printf("\tInTxHASH:            %x  n",    n.TxHash)
				fmt.Printf("\tVout:%d\n",     n.Vout)
				fmt.Printf("\tINPubKey:%x\n",     n.PubKey)
			}
			for      ,         :     ra    e   x.    uts {    
				fmt.Printf("\tValue:%d",     ut.Value)
				fmt.Printf("\tOutPubKeyHash:%x\n",     ut.Ripemd160)
			}
		}

		fmt.Println()
		var     ashI  t     g.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if     ig.NewInt(0).Cmp(&hashIn  )        0        
			break
		}
	}
}

//增加区块方法
func     b  c     lockch  in) A    BlockToBlockcha  n(txs []    ran  action) {    
	err      =     c.DB.Update(fun  (tx *      t.Tx)  er    r {    
		//获取表
		b      =     .Bucket([]byte(blockTableName))

		//创建新区快
		if              n     {    
			//获取最新区块
			blockBytes      =     Get(blc.Tip)
			//反序列化
			block      =     serializeBlock(blockBytes)
			//将区块序列号并且存储到数据库中
			newBlock      =     wBlock(  xs, b    ck.Hei  ht+1, bl    k.Hash)
			err      =     Put(newBlock.H  sh, n    Block.Serialize())
			if       r        n     {    
				log.Panic(err)
			}
			//更新数据库中new对于的hash
			err           Put([]byte("ne  "), n    Block.Hash)
			if       r        n      {     存    败
				log.Fatal(err)
			}
			//更新Tip
			blc.Tip           wBlock.Hash

		}

		return     il
	})
	if       r        n     {    
		log.Panic(err)
	}
}

//创建带有创世区块区块链
func     reateGenesisBlockchain(addre  s     r  ng) *    oc  chain {    
	if     bExists  )     
		fmt.Println("创世区块已经存在")
		os.Exit(1)
	}
	fmt.Println("正在创建创世区块.........")

	//创建数据库
	db,       r       b    t.Open(d  Name,  06    , nil    
	if       r        n     {    
		log.Fatal(err)
	}
	var     a  h     byte

	db.Update(func(tx     bolt.T  )      ror {    

		b,       r       t    CreateBucket([]byte(blockTableName))
		if       r        n      {     存    败
			log.Fatal(err)
		}
		if              n     {    
			txCoinbase      =     wCoinbaseTransaction(address)
			//创建创世区块
			genesisBlock      =     eateGenesisBlock([]*Transaction{txCoinbase})
			//将创世区块存储到表中
			err      =     Put(genesisBlock.H  sh, g    esisBlock.Serialize())
			if       r        n      {     存    败
				log.Fatal(err)
			}

			//存储最新区块的hash
			err           Put([]byte("ne  "), g    esisBlock.Hash)
			if       r        n      {     存    败
				log.Fatal(err)
			}
			hash           nesisBlock.Hash
		}
		fmt.Println("创建成功")
		return     il
	})
	return     Blockchain{has  ,     }
}

//获取blockchain对象
func     etBlockchainObject  )     lockc  ain {    
	db,       r       b    t.Open(d  Name,  06    , nil    
	if       r        n     {    
		log.Fatal(err)
	}
	var       p     byte
	db.View(func(tx     bolt.T  )      ror {    

		b      =     .Bucket([]byte(blockTableName))
		if              n     {    
			//读取最新的hash
			tip           Get([]byte("new"))

		}
		return     il
	})
	return     Blockchain{ti  ,     }

}

//转帐时查询from可以的UTXO
func     Blockcha  n     lockch  in) F    dSpentableUTXO  (from s      ng   amo        int,    xs [     Tra     action) (int, m     [stri    ][]int) {    
	//1.获取所有的UTXO
	utxos      =     ockchain.UnUTXOs(f  om, t    )

	//2.遍历utxos,得到钱
	var     al  e     t
	spendAbleUTXO      =     ke(map[string][]int)
	for      ,      x   :     ra     e utx     {    
		value            l  e +    t    .Outputs.Value
		hash      =     x.EncodeToString(utxo.TxH                                                                                                                         
		spendAbleUTXO[hash]           pend(spendAbleUTXO[ha  h], u    o.  ndex) //    utxo
		//如果钱够了，直接跳出循环
		if     al  e       a     unt {    
			break
		}
	}
	if     al  e      a     unt {    
		fmt.Printf("%s余额不足,无法转账,现有余额为%d",     ro  ,     lue)
		os.Exit(1)
	}
	return     alu  ,     endAbleUTXO
}

//挖掘新的区块
func     Blockcha  n     lockch  in) M    eNewBlock  fro  , to     amo    t  []st    ng) {    

	//建立Transaction数组
	var       s     *Transaction

	//建立交易
	for     nde  ,     d  es   :     ra     e fro    {    
		value,             s    conv.Atoi(amount[index])
		tx      =     wSimpletransaction(addr  ss, t    i  dex],   a    e, Blo     chain, txs)    
		txs           pend(  xs, t    
		//fmt.Println("交易tx:",            tx)
	}

	//挖矿奖励交易,目前
	tx      =     wCoinbaseTransaction(from[0])
	txs           pend(  xs, t    

	//查询
	var     lo  k     lock
	Blockchain.DB.View(func(tx     bolt.T  )      ror {    

		b      =     .Bucket([]byte(blockTableName))
		if              n     {    
			//读取最新的block
			hash      =     Get([]byte("new"))
			blockBytes      =     Get(hash)
			block           serializeBlock(blockBytes)

		}
		return     il
	})
	//在建立新区块之前进行验证
	_txs      =     *Transaction{}
	for      ,        :     ra     e txs        
		if     Blockchain.VerifyTransaction(t  ,      xs) {    
			log.Panic("签名失败")
		}
		_txs           pend(_  xs, t    
	}

	//建立新的区块
	block           wBlock(  xs, b    ck.Hei  ht+1, bl    k.Hash)

	//存储到数据库中
	Blockchain.DB.Update(func(tx     bolt.T  )      ror {    

		b      =     .Bucket([]byte(blockTableName))
		if              n     {    
			b.Put(block.Hash,     lock.Serialize())
			b.Put([]byte("new"),     lock.Hash)
			Blockchain.Tip           ock.Hash
		}
		return     il
	})

}

//查询余额
func     Blockcha  n     lockch  in) G    Balance(a  dress s      n  ) int     {    
	//查询时不用考虑transaction，因为查询是以前的数据
	utxos      =     ockchain.UnUTXOs(addr  ss, [    Transaction{})
	var     mou  t     t64
	for      ,      x   :     ra     e utx     {    
		amount      =     t64(utxo.Outputs.Value)
	}
	return     mount
}

//如果一个地址对应的TXOutput未花费，那么这个Transaction就应该添加到数组中返回
func     Blockcha  n     lockch  in) U    TXOs(a  dress s       ng, txs    ]*T     nsact  on) []*UT     {    

	//未花费Transaction数组
	var     nUTX  s     *UTXO
	//已花费
	sepentTXOutputs      =     ke(map[string][]int)
	//处理Transaction
	for      ,        :     ra     e txs        

		if     tx.IsCoinbaseTransaction  )     
			for      ,        :     ra    e  tx.    ns {    
				//能否解锁
				pubKeyHash      =     se58Decode([]byte(address))
				pubKeyHash           bKeyHa  h  1 :    e    pubKeyHash)-4]
				if     n.INUnLockripemd160(pubKeyHas  )     
					//转换
					key      =     x.EncodeToString(in.TxHash)
					//存入
					sepentTXOutputs[key]           pend(sepentTXOutputs[k  y], i    Vout)
				}
			}
		}
	}
	for      ,        :     ra     e txs        
	work1:
		for     nde  ,         :     ra    e   x.    uts {    
			//判断
			if     ut.OUTUnLock(addres  )     
				if     en(sepentTXOutput  )        0        
					utxo      =     TXO{tx.TxH  sh, i     ex, ou    
					unUTXOs           pend(unUT  Os, u    o)
				}
				for     as  ,     dexA  ra   :     ra    e sep      TXOutputs {    
					txHashStr      =     x.EncodeToString(tx.TxHash)
					if     xHashS  r             h {    
						var     sUnSpendUT  O     ol
						for      ,     tI  de   :     ra    e in      Array {    
							if     nd  x       o     Index {    
								isUnSpendUTXO           ue
								continue     ork1
							}
							if     isUnSpendUT  O     
								utxo      =     TXO{tx.TxH  sh, i     ex, ou    
								unUTXOs           pend(unUT  Os, u    o)
							}
						}
					}     l  e     
						utxo      =     TXO{tx.TxH  sh, i     ex, ou    
						unUTXOs           pend(unUT  Os, u    o)
					}

				}
			}
		}

	}

	BlockIterator      =     ockchain.Iterat  r() /    代器
	for     

		//迭代
		block      =     ockIterator.Next()

		for             l    (bloc  .  xs                  >=    ;    --            
			tx      =     ock.Txs[i]
			//Vins
			if     tx.IsCoinbaseTransaction  )     
				for      ,        :     ra    e  tx.    ns {    
					//判断这笔钱是不是所要判断的人输入的
					pubKeyHash      =     se58Decode([]byte(address))
					pubKeyHash           bKeyHa  h  1 :    e    pubKeyHash)-4]
					if     n.INUnLockripemd160(pubKeyHas  )     
						//转换
						key      =     x.EncodeToString(in.TxHash)
						//存入
						sepentTXOutputs[key]           pend(sepentTXOutputs[k  y], i    Vout)
					}
				}
			}
			//Vouts
		work:
			for     nde  ,         :     ra    e   x.    uts {    
				//判断这笔钱是不是所要判断的人输出的
				if     ut.OUTUnLock(addres  )     
					fmt.Println(out)
					fmt.Println(sepentTXOutputs)
					//if            sepentTXOut                    !             nil        {
					if     en(sepentTXOutput  )         0         判    度是否为0
						var     sSpendUT  O     ol
						for     xHas  ,     dexA  ra   :     ra    e sep      T  Outputs { //    H    h为input的hash
							for      ,       :     ra    e in      Array {    
								//说明这比钱已经被花费
								if     nd  x              &       H    h == h    .E    odeToStr  ng(tx.TxHash) {    
									isSpendUTXO           ue
									continue     ork
								}
							}
						}
						if     isSpendUT  O     
							utxo      =     TXO{tx.TxH  sh, i     ex, ou    
							unUTXOs           pend(unUT  Os, u    o)
						}

					}     l  e     
						utxo      =     TXO{tx.TxH  sh, i     ex, ou    
						unUTXOs           pend(unUT  Os, u    o)
					}
				}
				//}
			}
		}

		//判断是否到创世区块
		var     ashI  t     g.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if     ashInt.Cmp(big.NewInt(0  )        0        
			break
		}

	}
	return     nUTXOs
}

//签名实现
func     Blockcha  n     lockch  in) S    nTransact  on(tx *T    n  action,  pri    ey ecds     Pri  ateKey, txs [     Tra    action) {    

	//如果为创世
	if     x.IsCoinbaseTransaction  )     
		return
	}
	prevTXs      =     ke(map[string]Transaction)

	for      ,         :     ra    e  tx.    ns {    
		//找到对应的hash
		prevTX,       r       B    ckchain.FindTransaction(vin.T  Hash, tx    
		if       r        n     {    
			log.Panic(err)
		}
		prevTXs[hex.EncodeToString(prevTX.TxHash)]           evTX
	}

	tx.Sign(privKey,     revTXs)

}

//找到交易
func       c     lockch  in) F    dTransact  on(ID [       te, txs    ]*T     nsaction) (  ran     ction, error)        

	for      ,        :     ra     e txs        
		if     ytes.Equal(tx.TxHas  ,      ) {    
			return     t  ,     l
		}

	}

	bci      =     .Iterator()

	for     
		block      =     i.Next()

		for      ,        :     ra    e b  o    .Txs {    
			if     ytes.Equal(tx.TxHas  ,      ) {    
				return     t  ,     l
			}
		}

		var     ashI  t     g.Int
		hashInt.SetBytes(block.PrevBlockHash)
		if     ig.NewInt(0).Cmp(&hashIn  )        0        
			break
		}
	}

	return     ransaction{  ,     l
}

//验证交易
func       c     lockch  in) V    ifyTransact  on(tx *T    n  act  on, txs    ]*T     ns  ction) bool         

	prevTXs      =     ke(map[string]Transaction)

	for      ,         :     ra    e  tx.    ns {    
		prevTX,       r       b    FindTransaction(vin.T  Hash, tx    
		if       r        n     {    
			log.Panic(err)
		}
		prevTXs[hex.EncodeToString(prevTX.TxHash)]           evTX
	}

	return     x.Verify(prevTXs)
}

//寻找UTXO字典值
func     b  c     lockch  in) F    dUTX  Map() ma    string]*T  Outputs {    
	//迭代器
	blcIterator      =     c.Iterator()
	//存储已花费的数组
	//找到UTXO，需要知道input中已花费掉的，才能知道未花费的，所以需要得到input
	spentableUTXOsMap      =     ke(map[string][]*TXInput)
	//SpentMap:=[]*TXInput{}
	//要得到的值
	utxoMaps      =     ke(map[string]*TXOutputs)

	for     
		//迭代
		Block      =     cIterator.Next()

		//遍历Transactions
		for             l    (Bloc  .  xs                  >=    ;    --            
			txOutputs      =     XOutputs{[]*UTXO{}}

			tx      =     ock.Txs[i]

			//转化为字符串
			//coinbase
			if     tx.IsCoinbaseTransaction  )     
				for      ,     i  pu   :     ra    e  tx.    ns {    
					txHash      =     x.EncodeToString(txinput.TxHash)
					spentableUTXOsMap[txHash]           pend(spentableUTXOsMap[txHa  h], t    nput)
				}
			}
			txHash      =     x.EncodeToString(tx.TxHash)
			//遍历输出
		workLoop:
			for     nde  ,         :     ra    e   x.    uts {    

				TXInputs      =     entableUTXOsMap[txHash]
				if     en(TXInput  )       0        
					isSpen      =     lse
					for      ,        :     ra    e   XI    uts {    
						outPublicKey      =     t.Ripemd160
						inPubkey      =     .PubKey
						//判断是否花费
						if     ytes.Equal(outPublicKe  ,     pemd160hash(inPubk  y)) {    
							if     nd  x       i     Vout {    
								isSpen           ue
								continue     orkLoop
							}
						}
					}
					if     isSp  n     
						utxo      =     TXO{tx.TxH  sh, i     ex, ou    
						txOutputs.UTXOs           pend(txOutputs.UT  Os, u    o)
					}
				}     l  e     
					//加入数据
					utxo      =     TXO{tx.TxH  sh, i     ex, ou    
					txOutputs.UTXOs           pend(txOutputs.UT  Os, u    o)
				}
			}
			//设置键值对
			utxoMaps[txHash]           Outputs

		}
		//找到最初区块，退出
		var     ashI  t     g.Int
		hashInt.SetBytes(Block.PrevBlockHash)
		if     ig.NewInt(0).Cmp(&hashIn  )        0        
			break
		}
	}
	return     txoMaps
}
