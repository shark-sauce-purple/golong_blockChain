package blc

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	
	"log"

	"golang.org/x/crypto/ripemd160"
)

const version = byte(0x00)
const addressChecksumLen = 4 //校验长度

type Wallet struct {
	//私钥
	PrivateKey ecdsa.PrivateKey
	//公钥
	PublicKey []byte
}

// 创建钱包
func NewWallet() *Wallet {
	private, public := newKeyPair()
	wallet := Wallet{private, public}

	return &wallet
}

// 返回钱包地址
func (w *Wallet) GetAddress() []byte {
	ripemd160hash := ripemd160hash(w.PublicKey)
	Vripemd160hash := append([]byte{version}, ripemd160hash...)
	checkSumBytes := checksum(Vripemd160hash)
	bytes := append(Vripemd160hash, checkSumBytes...)
	
	return Base58Encode(bytes)
}

func ripemd160hash(pubKey []byte) []byte {
	//256
	hash256 := sha256.New()
	hash256.Write(pubKey)
	hash := hash256.Sum(nil)
	//160
	ripemd160 := ripemd160.New()
	ripemd160.Write(hash)
	return ripemd160.Sum(nil)

}

// 对公钥进行Hash
func HashPubKey(pubKey []byte) []byte {
	publicSHA256 := sha256.Sum256(pubKey)

	RIPEMD160Hasher := ripemd160.New()
	_, err := RIPEMD160Hasher.Write(publicSHA256[:])
	if err != nil {
		log.Panic(err)
	}
	publicRIPEMD160 := RIPEMD160Hasher.Sum(nil)

	return publicRIPEMD160
}

//验证地址有效性
func isValidForAddress(address []byte) bool {
	pubKeyHash := Base58Decode(address)
	actualChecksum := pubKeyHash[len(pubKeyHash)-addressChecksumLen:]
	//version := pubKeyHash[0]
	pubKeyHash = pubKeyHash[:len(pubKeyHash)-addressChecksumLen]
	//targetChecksum := checksum(append([]byte{version}, pubKeyHash...))
	targetChecksum := checksum(pubKeyHash)
	return bytes.Equal(actualChecksum, targetChecksum)

}

// Checksum 为一个公钥生成checksum
func checksum(payload []byte) []byte {
	//进行两次sum256
	firstSHA := sha256.Sum256(payload)
	secondSHA := sha256.Sum256(firstSHA[:])
	//取后四位字节
	return secondSHA[:addressChecksumLen]
}

//创建钥匙对
func newKeyPair() (ecdsa.PrivateKey, []byte) {
	//椭圆曲线
	curve := elliptic.P256()
	private, err := ecdsa.GenerateKey(curve, rand.Reader)
	if err != nil {
		log.Panic(err)
	}
	pubKey := append(private.PublicKey.X.Bytes(), private.PublicKey.Y.Bytes()...)

	return *private, pubKey
}
