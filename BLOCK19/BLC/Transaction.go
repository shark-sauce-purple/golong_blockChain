package blc

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"log"
	"math/big"
	"time"
)

type Transaction struct {
	//1.交易hash
	TxHash []byte
	//2.输入
	Vins []*TXInput
	//3.输出
	Vouts []*TXOutput
}

//判断当前交易是否是Coinbase交易
func (tx *Transaction) IsCoinbaseTransaction() bool {

	return len(tx.Vins[0].TxHash) == 0 && tx.Vins[0].Vout == -1
}

//Transaction 创建
//新区块的Transaction
func NewCoinbaseTransaction(address string) *Transaction { //address为地址

	//消费
	txinput := &TXInput{[]byte{}, -1, nil, []byte{}}

	txoutput := NewTXOutput(10, address)

	//txoutput := &TXOutput{10, address} //给创世区块10Token

	txCoinbase := &Transaction{[]byte{}, []*TXInput{txinput}, []*TXOutput{txoutput}}

	//此处hash为将自己序列化
	txCoinbase.HashTransactions()

	return txCoinbase
}

//此处为hash将自己序列化
func (tx *Transaction) HashTransactions() {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(tx)
	if err != nil {
		log.Panic(err)
	}

	resultBytes := bytes.Join([][]byte{IntToHex(time.Now().Unix()), result.Bytes()}, []byte{})
	hash := sha256.Sum256(resultBytes)
	tx.TxHash = hash[:]

}

//交易的实现
func NewSimpletransaction(from, to string, amount int, utxoSet *UTXOSet, txs []*Transaction) *Transaction {

	//返回from的所有未花费输出所对应的Transaction
	//unUTXOs := bloBlockchain.UnUTXOs(from)
	Wallet, _ := NewWallets()
	wallet := Wallet.Wallets[from]

	//返回可以用的UTXO
	money, spentableUTXODic := utxoSet.FindSpentableUTXOS(from, amount, txs)

	//unSpentTx:=UnU(from)
	//fmt.Println(unSpentTx)

	// //money,dic:={}
	var txIntputs []*TXInput
	var txoutputs []*TXOutput
	//消费
	//b, _ := hex.DecodeString("53ff52493255663c01a7817170cf4773de4a263cc4fbfc97aedfb7c29d30d1f6 ") //创世区块的交易hash
	for txhash, indexArray := range spentableUTXODic {
		txhashBytes, _ := hex.DecodeString(txhash)
		//循环得到输入
		for _, index := range indexArray {
			TXInput := &TXInput{txhashBytes, index, nil, wallet.PublicKey}
			//将输入存进Transaction
			txIntputs = append(txIntputs, TXInput)
		}

	}

	//转帐
	txoutput := NewTXOutput(amount, to)

	txoutputs = append(txoutputs, txoutput)

	//找零
	txoutput = NewTXOutput(money-amount, from)
	txoutputs = append(txoutputs, txoutput)

	tx := &Transaction{[]byte{}, txIntputs, txoutputs}

	//此处hash为将自己序列化(设置hash指)
	tx.HashTransactions()

	//进行签名
	utxoSet.Blockchain.SignTransaction(tx, wallet.PrivateKey, txs)

	return tx

}

//序列化方法
func (tx *Transaction) Serialize() []byte {
	var encoded bytes.Buffer

	enc := gob.NewEncoder(&encoded)
	err := enc.Encode(tx)
	if err != nil {
		log.Panic(err)
	}

	return encoded.Bytes()
}

//对transaction进行hash
func (tx *Transaction) Hash() []byte {
	txCopy := tx
	txCopy.TxHash = []byte{}

	hash := sha256.Sum256(txCopy.Serialize())
	return hash[:]

}

//Sign方法
func (tx *Transaction) Sign(privKey ecdsa.PrivateKey, prevTXs map[string]Transaction) {
	if tx.IsCoinbaseTransaction() {
		return
	}

	for _, vin := range tx.Vins {
		if prevTXs[hex.EncodeToString(vin.TxHash)].TxHash == nil {
			log.Panic("ERROR: 交易hash不正确")
		}
	}
	//备份
	txCopy := tx.TrimmedCopy()

	for inID, vin := range txCopy.Vins {
		prevTx := prevTXs[hex.EncodeToString(vin.TxHash)]
		txCopy.Vins[inID].Signature = nil
		txCopy.Vins[inID].PubKey = prevTx.Vouts[vin.Vout].Ripemd160
		txCopy.TxHash = txCopy.Hash()
		txCopy.Vins[inID].PubKey = nil
		//dataToSign := fmt.Sprintf("%x\n", txCopy)

		//签名代码
		r, s, err := ecdsa.Sign(rand.Reader, &privKey, txCopy.TxHash)
		if err != nil {
			log.Panic(err)
		}
		//签名的字节数组
		signature := append(r.Bytes(), s.Bytes()...)

		tx.Vins[inID].Signature = signature
		//txCopy.Vins[inID].PubKey = nil
	}
}

// 拷贝一份新的Transaction进行签名
func (tx *Transaction) TrimmedCopy() Transaction {
	var inputs []*TXInput
	var outputs []*TXOutput

	for _, vin := range tx.Vins {
		inputs = append(inputs, &TXInput{vin.TxHash, vin.Vout, nil, nil})
	}

	for _, vout := range tx.Vouts {
		outputs = append(outputs, &TXOutput{vout.Value, vout.Ripemd160})
	}

	txCopy := Transaction{tx.TxHash, inputs, outputs}

	return txCopy
}

//验证数字签名
func (tx *Transaction) Verify(prevTXs map[string]Transaction) bool {
	if tx.IsCoinbaseTransaction() {
		return true
	}

	for _, vin := range tx.Vins {
		if prevTXs[hex.EncodeToString(vin.TxHash)].TxHash == nil {
			log.Panic("ERROR: 交易hash不正确")
		}
	}
	//获取交易的副本
	txCopy := tx.TrimmedCopy()

	curve := elliptic.P256()

	//签名时所用的ID，检查他们的签名
	for inID, vin := range tx.Vins {
		prevTx := prevTXs[hex.EncodeToString(vin.TxHash)]
		txCopy.Vins[inID].Signature = nil
		txCopy.Vins[inID].PubKey = prevTx.Vouts[vin.Vout].Ripemd160
		txCopy.TxHash = txCopy.Hash()
		txCopy.Vins[inID].PubKey = nil
		//私钥ID产生
		r := big.Int{}
		s := big.Int{}
		sigLen := len(vin.Signature)
		r.SetBytes(vin.Signature[:(sigLen / 2)])
		s.SetBytes(vin.Signature[(sigLen / 2):])

		//验证数据
		x := big.Int{}
		y := big.Int{}
		//从签名中取到的
		keyLen := len(vin.PubKey)
		x.SetBytes(vin.PubKey[:(keyLen / 2)])
		y.SetBytes(vin.PubKey[(keyLen / 2):])

		//dataToVerify := fmt.Sprintf("%x\n", txCopy)
		//对签名进行解包
		rawPubKey := ecdsa.PublicKey{Curve: curve, X: &x, Y: &y} //椭圆曲线
		if !ecdsa.Verify(&rawPubKey, txCopy.TxHash, &r, &s) {
			return false
		}
		//txCopy.Vin[inID].PubKey = nil
	}

	return true

}
