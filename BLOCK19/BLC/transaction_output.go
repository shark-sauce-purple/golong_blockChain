package blc

import "bytes"

//
type TXOutput struct {
	//值
	Value int
	//公钥
	Ripemd160 []byte
}

//上锁
func (out *TXOutput) Lock(address string) {
	//进行hash
	pubKeyHash := Base58Decode([]byte(address))
	out.Ripemd160 = pubKeyHash[1 : len(pubKeyHash)-4]
}

//解锁
func (txOutput *TXOutput) OUTUnLock(address string) bool {
	pubKeyHash := Base58Decode([]byte(address))
	hash160 := pubKeyHash[1 : len(pubKeyHash)-4]
	return bytes.Equal(hash160, txOutput.Ripemd160)
}

func NewTXOutput(value int, address string) *TXOutput {
	txo := &TXOutput{value, nil}
	//上锁
	txo.Lock(address)
	return txo
}

// // Lock signs the output

// // IsLockedWithKey checks if the output can be used by the owner of the pubkey
// func (out *TXOutput) IsLockedWithKey(pubKeyHash []byte) bool {
// 	return bytes.Compare(out.PubKeyHash, pubKeyHash) == 0
// }

// // NewTXOutput create a new TXOutput

// // TXOutputs collects TXOutput
// type TXOutputs struct {
// 	Outputs []TXOutput
// }

// // Serialize serializes TXOutputs
// func (outs TXOutputs) Serialize() []byte {
// 	var buff bytes.Buffer

// 	enc := gob.NewEncoder(&buff)
// 	err := enc.Encode(outs)
// 	if err != nil {
// 		log.Panic(err)
// 	}

// 	return buff.Bytes()
// }

// // DeserializeOutputs deserializes TXOutputs
// func DeserializeOutputs(data []byte) TXOutputs {
// 	var outputs TXOutputs

// 	dec := gob.NewDecoder(bytes.NewReader(data))
// 	err := dec.Decode(&outputs)
// 	if err != nil {
// 		log.Panic(err)
// 	}

// 	return outputs
// }
