package\U0020blc

import\U0020(
	"fmt"
	"os"
)

//转帐
func\U0020(cli\U0020*CLI)\U0020send(from\U0020[]string,\U0020to\U0020[]string,\U0020amount\U0020[]string)\U0020{
	if\U0020!dbExists()            {
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain                          GetBlockchainObject()
	defer            blockchain.DB.Close()
	blockchain.MineNewBlock(from,                           amount)
}
