package blc

import (
	"flag"
	"fmt"
	"log"
	"os"
)

type CLI struct {
	// Blockchain *Blockchain
}

//输出使用说明
func printUsage() {
	fmt.Println("使用说明")
	fmt.Println("\tcreateblockchain -address ADDRESS--地址")
	fmt.Println("\tsend -from FROM -to To -amount AMOUNT  DATA--交易数据")
	fmt.Println("\tprintchain --输出数据")

}

//判断args，防止error
func isValidArgs() {
	//如果终端数据<2
	if len(os.Args) < 2 {
		printUsage()
		os.Exit(0)
	}

}

// //增加区块
// func (cli *CLI) addBlock(txs []*Transaction) {
// 	if dbExists() == false {
// 		fmt.Println("数据库不存在")
// 		os.Exit(1)
// 	}
// 	//获取数据库
// 	blockchain := GetBlockchainObject()
// 	defer blockchain.DB.Close()
// 	//调用函数
// 	blockchain.AddBlockToBlockchain(txs)

// }

//输出区块链数据
func (cli *CLI) printChain() {
	if dbExists() == false {
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	//调用函数
	blockchain.PrintChain()

}

//创建创世区块
func (cli *CLI) creatBlockChain(address string) {

	//创建交易
	blockchain:=CreateGenesisBlockchain(address)
	defer blockchain.DB.Close()
}

//转帐
func (cli *CLI) send(from []string, to []string, amount []string) {
	if dbExists() == false {
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	blockchain.MineNewBlock(from,to,amount)
}

func (cli *CLI) Run() {
	isValidArgs()

	sendBlockCmd := flag.NewFlagSet("send", flag.ExitOnError)
	printChainCmd := flag.NewFlagSet("printchain", flag.ExitOnError)
	creatBlockChainCmd := flag.NewFlagSet("createblockchain", flag.ExitOnError)
	flagFrom := sendBlockCmd.String("from", "", "转账源地址。。。")
	flagTo := sendBlockCmd.String("to", "", "转账目的地址。。。")                                          //增加数据
	flagamount := sendBlockCmd.String("amount", "", "转账金额。。。")                                    //增加数据
	flagCreateBlockchainWithAddress := creatBlockChainCmd.String("address", "创世区块数据", "创建创世区块地址") //第二个为默认值

	//解析数据
	switch os.Args[1] {
	case "send":
		//解析后续内容
		err := sendBlockCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "printchain":
		//解析后续内容
		err := printChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	case "createblockchain":
		err := creatBlockChainCmd.Parse(os.Args[2:])
		if err != nil {
			log.Panic(err)
		}
	default:
		printUsage()
		os.Exit(0)

	}
	//增加
	if sendBlockCmd.Parsed() { //解析add数据
		if *flagFrom == "" || *flagTo == "" || *flagamount == "" { //如果没有data
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		// fmt.Println(JSONTOArry(*flagFrom))
		// fmt.Println(JSONTOArry(*flagTo))
		// fmt.Println(JSONTOArry(*flagamount))
		from := JSONTOArry(*flagFrom)
		to := JSONTOArry(*flagTo)
		amount := JSONTOArry(*flagamount)
		cli.send(from, to, amount)
	}
	//输出
	if printChainCmd.Parsed() {
		//fmt.Println("输出所有区块数据")
		cli.printChain()
	}

	//创世区块创建
	if creatBlockChainCmd.Parsed() {
		if *flagCreateBlockchainWithAddress == "" { //如果没有data
			fmt.Println("地址不能为空")
			printUsage()
			os.Exit(1)
		}
		//如果有数据
		//fmt.Println(*flagAddBlockData)
		cli.creatBlockChain(*flagCreateBlockchainWithAddress)

	}
}
