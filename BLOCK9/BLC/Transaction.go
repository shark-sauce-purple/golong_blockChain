package blc

import (
	"bytes"
	"crypto/sha256"
	"encoding/gob"
	"encoding/hex"
	"log"
)

type Transaction struct {
	//1.交易hash
	TxHash []byte
	//2.输入
	Vins []*TXInput
	//3.输出
	Vouts []*TXOutput
}

//Transaction 创建
//1.创世区块的Transaction
func NewCoinbaseTransaction(address string) *Transaction { //address为

	//消费
	txinput := &TXInput{[]byte{}, -1, "创世区块"}
	txoutput := &TXOutput{10, address}

	txCoinbase := &Transaction{[]byte{}, []*TXInput{txinput}, []*TXOutput{txoutput}}

	//此处hash为将自己序列化
	txCoinbase.HashTransactions()

	return txCoinbase
}

//此处为hash将自己序列化
func (tx *Transaction) HashTransactions() {
	var result bytes.Buffer
	encoder := gob.NewEncoder(&result)

	err := encoder.Encode(tx)
	if err != nil {
		log.Panic(err)
	}
	hash := sha256.Sum256(result.Bytes())
	tx.TxHash = hash[:]

}

//一笔交易的实现
func NewSimpletransaction(from, to string, amount int) *Transaction {

	//$./main send -from '[\"测试1号发送\"]' -to '[\"测试1号接受\"]' -amount '[\"4\"]'
	var txIntputs []*TXInput
	var txoutputs []*TXOutput
	//消费
	b, _ := hex.DecodeString("6cbb3d9168f775eda6689647a4a1a0ff4543665629ba9c692b7473619e950fbc") //创世区块的交易hash
	txinput := &TXInput{b, 0, from} //上一个区块的hash，索引，来自
	txIntputs = append(txIntputs, txinput)

	//转帐
	txoutput := &TXOutput{amount, to}
	txoutputs = append(txoutputs, txoutput)

	//找零
	txoutput = &TXOutput{10 - amount, to}
	txoutputs = append(txoutputs, txoutput)

	txCoinbase := &Transaction{[]byte{}, txIntputs, txoutputs}

	//此处hash为将自己序列化(设置hash指)
	txCoinbase.HashTransactions()

	return txCoinbase

}

//第二笔交易实现
