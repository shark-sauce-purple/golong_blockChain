package blc

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"log"
)

//将int64转换为字节数组
func IntToHex(num int64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, num)
	if err != nil {
		log.Panic(err)
	}

	return buff.Bytes()
}

//将json字符串转数组
func JSONTOArry(js string)[]string{

	//json-》数组
	var sArr []string
	if err:=json.Unmarshal([]byte(js),&sArr);err!=nil{
		log.Panic(err)
	}
	return sArr
}