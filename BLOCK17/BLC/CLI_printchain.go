package blc

import (
	"fmt"
	"os"
)

//输出区块链数据
func (cli *CLI) printChain() {
	if !dbExists() {
		fmt.Println("数据库不存在")
		os.Exit(1)
	}
	//获取数据库
	blockchain := GetBlockchainObject()
	defer blockchain.DB.Close()
	//调用函数
	blockchain.PrintChain()

}
