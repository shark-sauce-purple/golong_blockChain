package blc

type UTXO struct {
	TxHash  []byte
	index   int
	Outputs *TXOutput
}
